package cn.krismile.boot.framework.core.util.common;

import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;

/**
 * 项目工具类
 *
 * <ul>
 *     <li><b>getResourcePath</b> - 获取项目 [resources] 文件夹下的文件</li>
 * </ul>
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public class ProjectUtils {

    /**
     * 运行时获取项目中Resource目录
     *
     * @return 项目中Resource目录
     * @throws FileNotFoundException 文件未找到
     * @since 1.0.0
     */
    public static String getResourcePath() throws FileNotFoundException {
        return getResourcePath("");
    }

    /**
     * 运行时获取项目中Resource目录
     *
     * @param filePath 相对于 {@code resource} 目录下的路径
     * @return 项目中Resource目录
     * @throws FileNotFoundException 文件未找到
     * @since 1.0.0
     */
    public static String getResourcePath(@NonNull String filePath) throws FileNotFoundException {
        return ResourceUtils.getURL(ResourceUtils.CLASSPATH_URL_PREFIX + filePath).getPath();
    }
}
