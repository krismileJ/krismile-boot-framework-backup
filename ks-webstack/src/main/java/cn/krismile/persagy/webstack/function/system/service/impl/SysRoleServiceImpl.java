package cn.krismile.persagy.webstack.function.system.service.impl;

import cn.krismile.persagy.webstack.pojo.domain.SysRoleDO;
import cn.krismile.persagy.webstack.function.system.mapper.SysRoleMapper;
import cn.krismile.persagy.webstack.function.system.service.SysRoleService;
import cn.krismile.boot.framework.mybatisplus.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 系统-角色表 ServiceImpl
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Service
public class SysRoleServiceImpl extends BaseServiceImpl<SysRoleMapper, SysRoleDO> implements SysRoleService {

}
