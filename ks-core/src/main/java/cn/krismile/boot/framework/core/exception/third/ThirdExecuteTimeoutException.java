package cn.krismile.boot.framework.core.exception.third;

import cn.krismile.boot.framework.core.enumeration.error.third.ThirdExecuteTimeoutErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 第三方系统执行超时自定义异常
 *
 * @author JiYinchuan
 * @see ThirdExecuteTimeoutErrorEnum
 * @since 1.0.0
 */
public class ThirdExecuteTimeoutException extends ApplicationException {

    private static final long serialVersionUID = -873199353552536101L;

    public ThirdExecuteTimeoutException(ThirdExecuteTimeoutErrorEnum thirdExecuteTimeoutErrorEnum) {
        super(thirdExecuteTimeoutErrorEnum);
    }

    public ThirdExecuteTimeoutException(ThirdExecuteTimeoutErrorEnum thirdExecuteTimeoutErrorEnum, String userTip) {
        super(thirdExecuteTimeoutErrorEnum, userTip);
    }
}
