package cn.krismile.boot.framework.core.constant;

/**
 * Krismile常量
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public class KrismileConstant {

    /**
     * 小写 krismile
     */
    public static final String KRISMILE_LOWERCASE = "krismile";

    /**
     * 大写 krismile
     */
    public static final String KRISMILE_UPPERCASE = "KRISMILE";

    /**
     * 缩写 krismile
     */
    public static final String KRISMILE_ABBREVIATION = "ks";

}
