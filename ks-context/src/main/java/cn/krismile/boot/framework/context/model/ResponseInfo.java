package cn.krismile.boot.framework.context.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 请求响应信息封装
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@Accessors(chain = true)
public class ResponseInfo implements Serializable {

    private static final long serialVersionUID = -8829636801227767948L;

    /**
     * 请求结果
     */
    private Object result;

    /**
     * 请求执行时间
     */
    private long executionTime;

}
