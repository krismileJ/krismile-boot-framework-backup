package cn.krismile.persagy.webstack.config.intercept;

import cn.krismile.boot.framework.context.storage.LocalContextStorage;
import cn.krismile.boot.framework.context.util.HttpResponseUtils;
import cn.krismile.boot.framework.core.enumeration.error.client.AuthorityErrorEnum;
import cn.krismile.boot.framework.core.exception.client.AuthorityException;
import cn.krismile.boot.framework.core.response.ResultVO;
import cn.krismile.boot.framework.jwt.payload.JwtPayload;
import cn.krismile.boot.framework.jwt.storage.JwtPayloadStorage;
import cn.krismile.boot.framework.jwt.util.JwtUtils;
import cn.krismile.persagy.webstack.function.system.service.SysAdminService;
import cn.krismile.persagy.webstack.key.cache.UserKey;
import cn.krismile.persagy.webstack.pojo.context.UserInfoContext;
import cn.krismile.persagy.webstack.pojo.domain.SysAdminDO;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * 权限拦截器
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Component
@Slf4j
public class AuthorizationIntercept implements HandlerInterceptor {

    private static final String LOG_TAG = "Authorization";

    @Resource
    private SysAdminService adminService;

    @Override
    public boolean preHandle(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull Object handler) throws Exception {
        try {
            // 校验WebCode
            String webCode = request.getHeader("WebCode");
            if (StringUtils.isNotBlank(webCode)) {
                SysAdminDO dbUser = adminService.getCacheByWebCode(webCode);
                if (Objects.nonNull(dbUser)) {
                    try {
                        adminService.checkStatus(dbUser.getId());
                    } catch (AuthorityException e) {
                        log.warn("[{}] 账号被冻结 [userId: {}, requestUri: {}]", LOG_TAG, dbUser.getId(), request.getRequestURI());
                        HttpResponseUtils.writeOfJson(response, ResultVO.base(AuthorityErrorEnum.ACCOUNT_IS_FROZEN, "帐号被冻结"));
                        return false;
                    }
                    LocalContextStorage.set(UserKey.CONTEXT, new UserInfoContext().setUserId(dbUser.getId()));
                    return true;
                }
            }

            // 校验Token
            String token = request.getHeader("Authorization");
            if (StringUtils.isBlank(token)) {
                log.warn("[{}] 未授权的请求 [requestUri: {}]", LOG_TAG, request.getRequestURI());
                HttpResponseUtils.writeOfJson(response, ResultVO.base(AuthorityErrorEnum.UNAUTHORIZED, "未授权的请求"));
                return false;
            }
            try {
                JwtPayload jwtPayload = JwtUtils.decodeTokenGet("123456", token);
                JwtPayloadStorage.set(jwtPayload);
                SysAdminDO dbUser = adminService.getCacheById(jwtPayload.getUserId());
                try {
                    // 校验用户状态
                    adminService.checkStatus(dbUser.getId());
                } catch (AuthorityException e) {
                    log.warn("[{}] 账号被冻结 [userId: {}, requestUri: {}]", LOG_TAG, dbUser.getId(), request.getRequestURI());
                    HttpResponseUtils.writeOfJson(response, ResultVO.base(AuthorityErrorEnum.ACCOUNT_IS_FROZEN, "账号被冻结"));
                    return false;
                }
                LocalContextStorage.set(UserKey.CONTEXT, new UserInfoContext().setUserId(jwtPayload.getUserId()));
                return true;
            } catch (AuthorityException e) {
                log.warn("[{}] 账号被冻结 [payload: {}, requestUri: {}]", LOG_TAG, JwtPayloadStorage.get(), request.getRequestURI());
                HttpResponseUtils.writeOfJson(response, ResultVO.base(AuthorityErrorEnum.ACCOUNT_IS_FROZEN, "账号被冻结"));
            } catch (TokenExpiredException e) {
                log.warn("[{}] Token已过期 [requestUri: {}]", LOG_TAG, request.getRequestURI());
                HttpResponseUtils.writeOfJson(response, ResultVO.base(AuthorityErrorEnum.AUTHORITY_EXPIRED, "授权已过期"));
            } catch (SignatureVerificationException | JWTDecodeException e) {
                log.error("[{}] Token异常 [requestUri: {}]", LOG_TAG, request.getRequestURI());
                HttpResponseUtils.writeOfJson(response, ResultVO.base(AuthorityErrorEnum.ACCESS_BLOCKED, "非法的授权"));
            }
        } catch (Exception e) {
            log.error("[{}] 授权异常 [requestUri: {}]", LOG_TAG, request.getRequestURI(), e);
            HttpResponseUtils.writeOfJson(response, ResultVO.base(AuthorityErrorEnum.AUTHORITY_REJECTED, "授权异常"));
        }
        return false;
    }
}
