package cn.krismile.persagy.webstack.function.system.service.impl;

import cn.krismile.persagy.webstack.pojo.domain.SysPermissionDO;
import cn.krismile.persagy.webstack.function.system.mapper.SysPermissionMapper;
import cn.krismile.persagy.webstack.function.system.service.SysPermissionService;
import cn.krismile.boot.framework.mybatisplus.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 系统-权限表 ServiceImpl
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Service
public class SysPermissionServiceImpl extends BaseServiceImpl<SysPermissionMapper, SysPermissionDO> implements SysPermissionService {

}
