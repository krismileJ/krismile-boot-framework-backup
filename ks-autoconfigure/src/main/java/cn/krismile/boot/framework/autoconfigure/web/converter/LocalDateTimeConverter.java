package cn.krismile.boot.framework.autoconfigure.web.converter;

import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.boot.framework.core.constant.PatternConstant;
import cn.krismile.boot.framework.core.util.inner.StringUtils;
import org.springframework.core.convert.converter.Converter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * LocalDateTime自定义转换器
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public class LocalDateTimeConverter implements Converter<String, LocalDateTime> {

    @Override
    public LocalDateTime convert(@NonNull String s) {
        try {
            return LocalDateTime.parse(s, DateTimeFormatter.ofPattern(PatternConstant.Date.NORM_DATETIME_PATTERN));
        }catch (DateTimeParseException e) {
            if (StringUtils.isParsable(s)) {
                return Instant.ofEpochMilli(Long.parseLong(s)).atZone(ZoneId.systemDefault()).toLocalDateTime();
            }
            throw e;
        }
    }
}
