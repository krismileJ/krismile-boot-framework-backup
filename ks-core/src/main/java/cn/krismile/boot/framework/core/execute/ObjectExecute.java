package cn.krismile.boot.framework.core.execute;

import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.boot.framework.core.annotation.jsr.Nullable;
import cn.krismile.boot.framework.core.exception.EnumIllegalArgumentException;
import cn.krismile.boot.framework.core.util.inner.CollectionUtils;
import cn.krismile.boot.framework.core.util.inner.StringUtils;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * 对象执行器
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public class ObjectExecute {

    /**
     * 执行器
     *
     * @param condition 执行条件
     * @param callback  执行回调
     * @since 1.0.0
     */
    public static void execute(@Nullable Boolean condition, @NonNull Consumer<Boolean> callback) {
        if (Objects.nonNull(condition) && condition) {
            callback.accept(true);
        }
    }

    /**
     * 执行器
     *
     * @param condition 执行条件
     * @param callback  执行回调
     * @since 1.0.0
     */
    public static void execute(@Nullable Supplier<Boolean> condition, @NonNull Consumer<Boolean> callback) {
        if (Objects.nonNull(condition) && condition.get()) {
            callback.accept(true);
        }
    }

    /**
     * 执行器
     *
     * @param condition 非 {@code null} 时执行
     * @param callback  执行回调
     * @since 1.0.0
     */
    public static void notNullExecute(@Nullable Object condition, @NonNull Consumer<Boolean> callback) {
        if (Objects.nonNull(condition)) {
            callback.accept(true);
        }
    }

    /**
     * 执行器
     *
     * @param condition 非 {@code null} 且非空时执行
     * @param callback  执行回调
     * @since 1.0.0
     */
    public static void notBlankExecute(@Nullable String condition, @NonNull Consumer<Boolean> callback) {
        if (StringUtils.isNotBlank(condition)) {
            callback.accept(true);
        }
    }

    /**
     * 执行器
     *
     * @param condition 非 {@code null} 且非空时执行
     * @param callback  执行回调
     * @since 1.0.0
     */
    public static void notEmptyExecute(@Nullable Collection<?> condition, @NonNull Consumer<Boolean> callback) {
        if (CollectionUtils.isNotEmpty(condition)) {
            callback.accept(true);
        }
    }

    public static <T extends Enum<T>> void matchEnumExecute(
            @Nullable T sourceEnum, @Nullable T matchEnum,
            @NonNull Class<T> enumClass, @NonNull Supplier<T> callback) {
        if (Objects.nonNull(sourceEnum) && Objects.nonNull(matchEnum)) {
            boolean isMatch = false;
            T[] enumConstants = enumClass.getEnumConstants();
            for (T enumConstant : enumConstants) {
                if (enumConstant == matchEnum) {
                    callback.get();
                    isMatch = true;
                    break;
                }
            }
            if (!isMatch) {
                throw new EnumIllegalArgumentException("No matching [" + enumClass.getName() + "] for [" + sourceEnum + "]");
            }
        }
    }
}
