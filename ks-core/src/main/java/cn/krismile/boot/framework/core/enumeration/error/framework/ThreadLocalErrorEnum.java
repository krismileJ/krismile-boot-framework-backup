package cn.krismile.boot.framework.core.enumeration.error.framework;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * ThreadLocal异常枚举
 *
 * @author JiYinchuan
 * @see cn.krismile.boot.framework.core.exception.framework.ThreadLocalException
 * @since 1.0.0
 */
public enum ThreadLocalErrorEnum implements BaseEnum<String> {

    /**
     * ThreadLocal异常
     */
    THREAD_LOCAL_ERROR("K0100", "ThreadLocal异常"),

    /**
     * 设置的键为空
     */
    KEY_EMPTY("K0110", "设置的键为空"),

    /**
     * 设置的值为空
     */
    VALUE_EMPTY("K0120", "设置的值为空"),

    /**
     * 获取数据结果为空
     */
    RESULT_EMPTY("K0130", "获取数据结果为空");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    ThreadLocalErrorEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static ThreadLocalErrorEnum analyze(String value) {
        return BaseEnum.analyze(value, ThreadLocalErrorEnum.class);
    }
}
