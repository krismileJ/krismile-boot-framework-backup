package cn.krismile.persagy.webstack.pojo.vo.admin;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 导航栏VO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@Accessors(chain = true)
@Schema(title = "导航栏VO")
public class NavVO {

    @Schema(description = "导航栏ID")
    private Long navId;

    @Schema(description = "导航栏标题")
    private String navTitle;

    @Schema(description = "标签名称")
    private String navTagName;

    @Schema(description = "图标样式")
    private String navIcon;

    @Schema(description = "导航栏排序权重")
    private Integer navOrderWeight;

    @Schema(description = "导航栏是否已经关联")
    private Boolean navIsAlreadyRelation;

    @Schema(description = "上级导航栏ID")
    private Long navParentId;

    @Schema(description = "导航栏子节点")
    private List<NavVO> navChildren;

}
