package cn.krismile.persagy.webstack.function.business.mapper;

import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.persagy.webstack.pojo.domain.BizRelWebsiteSysAdminDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 业务-站点-用户关联表 Mapper 接口
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Mapper
@Repository
public interface BizRelWebsiteSysAdminMapper extends BaseMapper<BizRelWebsiteSysAdminDO> {

    /**
     * 数据数量
     *
     * @param websiteId 网站ID
     * @param adminId   管理员ID
     * @return 数据是否存在
     * @since 1.0.0
     */
    Long count(@NonNull @Param("websiteId") Long websiteId,
               @NonNull @Param("adminId") Long adminId);


    /**
     * 获取实体类
     *
     * @param isLock    是否锁定当前数据
     * @param websiteId 网站ID
     * @param adminId   用户ID
     * @return 实体类
     * @since 1.0.0
     */
    BizRelWebsiteSysAdminDO get(@NonNull @Param("isLock") boolean isLock,
                                @NonNull @Param("websiteId") Long websiteId,
                                @NonNull @Param("adminId") Long adminId);


    /**
     * 获取实体类
     *
     * @param websiteId 网站ID
     * @param adminId   用户ID
     * @return 实体类
     * @since 1.0.0
     */
    default BizRelWebsiteSysAdminDO get(@NonNull @Param("websiteId") Long websiteId,
                                        @NonNull @Param("adminId") Long adminId) {
        return get(false, websiteId, adminId);
    }
}
