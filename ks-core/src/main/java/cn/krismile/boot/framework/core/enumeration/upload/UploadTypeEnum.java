package cn.krismile.boot.framework.core.enumeration.upload;

import com.fasterxml.jackson.annotation.JsonCreator;
import cn.krismile.boot.framework.core.enumeration.BaseEnum;

/**
 * 上传类型枚举
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public enum UploadTypeEnum implements BaseEnum<String> {

    /**
     * 图片类型
     */
    IMAGE("image", "图片类型"),

    /**
     * 文件类型
     */
    FILE("file", "文件类型"),

    /**
     * 视频类型
     */
    VIDEO("video", "视频类型"),

    /**
     * 音频类型
     */
    AUDIO("audio", "音频类型"),

    /**
     * 其他类型
     */
    OTHER("other", "其他类型");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    UploadTypeEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static UploadTypeEnum analyze(String value) {
        return BaseEnum.analyze(value, UploadTypeEnum.class);
    }
}
