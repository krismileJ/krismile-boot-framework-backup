package cn.krismile.persagy.webstack.pojo.dto;

import cn.krismile.persagy.webstack.enumeration.business.ResourceStatusEnum;
import cn.krismile.persagy.webstack.enumeration.business.ResourceTypeEnum;
import cn.krismile.persagy.webstack.validate.ValidateGroup.Insert;
import cn.krismile.persagy.webstack.validate.ValidateGroup.Update;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 导航栏DTO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@Accessors(chain = true)
@Schema(title = "导航栏DTO")
public class NavDTO {

    @Schema(description = "模版库ID(内置导航栏ID或公开导航栏ID)")
    private Long templateId;

    @Schema(description = "导航栏ID")
    @NotNull(groups = Update.class)
    private Long navId;

    @Schema(description = "导航栏标题名称")
    @NotBlank(groups = {Insert.class, Update.class})
    private String navTitle;

    @Schema(description = "导航栏标签名称")
    private String navTagName;

    @Schema(description = "导航栏图标样式")
    private String navIcon;

    @Schema(description = "导航栏排序权重")
    @NotNull(groups = {Insert.class, Update.class})
    private Integer navOrderWeight;

    @Schema(description = "导航栏类型 [INTERNAL: 内部, USER: 用户, PUBLIC: 公共]")
    private ResourceTypeEnum navType;

    @Schema(description = "导航栏状态 [UNAPPROVED: 待审核, APPROVED: 已审核]")
    private ResourceStatusEnum navStatus;

    @Schema(description = "导航栏是否已经关联")
    private Boolean navIsAlreadyRelation;

    @Schema(description = "上级导航栏ID")
    @NotNull(groups = {Insert.class, Update.class})
    private Long navParentId;

    @Schema(description = "子导航栏")
    private List<NavDTO> navChildren;

}
