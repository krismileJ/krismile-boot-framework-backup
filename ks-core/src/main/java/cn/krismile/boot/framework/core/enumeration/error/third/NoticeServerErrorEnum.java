package cn.krismile.boot.framework.core.enumeration.error.third;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import cn.krismile.boot.framework.core.enumeration.error.client.RequestParamErrorEnum;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 通知服务出错异常枚举
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public enum NoticeServerErrorEnum implements BaseEnum<String> {

    /**
     * 通知服务出错
     */
    NOTICE_SERVICE_ERROR("C0500", "通知服务出错"),

    /**
     * 短信提醒服务失败
     */
    SMS_SERVICE_ERROR("C0501", "短信提醒服务失败"),

    /**
     * 语音提醒服务失败
     */
    VOICE_SERVICE_ERROR("C0502", "语音提醒服务失败"),

    /**
     * 邮件提醒服务失败
     */
    EMAIL_SERVICE_ERROR("C0503", "邮件提醒服务失败");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    NoticeServerErrorEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static RequestParamErrorEnum analyze(String value) {
        return BaseEnum.analyze(value, RequestParamErrorEnum.class);
    }
}
