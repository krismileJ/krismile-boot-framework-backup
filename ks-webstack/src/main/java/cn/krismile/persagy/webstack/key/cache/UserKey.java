package cn.krismile.persagy.webstack.key.cache;

/**
 * 用户Key
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface UserKey {

    /**
     * 用户上下文Key
     */
    String CONTEXT = "USER:CONTEXT";

}
