package cn.krismile.boot.framework.autoconfigure.aop;

import cn.krismile.boot.framework.autoconfigure.aop.properties.KsAopProperties;
import cn.krismile.boot.framework.autoconfigure.aop.properties.children.KsRequestLogProperties;
import cn.krismile.boot.framework.context.aop.RequestLogAop;
import cn.krismile.boot.framework.context.chain.DefaultRequestLogChainExecute;
import cn.krismile.boot.framework.context.chain.RequestInfoChainExecute;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * AOP自动配置类
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnWebApplication
@AutoConfigureAfter(org.springframework.boot.autoconfigure.aop.AopAutoConfiguration.class)
@EnableConfigurationProperties({
        KsAopProperties.class,
        KsRequestLogProperties.class
})
public class KsAopAutoConfiguration {

    /**
     * 请求日志自动配置类
     *
     * @author JiYinchuan
     * @since 1.0.0
     */
    @Configuration(proxyBeanMethods = false)
    @ConditionalOnProperty(
            prefix = KsRequestLogProperties.KEY,
            name = "enable",
            havingValue = "true",
            matchIfMissing = true)
    static class RequestLogAutoConfiguration {

        @Bean
        @ConditionalOnMissingBean
        public RequestInfoChainExecute requestInfoChainExecute(KsRequestLogProperties ksRequestLogProperties) {
            return new DefaultRequestLogChainExecute(ksRequestLogProperties.getDebugPrintHeaderNames());
        }

        @Bean
        @ConditionalOnMissingBean
        public RequestLogAop requestLogAop(List<RequestInfoChainExecute> requestInfoChainExecutes) {
            return new RequestLogAop(requestInfoChainExecutes);
        }
    }
}
