package cn.krismile.boot.framework.mybatisplus.enumeration.error;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * MybatisPlus异常枚举
 *
 * @author JiYinchuan
 * @see cn.krismile.boot.framework.mybatisplus.exception.MybatisServiceException
 * @since 1.0.0
 */
public enum MybatisServiceErrorEnum implements BaseEnum<String> {

    /**
     * MybatisService异常
     */
    MYBATIS_SERVICE_ERROR("M0100", "MybatisService异常"),

    /**
     * ID为空
     */
    ID_IS_NULL("M0101", "ID为空"),

    /**
     * 结果为空
     */
    RESULT_IS_NULL("M0102", "结果为空"),

    /**
     * 结果集为空
     */
    RESULT_SET_IS_NULL("M0103", "结果集为空"),

    /**
     * 执行操作异常
     */
    OPERATION_ERROR("M0110", "执行操作异常"),

    /**
     * ID异常
     */
    ID_ERROR("M0111", "ID异常"),

    /**
     * 幂等性重复
     */
    IDEMPOTENT_REPETITION("M0120", "幂等性重复");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    MybatisServiceErrorEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static MybatisServiceErrorEnum analyze(String value) {
        return BaseEnum.analyze(value, MybatisServiceErrorEnum.class);
    }
}
