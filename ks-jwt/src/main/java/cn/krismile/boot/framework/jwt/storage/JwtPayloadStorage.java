package cn.krismile.boot.framework.jwt.storage;

import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.boot.framework.core.annotation.jsr.Nullable;
import cn.krismile.boot.framework.core.enumeration.error.client.AuthorityErrorEnum;
import cn.krismile.boot.framework.core.exception.client.AuthorityException;
import cn.krismile.boot.framework.jwt.payload.JwtPayload;
import com.alibaba.ttl.TransmittableThreadLocal;

import java.util.Optional;

/**
 * 当前用户上下文存储
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public class JwtPayloadStorage {

    private static final ThreadLocal<JwtPayload> LOCAL = new TransmittableThreadLocal<>();

    /**
     * 设置用户JWT信息
     *
     * @param payload JWT载荷
     * @return JwtPayload
     * @since 1.0.0
     */
    @Nullable
    public static JwtPayload set(@Nullable final JwtPayload payload) {
        LOCAL.set(payload);
        return payload;
    }

    /**
     * 获取用户JWT信息
     *
     * @return JwtPayload
     * @since 1.0.0
     */
    @Nullable
    public static JwtPayload get() {
        return LOCAL.get();
    }

    /**
     * 获取用户JWT信息
     *
     * @return JwtPayload
     * @since 1.0.0
     */
    @NonNull
    public static Optional<JwtPayload> getOpt() {
        return Optional.ofNullable(get());
    }

    /**
     * 获取用户JWT信息
     *
     * @return JwtPayload
     * @throws AuthorityException 未获取到用户JWT信息
     * @since 1.0.0
     */
    @NonNull
    public static JwtPayload getThrow() throws AuthorityException {
        Optional<JwtPayload> jwtPayload = Optional.ofNullable(get());
        if (jwtPayload.isPresent()) {
            return jwtPayload.get();
        }
        throw new AuthorityException(AuthorityErrorEnum.AUTHORITY_ERROR);
    }

    /**
     * 判断是否已经存在用户JWT信息
     *
     * @return {false: 不存在, true: 已存在}
     * @since 1.0.0
     */
    public static boolean isExists() {
        return get() != null;
    }

    /**
     * 移除用户JWT信息
     *
     * @since 1.0.0
     */
    public static void remove() {
        LOCAL.remove();
    }
}
