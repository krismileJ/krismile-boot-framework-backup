package cn.krismile.boot.framework.cache.redis;

import cn.krismile.boot.framework.cache.CacheObjectOperation;
import cn.krismile.boot.framework.cache.CacheProvider;
import cn.krismile.boot.framework.cache.redis.operation.RedisCacheObjectOperation;
import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * RedisService服务
 *
 * <p>该类为 {@code redis} 操作核心服务接口
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public class RedisCache implements CacheProvider {

    private final RedisTemplate<String, Object> redisTemplate;
    private final CacheObjectOperation objectOperation;

    /**
     * 构造器
     *
     * @param redisTemplate RedisTemplate
     * @since 1.0.0
     */
    public RedisCache(@NonNull RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
        objectOperation = new RedisCacheObjectOperation(this);
    }

    @Override
    @NonNull
    public Set<String> keys(@NonNull String pattern) {
        Set<String> result = redisTemplate.keys(pattern);
        return Objects.nonNull(result) ? result : new HashSet<>();
    }

    @Override
    public boolean expire(@NonNull String key, long expireTime) {
        if (expireTime > 0) {
            Boolean result = redisTemplate.expire(key, expireTime, TimeUnit.SECONDS);
            return result != null && result;
        }
        return false;
    }

    @Override
    public long getExpire(@NonNull String key) {
        Long result = redisTemplate.getExpire(key, TimeUnit.SECONDS);
        return Objects.nonNull(result) ? result : -1;
    }

    @Override
    public boolean hasKey(@NonNull String key) {
        Boolean result = redisTemplate.hasKey(key);
        return Objects.nonNull(result) && result;
    }

    @Override
    public long del(@NonNull String... key) {
        long result = 0;
        if (key.length > 0) {
            if (key.length == 1) {
                Boolean deleteResult = redisTemplate.delete(key[0]);
                result = Objects.nonNull(deleteResult) && deleteResult ? 1 : -1;
            } else {
                Long deleteResult = redisTemplate.delete(Arrays.asList(key));
                result = Objects.nonNull(deleteResult) ? deleteResult : -1;
            }
        }
        return result;
    }

    /**
     * 获取实际操作类
     *
     * @return 实际操作类
     * @since 1.0.0
     */
    public RedisTemplate<String, Object> delegate() {
        return redisTemplate;
    }

    /**
     * 对象操作类实体
     *
     * @return 对象操作类实体
     * @since v5.0.1
     */
    public CacheObjectOperation object() {
        return objectOperation;
    }
}
