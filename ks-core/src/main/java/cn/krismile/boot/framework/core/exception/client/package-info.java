/**
 * [ks-core] 异常-客户端
 */
@NonNullApi
@NonNullFields
package cn.krismile.boot.framework.core.exception.client;

import cn.krismile.boot.framework.core.annotation.jsr.NonNullApi;
import cn.krismile.boot.framework.core.annotation.jsr.NonNullFields;