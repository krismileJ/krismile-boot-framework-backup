package cn.krismile.boot.framework.context.util;

import cn.krismile.boot.framework.context.model.RequestInfo;
import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.boot.framework.core.util.Assert;
import eu.bitwalker.useragentutils.UserAgent;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * 请求工具类
 *
 * <p>该类主要用于处理请求相关数据
 * <ul>
 *     <li><b>parseInfo</b> - 解析请求相关信息</li>
 *     <li><b>isJsonRequest</b> - 判断是否为JSON请求</li>
 * </ul>
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public class HttpRequestUtils {

    /**
     * 方法参数发现器
     */
    private static final ParameterNameDiscoverer PARAMETER_NAME_DISCOVERER = new DefaultParameterNameDiscoverer();

    /**
     * 解析请求相关信息
     *
     * @param request HttpServletRequest
     * @param method  请求方法
     * @return RequestInfo
     * @since 1.0.0
     */
    public static RequestInfo parseInfo(@NonNull HttpServletRequest request, @NonNull Method method) {
        Assert.notNull(request, "HttpServletRequest must not be null");
        Assert.notNull(method, "Request method must not be null");

        String threadId = Long.toString(Thread.currentThread().getId());
        String threadName = Thread.currentThread().getName();
        String uri = request.getRequestURI();
        String clientIp = IpUtils.getIpv4Address(request);

        String userAgent = request.getHeader(HttpHeaders.USER_AGENT);
        UserAgent userAgentData = UserAgent.parseUserAgentString(userAgent);
        String os = userAgentData.getOperatingSystem().toString();
        String browser = userAgentData.getBrowser().toString();

        String methodType = request.getMethod();
        String[] parameterNames = PARAMETER_NAME_DISCOVERER.getParameterNames(method);
        String parameterFullNames = "";
        if (parameterNames != null) {
            parameterFullNames = String.join(", ", parameterNames);
        }
        String fullMethodName = String.format("%s.%s(%s)", method.getDeclaringClass().getName(), method.getName(),
                String.join(", ", parameterFullNames));

        return new RequestInfo()
                .setThreadId(threadId)
                .setThreadName(threadName)
                .setUri(uri)
                .setClientIp(clientIp)
                .setUserAgent(userAgent)
                .setOs(os)
                .setBrowser(browser)
                .setMethodType(methodType)
                .setFullMethodName(fullMethodName);
    }

    /**
     * 判断是否为JSON请求
     *
     * @param request HttpServletRequest
     * @return 是否为JSON请求 [true: 是, false: 否]
     * @since 1.0.0
     */
    public static boolean isJsonRequest(@NonNull HttpServletRequest request) {
        Assert.notNull(request, "HttpServletRequest must not be null");

        String contentTypeInHeaderValue = request.getHeader(HttpHeaders.CONTENT_TYPE);
        return contentTypeInHeaderValue != null && contentTypeInHeaderValue.contains(MediaType.APPLICATION_JSON_VALUE);
    }
}
