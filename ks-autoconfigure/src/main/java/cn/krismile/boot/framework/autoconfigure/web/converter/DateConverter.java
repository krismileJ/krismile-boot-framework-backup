package cn.krismile.boot.framework.autoconfigure.web.converter;

import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.boot.framework.core.constant.PatternConstant;
import cn.krismile.boot.framework.core.util.inner.StringUtils;
import org.springframework.core.convert.converter.Converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Date自定义转换器
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public class DateConverter implements Converter<String, Date> {

    @Override
    public Date convert(@NonNull String s) {
        try {
            return new SimpleDateFormat(PatternConstant.Date.NORM_DATETIME_PATTERN).parse(s);
        } catch (ParseException e) {
            if (StringUtils.isParsable(s)) {
                return new Date((Long.parseLong(s)));
            }
            throw new RuntimeException(e);
        }
    }
}
