package cn.krismile.persagy.webstack.enumeration.business;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import com.baomidou.mybatisplus.annotation.IEnum;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 是否枚举
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public enum YesNoEnum implements BaseEnum<Integer>, IEnum<Integer> {

    /**
     * 是
     */
    YES(1, "是"),

    /**
     * 否
     */
    NO(0, "否");

    /**
     * 枚举值
     */
    private final Integer value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    YesNoEnum(Integer value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    /**
     * 获取 {@link String} 类型的 {@code value}
     *
     * @return {@link String} 类型的 {@code value}
     * @since 1.0.0
     */
    public String stringValue() {
        return String.valueOf(this.value);
    }

    @Override
    public Integer value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public Integer getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return stringValue();
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static YesNoEnum analyze(Integer value) {
        return BaseEnum.analyze(value, YesNoEnum.class);
    }
}
