package cn.krismile.boot.framework.autoconfigure;

import cn.krismile.boot.framework.autoconfigure.aop.properties.KsAopProperties;
import cn.krismile.boot.framework.autoconfigure.filter.properties.KsFilterProperties;
import cn.krismile.boot.framework.autoconfigure.web.properties.KsWebProperties;
import cn.krismile.boot.framework.core.constant.KrismileConstant;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * 标准配置文件
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = KrismileProperties.KEY)
public class KrismileProperties {

    /**
     * krismile 配置文件 Key
     */
    public static final String KEY = KrismileConstant.KRISMILE_LOWERCASE;

    /**
     * AOP配置文件
     */
    @NestedConfigurationProperty
    private KsAopProperties aop = new KsAopProperties();

    /**
     * Filter配置文件
     */
    @NestedConfigurationProperty
    private KsFilterProperties filter = new KsFilterProperties();

    /**
     * Web配置文件
     */
    @NestedConfigurationProperty
    private KsWebProperties web = new KsWebProperties();

}
