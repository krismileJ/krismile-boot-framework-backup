/**
 * [ks-core] 分页
 */
@NonNullApi
package cn.krismile.boot.framework.core.page;

import cn.krismile.boot.framework.core.annotation.jsr.NonNullApi;