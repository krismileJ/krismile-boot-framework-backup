package cn.krismile.persagy.webstack.function.business.mapper;

import cn.krismile.persagy.webstack.pojo.domain.BizWebsiteDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 业务-网站表 Mapper 接口
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Mapper
@Repository
public interface BizWebsiteMapper extends BaseMapper<BizWebsiteDO> {

    /**
     * 获取数据
     *
     * @param isLock    是否锁定
     * @param websiteId 网站ID
     * @return 数据
     * @since 1.0.0
     */
    BizWebsiteDO get(@Param("isLock") boolean isLock, @NonNull @Param("websiteId") Long websiteId);

    /**
     * 网站列表查询
     *
     * @param userId             用户ID
     * @param relNavIds          关联网站ID
     * @param websiteTitle       网站标题名称
     * @param websiteTagName     网站标签名称
     * @param websiteLink        网站链接地址
     * @param websiteDescription 网站描述信息
     * @return 网站列表
     * @since 1.0.0
     */
    List<BizWebsiteDO> list(@NonNull @Param("userId") Long userId,
                            @NonNull @Param("relNavIds") List<Long> relNavIds,
                            @Nullable @Param("websiteTitle") String websiteTitle,
                            @Nullable @Param("websiteTagName") String websiteTagName,
                            @Nullable @Param("websiteLink") String websiteLink,
                            @Nullable @Param("websiteDescription") String websiteDescription);

}
