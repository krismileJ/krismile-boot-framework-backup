package cn.krismile.boot.framework.cache;

import cn.krismile.boot.framework.core.annotation.jsr.NonNull;

import java.util.Set;

/**
 * 缓存提供者
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface CacheProvider {

    /**
     * 查询匹配的键
     *
     * @param pattern 正则表达式
     * @return 匹配的键集合
     * @since 1.0.0
     */
    @NonNull
    Set<String> keys(String pattern);

    /**
     * 设置缓存失效时间
     *
     * @param key        键
     * @param expireTime 过期时间, 单位 [秒]
     * @return 执行结果 [false: 失败, true: 成功]
     * @since 1.0.0
     */
    boolean expire(String key, long expireTime);

    /**
     * 查询键过期时间
     *
     * @param key 键
     * @return 过期时间, 单位 [秒], 不存在则小于 [0]
     * @since 1.0.0
     */
    long getExpire(String key);

    /**
     * 判断键是否存在
     *
     * @param key 键
     * @return 是否存在 [true: 存在, false: 不存在]
     * @since 1.0.0
     */
    boolean hasKey(String key);

    /**
     * 删除键
     *
     * @param key 键
     * @return 被删除的键数量 [&lt;0: 操作失败, =0: 未执行操作, &gt;0: 删除成功的键数量]
     * @since 1.0.0
     */
    long del(String... key);

}
