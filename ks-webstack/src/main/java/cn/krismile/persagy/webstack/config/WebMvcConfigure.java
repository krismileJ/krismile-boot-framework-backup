package cn.krismile.persagy.webstack.config;

import cn.krismile.persagy.webstack.config.intercept.AuthorizationIntercept;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.lang.NonNull;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * WebMvcConfigure
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@SpringBootConfiguration
public class WebMvcConfigure implements WebMvcConfigurer {

    @Resource
    private AuthorizationIntercept authorizationIntercept;

    @Override
    public void addInterceptors(@NonNull InterceptorRegistry registry) {
        registry.addInterceptor(authorizationIntercept)
                .excludePathPatterns("/doc.html", "/swagger-ui/**", "/webjars/**", "/v3/api-docs/**")
                .addPathPatterns("/**");
    }
}
