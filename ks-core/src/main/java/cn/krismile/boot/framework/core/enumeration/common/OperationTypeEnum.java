package cn.krismile.boot.framework.core.enumeration.common;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import com.fasterxml.jackson.annotation.JsonCreator;
import org.springframework.lang.Nullable;

import java.util.Objects;

/**
 * 操作类型枚举
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public enum OperationTypeEnum implements BaseEnum<String> {

    /**
     * 查询
     */
    QUERY("QUERY", "查询"),

    /**
     * 新增
     */
    ADD("ADD", "新增"),

    /**
     * 编辑
     */
    EDIT("EDIT", "编辑"),

    /**
     * 删除
     */
    DEL("DEL", "删除");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    OperationTypeEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    /**
     * 是否匹配
     *
     * @param matchEnums 匹配枚举
     * @return boolean
     * @since 1.0.0
     */
    public boolean isMatch(@Nullable OperationTypeEnum... matchEnums) {
        if (Objects.isNull(matchEnums)) {
            return false;
        }
        for (OperationTypeEnum matchEnum : matchEnums) {
            if (this == matchEnum) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static OperationTypeEnum analyze(String value) {
        return BaseEnum.analyze(value, OperationTypeEnum.class);
    }
}
