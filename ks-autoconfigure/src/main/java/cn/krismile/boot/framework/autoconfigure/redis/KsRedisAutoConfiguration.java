package cn.krismile.boot.framework.autoconfigure.redis;

import cn.krismile.boot.framework.autoconfigure.redis.properties.KsRedisProperties;
import cn.krismile.boot.framework.cache.redis.RedisCache;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * Cache自动配置
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Configuration(proxyBeanMethods = false)
@AutoConfigureBefore(RedisAutoConfiguration.class)
@AutoConfigureAfter(JacksonAutoConfiguration.class)
@ConditionalOnClass(RedisOperations.class)
@EnableConfigurationProperties({
        KsRedisProperties.class
})
@ConditionalOnProperty(
        prefix = KsRedisProperties.KEY,
        name = "enable",
        havingValue = "true"
)
public class KsRedisAutoConfiguration {

    public static final String KEY_SERIALIZER_BEAN_NAME = "keySerializer";
    public static final String VALUE_SERIALIZER_BEAN_NAME = "valueSerializer";
    public static final String CACHE_BEAN_NAME = "ks_redisCache";

    @Bean
    @ConditionalOnMissingBean
    public RedisTemplate<String, Object> redisTemplate(
            RedisConnectionFactory connectionFactory,
            @Qualifier(KEY_SERIALIZER_BEAN_NAME) RedisSerializer<String> keySerializer,
            @Qualifier(VALUE_SERIALIZER_BEAN_NAME) RedisSerializer<Object> valueSerializer) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(connectionFactory);

        redisTemplate.setKeySerializer(keySerializer);
        redisTemplate.setValueSerializer(valueSerializer);

        redisTemplate.setHashKeySerializer(keySerializer);
        redisTemplate.setHashValueSerializer(valueSerializer);
        return redisTemplate;
    }

    @Bean
    @ConditionalOnMissingBean
    public StringRedisTemplate stringRedisTemplate(
            RedisConnectionFactory redisConnectionFactory,
            @Qualifier(KEY_SERIALIZER_BEAN_NAME) RedisSerializer<String> keySerializer,
            @Qualifier(VALUE_SERIALIZER_BEAN_NAME) RedisSerializer<Object> valueSerializer) {
        StringRedisTemplate stringRedisTemplate = new StringRedisTemplate(redisConnectionFactory);

        stringRedisTemplate.setKeySerializer(keySerializer);
        stringRedisTemplate.setValueSerializer(valueSerializer);

        stringRedisTemplate.setHashKeySerializer(keySerializer);
        stringRedisTemplate.setHashValueSerializer(valueSerializer);
        return stringRedisTemplate;
    }

    @Bean(KEY_SERIALIZER_BEAN_NAME)
    @ConditionalOnMissingBean(name = KEY_SERIALIZER_BEAN_NAME)
    public RedisSerializer<String> keySerializer() {
        return RedisSerializer.string();
    }

    @Bean(VALUE_SERIALIZER_BEAN_NAME)
    @ConditionalOnMissingBean(name = VALUE_SERIALIZER_BEAN_NAME)
    public RedisSerializer<Object> valueSerializer(ObjectProvider<ObjectMapper> objectMapperProvider) {
        return new GenericJackson2JsonRedisSerializer(objectMapperProvider.getIfAvailable(ObjectMapper::new).copy());
    }

    @Bean(CACHE_BEAN_NAME)
    @ConditionalOnClass(RedisCache.class)
    public RedisCache redisCache(RedisTemplate<String, Object> redisTemplate) {
        return new RedisCache(redisTemplate);
    }
}
