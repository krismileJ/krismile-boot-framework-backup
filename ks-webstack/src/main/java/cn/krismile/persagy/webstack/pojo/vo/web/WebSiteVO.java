package cn.krismile.persagy.webstack.pojo.vo.web;

import lombok.Data;

/**
 * 网站DTO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
public class WebSiteVO {

    /**
     * ID
     */
    private Long id;

    /**
     * 标题
     */
    private String title;

    /**
     * 描述信息
     */
    private String description;

    /**
     * 标签名称
     */
    private String tagName;

    /**
     * 图标图片
     */
    private String image;

    /**
     * 链接地址
     */
    private String link;

}
