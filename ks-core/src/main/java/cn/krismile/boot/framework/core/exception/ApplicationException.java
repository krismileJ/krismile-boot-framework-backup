package cn.krismile.boot.framework.core.exception;

import cn.krismile.boot.framework.core.annotation.jsr.Nullable;
import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import cn.krismile.boot.framework.core.util.inner.StringUtils;

/**
 * 全局自定义异常基类
 *
 * <p>该类为全局自定义异常基类, 为避免部分功能失效, 项目中所有的自定义异常原则上都必须继承该类
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public class ApplicationException extends RuntimeException {

    private static final long serialVersionUID = 5813304139081298680L;
    public static final String DEFAULT_ERROR_USER_TIP = "网络开小差了, 请稍后再试 (╯﹏╰)";

    /**
     * 错误码
     */
    protected final String errorCode;

    /**
     * 错误信息
     */
    protected final String errorMessage;

    /**
     * 用户提示信息
     */
    protected final String userTip;

    /**
     * 错误枚举
     */
    protected final BaseEnum<String> baseEnum;

    /**
     * 构造器
     *
     * @param baseEnum 枚举基类
     * @since 1.0.0
     */
    public ApplicationException(BaseEnum<String> baseEnum) {
        super("errorCode: " + baseEnum.value() + ", errorMessage: " + baseEnum.getReasonPhrase());
        this.errorCode = baseEnum.value();
        this.errorMessage = baseEnum.getReasonPhrase();
        this.baseEnum = baseEnum;
        userTip = DEFAULT_ERROR_USER_TIP;
    }

    /**
     * 构造器
     *
     * @param baseEnum 枚举基类
     * @param userTip  用户提示信息
     * @since 1.0.0
     */
    public ApplicationException(BaseEnum<String> baseEnum, @Nullable String userTip) {
        super("errorCode: " + baseEnum.value() + ", errorMessage: " + baseEnum.getReasonPhrase() + ", userTip: " + userTip);
        this.errorCode = baseEnum.value();
        this.errorMessage = baseEnum.getReasonPhrase();
        this.userTip = StringUtils.defaultString(userTip, DEFAULT_ERROR_USER_TIP);
        this.baseEnum = baseEnum;
    }

    /**
     * 构造器
     *
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     * @param userTip      用户提示信息
     * @since 1.0.0
     */
    public ApplicationException(String errorCode, String errorMessage, @Nullable String userTip) {
        super("errorCode: " + errorCode + ", errorMessage: " + errorMessage + ", userTip: " + userTip);
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.userTip = StringUtils.defaultString(userTip, DEFAULT_ERROR_USER_TIP);
        this.baseEnum = new BaseEnum<String>() {
            @Override
            public String value() {
                return errorCode;
            }

            @Override
            public String getReasonPhrase() {
                return errorMessage;
            }
        };
    }

    public Object getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getUserTip() {
        return userTip;
    }

    public BaseEnum<String> getBaseEnum() {
        return baseEnum;
    }
}
