package cn.krismile.boot.framework.core.exception.server;

import cn.krismile.boot.framework.core.enumeration.error.server.DisasterRecoveryErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 系统容灾功能被触发自定义异常
 *
 * @author JiYinchuan
 * @see DisasterRecoveryErrorEnum
 * @since 1.0.0
 */
public class DisasterRecoveryException extends ApplicationException {

    private static final long serialVersionUID = 5492105604800468837L;

    public DisasterRecoveryException(DisasterRecoveryErrorEnum disasterRecoveryErrorEnum) {
        super(disasterRecoveryErrorEnum);
    }

    public DisasterRecoveryException(DisasterRecoveryErrorEnum disasterRecoveryErrorEnum, String userTip) {
        super(disasterRecoveryErrorEnum, userTip);
    }
}