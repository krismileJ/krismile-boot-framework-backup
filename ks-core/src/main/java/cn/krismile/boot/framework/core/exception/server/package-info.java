/**
 * [ks-core] 异常-服务端
 */
@NonNullApi
@NonNullFields
package cn.krismile.boot.framework.core.exception.server;

import cn.krismile.boot.framework.core.annotation.jsr.NonNullApi;
import cn.krismile.boot.framework.core.annotation.jsr.NonNullFields;