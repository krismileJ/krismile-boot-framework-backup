package cn.krismile.boot.framework.autoconfigure.cache.properties;

import cn.krismile.boot.framework.autoconfigure.KrismileProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 缓存配置文件
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@ConfigurationProperties(prefix = KsCacheProperties.KEY)
public class KsCacheProperties {

    public static final String KEY = KrismileProperties.KEY + "." + "cache";

}
