package cn.krismile.persagy.webstack.function.business.service;

import cn.krismile.boot.framework.core.exception.client.AuthorityException;
import cn.krismile.boot.framework.core.exception.client.RequestServerException;
import cn.krismile.boot.framework.mybatisplus.exception.MybatisServiceException;
import cn.krismile.boot.framework.mybatisplus.service.BaseService;
import cn.krismile.persagy.webstack.pojo.domain.BizNavDO;
import cn.krismile.persagy.webstack.pojo.dto.NavDTO;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.List;

/**
 * 业务-导航栏表 Service
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface BizNavService extends BaseService<BizNavDO> {

    String DEFAULT_NOT_FOUND_MESSAGE = "未找到导航栏";

    // ------------------------- 查询相关 -------------------------

    /**
     * 导航栏列表实时查询
     *
     * @return 导航列表实时数据
     * @throws RequestServerException 用户ID获取失败
     * @since 1.0.0
     */
    List<NavDTO> listByUser() throws RequestServerException;

    /**
     * 内置导航栏数据列表查询
     *
     * @return 内置导航栏数据列表
     * @since 1.0.0
     */
    List<NavDTO> listByInternal();

    /**
     * 用户公开导航栏数据列表查询
     *
     * @return 用户公开导航栏数据列表
     * @since 1.0.0
     */
    List<NavDTO> listByPublic();

    // ------------------------- 操作相关 -------------------------

    /**
     * 导航栏添加
     *
     * @param dto 导航DTO
     * @return 操作是否成功
     * @throws MybatisServiceException 无效的父导航栏
     * @throws RequestServerException  用户ID获取失败
     * @throws AuthorityException      访问未授权, 当新增导航栏为内置时进行判断
     * @since 1.0.0
     */
    boolean add(@NonNull NavDTO dto) throws MybatisServiceException, RequestServerException, AuthorityException;

    /**
     * 导航栏编辑
     *
     * @param dto 导航DTO
     * @return 操作是否成功
     * @throws MybatisServiceException 无效的父导航栏
     * @throws RequestServerException  用户ID获取失败
     * @throws AuthorityException      访问未授权, 当编辑导航栏为内置时进行判断
     * @since 1.0.0
     */
    boolean edit(@NonNull NavDTO dto) throws MybatisServiceException, RequestServerException, AuthorityException;

    /**
     * 导航栏删除
     *
     * @param ids 导航ID
     * @return 操作是否成功
     * @throws RequestServerException 用户ID获取失败
     * @throws AuthorityException     访问未授权, 当删除导航栏为内置时进行判断
     * @since 1.0.0
     */
    boolean del(@Nullable List<Long> ids) throws RequestServerException, AuthorityException;

}
