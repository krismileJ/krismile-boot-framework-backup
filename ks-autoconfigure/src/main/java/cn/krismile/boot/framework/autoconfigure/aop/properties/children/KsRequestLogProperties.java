package cn.krismile.boot.framework.autoconfigure.aop.properties.children;

import cn.krismile.boot.framework.autoconfigure.aop.properties.KsAopProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpHeaders;

/**
 * 请求日志配置文件
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = KsRequestLogProperties.KEY)
public class KsRequestLogProperties {

    public static final String KEY = KsAopProperties.KEY + "." + "request-log";

    /**
     * 是否启用
     */
    private boolean enable = true;

    /**
     * DEBUG模式下打印的请求头名称数组, 忽略大小写, 为 {@code null} 时将打印所有请求头
     */
    private String[] debugPrintHeaderNames = {HttpHeaders.ACCEPT, HttpHeaders.CONTENT_TYPE};

}
