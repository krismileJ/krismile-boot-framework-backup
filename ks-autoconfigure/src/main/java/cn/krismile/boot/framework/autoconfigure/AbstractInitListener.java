package cn.krismile.boot.framework.autoconfigure;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;

/**
 * 初始化配置抽象监听器
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public abstract class AbstractInitListener implements ApplicationListener<ApplicationReadyEvent> {
}
