/**
 * [ks-core] 异常-第三方
 */
@NonNullApi
@NonNullFields
package cn.krismile.boot.framework.core.exception.third;

import cn.krismile.boot.framework.core.annotation.jsr.NonNullApi;
import cn.krismile.boot.framework.core.annotation.jsr.NonNullFields;