package cn.krismile.boot.framework.test.service;

import org.springframework.stereotype.Service;

/**
 * TODO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Service
public abstract class BaseService<T> {

    public abstract void query(T testDTO);
}
