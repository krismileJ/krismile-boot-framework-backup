package cn.krismile.boot.framework.mybatisplus.enumeration.error;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * MybatisPlus代码生成异常枚举
 *
 * <p>该类为 [MybatisPlus] 代码生成异常枚举
 *
 * @author JiYinchuan
 * @see cn.krismile.boot.framework.mybatisplus.exception.MybatisGenerateException
 * @since 1.0.0
 */
public enum MybatisGenerateErrorEnum implements BaseEnum<String> {

    /**
     * Mybatis代码生成异常
     */
    MYBATIS_CODE_GENERATE_ERROR("M0200", "Mybatis代码生成异常"),

    /**
     * 基本配置异常
     */
    BASIC_ERROR("M0201", "基本配置异常"),

    /**
     * 数据源配置异常
     */
    DATA_SOURCE_ERROR("M0202", "数据源配置异常"),

    /**
     * 数据源URL为空
     */
    DATA_SOURCE_URL_IS_BLANK("M0203", "数据源URL为空"),

    /**
     * 数据源用户名为空
     */
    DATA_SOURCE_USERNAME_IS_BLANK("M0204", "数据源用户名为空"),

    /**
     * 数据源密码为空
     */
    DATA_SOURCE_PASSWORD_IS_BLANK("M0205", "数据源密码为空"),

    /**
     * 包配置异常
     */
    PACKAGE_ERROR("M0206", "包配置异常"),

    /**
     * 输出包名
     */
    OUTPUT_PACKAGE_IS_BLANK("M0207", "输出包名为空"),

    /**
     * 父包模块名为空
     */
    PACKAGE_PARENT_MODEL_NAME_IS_BLANK("M0208", "父包模块名为空");


    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    MybatisGenerateErrorEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static MybatisGenerateErrorEnum analyze(String value) {
        return BaseEnum.analyze(value, MybatisGenerateErrorEnum.class);
    }
}
