package cn.krismile.boot.framework.core.exception.server;

import cn.krismile.boot.framework.core.enumeration.error.server.ExecutionErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 系统执行超时自定义异常
 *
 * @author JiYinchuan
 * @see ExecutionErrorEnum
 * @since 1.0.0
 */
public class ExecutionException extends ApplicationException {

    private static final long serialVersionUID = 6555713300011794654L;

    public ExecutionException(ExecutionErrorEnum executionErrorEnum) {
        super(executionErrorEnum);
    }

    public ExecutionException(ExecutionErrorEnum executionErrorEnum, String userTip) {
        super(executionErrorEnum, userTip);
    }
}