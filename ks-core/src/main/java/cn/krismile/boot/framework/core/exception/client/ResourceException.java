package cn.krismile.boot.framework.core.exception.client;

import cn.krismile.boot.framework.core.enumeration.error.client.ResourceErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 用户资源自定义异常
 *
 * @author JiYinchuan
 * @see ResourceErrorEnum
 * @since 1.0.0
 */
public class ResourceException extends ApplicationException {

    private static final long serialVersionUID = 2109523700169696145L;

    public ResourceException(ResourceErrorEnum resourceErrorEnum) {
        super(resourceErrorEnum);
    }

    public ResourceException(ResourceErrorEnum resourceErrorEnum, String userTip) {
        super(resourceErrorEnum, userTip);
    }
}
