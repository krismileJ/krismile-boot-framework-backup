package cn.krismile.boot.framework.core.exception.client;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 用户登录自定义异常
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public class LoginException extends ApplicationException {

    private static final long serialVersionUID = -4030434878358630752L;

    public LoginException(BaseEnum<String> baseEnum) {
        super(baseEnum);
    }

    public LoginException(BaseEnum<String> baseEnum, String userTip) {
        super(baseEnum, userTip);
    }
}
