package cn.krismile.boot.framework.test.service;

import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

/**
 * TODO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Validated
public interface TestService<T> {

    void test(@Valid T testDTO, List<Object> list);

    default Object test() {
        test(null, null);
        return null;
    };

}
