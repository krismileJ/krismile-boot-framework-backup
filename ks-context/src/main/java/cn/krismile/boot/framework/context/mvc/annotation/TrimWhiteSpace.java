package cn.krismile.boot.framework.context.mvc.annotation;

import cn.krismile.boot.framework.context.mvc.deserialization.TrimWhiteSpaceDeserialization;
import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.lang.annotation.*;

/**
 * 清空前后空格注解
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD})
@Documented
@JacksonAnnotationsInside
@JsonDeserialize(using = TrimWhiteSpaceDeserialization.class)
public @interface TrimWhiteSpace {
}
