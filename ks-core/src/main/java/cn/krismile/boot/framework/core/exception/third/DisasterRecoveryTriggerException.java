package cn.krismile.boot.framework.core.exception.third;

import cn.krismile.boot.framework.core.enumeration.error.third.DisasterRecoveryTriggerErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 第三方容灾系统被触发自定义异常
 *
 * @author JiYinchuan
 * @see DisasterRecoveryTriggerErrorEnum
 * @since 1.0.0
 */
public class DisasterRecoveryTriggerException extends ApplicationException {

    private static final long serialVersionUID = 2555916619627569847L;

    public DisasterRecoveryTriggerException(DisasterRecoveryTriggerErrorEnum disasterRecoveryTriggerErrorEnum) {
        super(disasterRecoveryTriggerErrorEnum);
    }

    public DisasterRecoveryTriggerException(DisasterRecoveryTriggerErrorEnum disasterRecoveryTriggerErrorEnum, String userTip) {
        super(disasterRecoveryTriggerErrorEnum, userTip);
    }
}
