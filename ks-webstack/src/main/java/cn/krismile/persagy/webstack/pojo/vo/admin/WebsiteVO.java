package cn.krismile.persagy.webstack.pojo.vo.admin;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 网站VO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@Accessors(chain = true)
@Schema(title = "网站VO")
public class WebsiteVO {

    @Schema(description = "网站ID")
    private Long websiteId;

    @Schema(description = "网站名称")
    private String websiteTitle;

    @Schema(description = "网站缩略图")
    private String websiteImage;

    @Schema(description = "网站链接地址")
    private String websiteLink;

    @Schema(description = "网站标签名称")
    private String websiteTagName;

    @Schema(description = "网站描述信息")
    private String websiteDescription;

    @Schema(description = "网站是否为内置")
    private Boolean websiteInternal;

    @Schema(description = "网站排序权重, 默认值为 [0]")
    private Integer websiteOrderWeight;

}
