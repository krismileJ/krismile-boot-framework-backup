package cn.krismile.persagy.webstack.function.system.service.impl;

import cn.krismile.persagy.webstack.pojo.domain.SysDepartmentDO;
import cn.krismile.persagy.webstack.function.system.mapper.SysDepartmentMapper;
import cn.krismile.persagy.webstack.function.system.service.SysDepartmentService;
import cn.krismile.boot.framework.mybatisplus.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 系统-部门表 ServiceImpl
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Service
public class SysDepartmentServiceImpl extends BaseServiceImpl<SysDepartmentMapper, SysDepartmentDO> implements SysDepartmentService {

}
