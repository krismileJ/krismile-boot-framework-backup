package cn.krismile.boot.framework.core.response.vo;

import cn.krismile.boot.framework.core.response.ResultVO;

import java.io.Serializable;

/**
 * 分页详情VO
 *
 * <p>该类为 {@link PageVO} 中分页详情数据抽离, 禁止直接使用此类用于返回, 需要配合 {@link PageVO}, 并通过 {@link ResultVO} 进行返回
 *
 * @author JiYinchuan
 * @see PageVO
 * @since 1.0.0
 */
public final class PageDetailVO implements Serializable {

    private static final long serialVersionUID = 8140126980707273057L;

    /**
     * 页码
     */
    private Long pageNo;

    /**
     * 每页展示数量
     */
    private Long pageSize;

    /**
     * 数据总条数
     */
    private Long totalCount;

    public PageDetailVO(long pageNo, long pageSize, long totalCount) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.totalCount = totalCount;
    }

    public long getPageNo() {
        return pageNo;
    }

    public void setPageNo(long pageNo) {
        this.pageNo = pageNo;
    }

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    @Override
    public String toString() {
        return "PageDetailVO{" +
                "pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", totalCount=" + totalCount +
                '}';
    }
}
