package cn.krismile.persagy.webstack.controller;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import cn.krismile.persagy.webstack.function.business.service.BizWebsiteService;
import cn.krismile.persagy.webstack.pojo.domain.BizWebsiteDO;
import cn.krismile.boot.framework.core.query.PageQuery ;
import cn.krismile.boot.framework.core.response.ResultVO;
import cn.krismile.boot.framework.core.response.vo.BaseVO;
import cn.krismile.boot.framework.core.response.vo.PageVO;
import cn.krismile.boot.framework.core.response.vo.SingleVO;

import java.util.List;

/**
 * 业务-网站表
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Tag(name = "[XXX] 业务-网站表")
@RestController
@RequestMapping("/webstack/bizWebsiteDO")
public class BizWebsiteController {

    @Autowired
    private BizWebsiteService bizWebsiteService;

    @Operation(summary = "[查询] 单条数据")
    @GetMapping("/{id}")
    public SingleVO<BizWebsiteDO> get(@PathVariable("id") Long id) {
        return ResultVO.okSingle(bizWebsiteService.getByIdValidate(id));
    }

    @Operation(summary = "[查询] 分页数据")
    @GetMapping("/page")
    public PageVO<BizWebsiteDO> page(@ModelAttribute PageQuery pageQuery) {
        return ResultVO.okPage(bizWebsiteService.pageValidate(pageQuery));
    }

    @Operation(summary = "[新增] 单条数据")
    @PostMapping
    public BaseVO add(@RequestBody @Validated BizWebsiteDO bizWebsite) {
        return ResultVO.ok(() -> bizWebsiteService.saveValidate(bizWebsite));
    }

    @Operation(summary = "[更新] 单条数据")
    @PutMapping
    public BaseVO edit(@RequestBody @Validated BizWebsiteDO bizWebsite) {
        return ResultVO.ok(() -> bizWebsiteService.updateByIdValidate(bizWebsite));
    }

    @Operation(summary = "[删除] 批量数据")
    @DeleteMapping
    public BaseVO del(@RequestBody List<Long> ids) {
        return ResultVO.ok(() -> bizWebsiteService.removeByIdValidate(ids));
    }
}

