package cn.krismile.boot.framework.autoconfigure.filter.properties;

import cn.krismile.boot.framework.autoconfigure.KrismileProperties;
import cn.krismile.boot.framework.autoconfigure.filter.properties.children.KsRequestBodyWrapperProperties;
import cn.krismile.boot.framework.autoconfigure.filter.properties.children.KsXssProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * Filter配置文件
 *
 * <pre>
 *     # ------------------------- KrismileConfig -------------------------
 *     krismile:
 *       filter:
 *         # 请求体包装过滤器
 *         request-body-wrapper:
 *           # 是否启用
 *           enable: true
 *         # XSS攻击防御配置
 *         xss:
 *           enable: true
 * </pre>
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = KsFilterProperties.KEY)
public class KsFilterProperties {

    public static final String KEY = KrismileProperties.KEY + "." + "filter";

    /**
     * 是否启用请求体包装过滤器自动配置
     */
    @NestedConfigurationProperty
    private KsRequestBodyWrapperProperties requestBodyWrapper = new KsRequestBodyWrapperProperties();

    @NestedConfigurationProperty
    private KsXssProperties xss = new KsXssProperties();

}
