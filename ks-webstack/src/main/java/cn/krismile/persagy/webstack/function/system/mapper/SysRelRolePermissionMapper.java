package cn.krismile.persagy.webstack.function.system.mapper;

import cn.krismile.persagy.webstack.pojo.domain.SysRelRolePermissionDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 系统-角色-权限关联表 Mapper 接口
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Mapper
@Repository
public interface SysRelRolePermissionMapper extends BaseMapper<SysRelRolePermissionDO> {

}
