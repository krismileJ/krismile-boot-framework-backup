package cn.krismile.persagy.webstack.function.business.service.impl;

import cn.krismile.persagy.webstack.function.business.service.BizNavService;
import cn.krismile.persagy.webstack.function.business.service.BizWebsiteService;
import cn.krismile.persagy.webstack.function.business.service.WebService;
import cn.krismile.persagy.webstack.pojo.dto.NavDTO;
import cn.krismile.persagy.webstack.pojo.dto.WebsiteDTO;
import cn.krismile.persagy.webstack.pojo.vo.web.NavVO;
import cn.krismile.persagy.webstack.pojo.vo.web.WebSiteVO;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 页面服务实现
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Service
public class WebServiceImpl implements WebService {

    @Resource
    private BizNavService navService;
    @Resource
    private BizWebsiteService websiteService;

    @Override
    @Cacheable(cacheNames = "web:list", key = "#{T(cn.krismile.persagy.webstack.key.CacheKey).SPEL_JWT_USER_ID + '-1d'}")
    public List<NavVO> listCache() {
        // 查询用户的导航栏列表
        List<NavDTO> dbNavs = navService.listByUser();
        // 查询用户的网站列表
        List<Long> dbNavIds = dbNavs.stream().map(NavDTO::getNavId).collect(Collectors.toList());
        List<WebsiteDTO> dbWebsites = websiteService.listByUser(dbNavIds);
        // 解析导航栏和网站列表
        return analyze(dbNavs, dbWebsites);
    }

    /**
     * 导航栏VO数据解析
     *
     * @param navs     导航栏数据
     * @param websites 网站数据
     * @return 导航栏VO数据
     * @since 1.0.0
     */
    private List<NavVO> analyze(@Nullable List<NavDTO> navs, @NonNull List<WebsiteDTO> websites) {
        List<NavVO> result = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(navs)) {
            websites = ListUtils.emptyIfNull(websites);
            // 导航栏ID -> 网站数据
            Map<Long, List<WebsiteDTO>> navIdAndWebsites = websites.stream()
                    .collect(Collectors.groupingBy(WebsiteDTO::getRelNavId));
            for (NavDTO nav : navs) {
                Long navId = nav.getNavId();

                NavVO resultItem = new NavVO();
                resultItem.setId(navId);
                resultItem.setCls(nav.getNavIcon());
                resultItem.setTitle(nav.getNavTitle());
                resultItem.setTagName(nav.getNavTagName());
                // 设置解析的网站数据
                resultItem.setWebSites(analyze(navIdAndWebsites.get(navId)));
                // 如果存在子集则递归解析
                resultItem.setChildren(analyze(nav.getNavChildren(), websites));
                result.add(resultItem);
            }
        }
        return result;
    }

    /**
     * 网站VO数据解析
     *
     * @param websites 网站数据
     * @return 网站VO数据
     * @since 1.0.0
     */
    private List<WebSiteVO> analyze(@Nullable List<WebsiteDTO> websites) {
        List<WebSiteVO> result = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(websites)) {
            for (WebsiteDTO website : websites) {
                WebSiteVO resultItem = new WebSiteVO();
                resultItem.setId(website.getWebsiteId());
                resultItem.setTitle(website.getWebsiteTitle());
                resultItem.setDescription(website.getWebsiteDescription());
                resultItem.setTagName(website.getWebsiteTagName());
                resultItem.setImage(website.getWebsiteImage());
                resultItem.setLink(website.getWebsiteLink());
                result.add(resultItem);
            }
        }
        return result;
    }
}
