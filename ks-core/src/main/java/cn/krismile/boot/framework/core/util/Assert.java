package cn.krismile.boot.framework.core.util;

import cn.krismile.boot.framework.core.annotation.jsr.Nullable;

/**
 * 验证参数断言类, 有助于在运行时更早地识别程序员错误
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public class Assert {

    /**
     * 断言对象不为 {@code null}
     *
     * @param object       断言的对象
     * @param errorMessage 错误信息
     * @since 1.0.0
     */
    public static void notNull(@Nullable Object object, String errorMessage) {
        if (object == null) {
            throw new IllegalArgumentException(errorMessage);
        }
    }
}
