package cn.krismile.boot.framework.core.constant.key;

/**
 * 上下文Key
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface ContextKey {

    /**
     * 自定义请求信息
     */
    String DIV_REQUEST_INFO = "KS_REQUEST_INFO";

}
