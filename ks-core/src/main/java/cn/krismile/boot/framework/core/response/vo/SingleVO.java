package cn.krismile.boot.framework.core.response.vo;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import cn.krismile.boot.framework.core.response.ResultVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;

/**
 * 单条数据VO
 *
 * <p>该类为单条数据VO, 用于单条数据返回的情况下使用, 使用 {@link ResultVO} 中相关方法进行返回
 *
 * @author JiYinchuan
 * @see ResultVO
 * @since 1.0.0
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public final class SingleVO<T> extends BaseVO implements Serializable {

    private static final long serialVersionUID = -7717533595279231188L;

    /**
     * 数据
     */
    private T data;

    /**
     * 构造器
     *
     * @param baseEnum 响应枚举
     * @param data     数据
     * @since 1.0.0
     */
    public SingleVO(BaseEnum<String> baseEnum, T data) {
        super(baseEnum);
        this.data = data;
    }

    /**
     * 构造器
     *
     * @param baseEnum 响应枚举
     * @param userTip  用户提示信息
     * @param data     数据
     * @since 1.0.0
     */
    public SingleVO(BaseEnum<String> baseEnum, String userTip, T data) {
        super(baseEnum, userTip);
        this.data = data;
    }

    /**
     * 构造器
     *
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     * @param data         数据
     * @since 1.0.0
     */
    public SingleVO(String errorCode, String errorMessage, T data) {
        super(errorCode, errorMessage);
        this.data = data;
    }

    /**
     * 构造器
     *
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     * @param userTip      用户提示信息
     * @param data         数据
     * @since 1.0.0
     */
    public SingleVO(String errorCode, String errorMessage, String userTip, T data) {
        super(errorCode, errorMessage, userTip);
        this.data = data;
    }
}
