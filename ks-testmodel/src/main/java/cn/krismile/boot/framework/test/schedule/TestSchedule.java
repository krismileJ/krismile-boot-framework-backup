// package cn.krismile.boot.framework.test.schedule;
//
// import cn.krismile.boot.framework.test.service.TestService;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.scheduling.annotation.Scheduled;
// import org.springframework.stereotype.Component;
// import org.springframework.web.bind.annotation.RequestMapping;
//
// import java.util.concurrent.atomic.AtomicInteger;
//
// /**
//  * TODO
//  *
//  * @author JiYinchuan
//  * @since 1.0.0
//  */
// @Component
// public class TestSchedule {
//
//     private static final AtomicInteger VALUE = new AtomicInteger();
//
//     @Autowired
//     private TestService testService;
//
//     @Scheduled(cron = "* * * * * ?")
//     public void printSchedule() {
//         System.out.println(VALUE.getAndIncrement());
//         System.out.println(testService.test());
//     }
//
//     @RequestMapping
//     public void t() {
//         System.out.println("TTT");
//     }
// }
