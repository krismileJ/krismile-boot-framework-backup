package cn.krismile.persagy.webstack.function.business.service.impl;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import cn.krismile.boot.framework.core.enumeration.common.OperationTypeEnum;
import cn.krismile.boot.framework.core.enumeration.error.client.AuthorityErrorEnum;
import cn.krismile.boot.framework.core.exception.EnumIllegalArgumentException;
import cn.krismile.boot.framework.core.exception.client.AuthorityException;
import cn.krismile.boot.framework.core.exception.client.RequestServerException;
import cn.krismile.boot.framework.core.execute.ObjectExecute;
import cn.krismile.boot.framework.core.util.Assert;
import cn.krismile.boot.framework.mybatisplus.exception.MybatisServiceException;
import cn.krismile.boot.framework.mybatisplus.service.BaseServiceImpl;
import cn.krismile.persagy.webstack.convert.WebsiteConvert;
import cn.krismile.persagy.webstack.enumeration.business.ResourceStatusEnum;
import cn.krismile.persagy.webstack.enumeration.business.ResourceTypeEnum;
import cn.krismile.persagy.webstack.function.business.mapper.BizWebsiteMapper;
import cn.krismile.persagy.webstack.function.business.service.BizRelNavWebsiteSysAdminService;
import cn.krismile.persagy.webstack.function.business.service.BizRelWebsiteSysAdminService;
import cn.krismile.persagy.webstack.function.business.service.BizWebsiteService;
import cn.krismile.persagy.webstack.function.system.service.SysAdminService;
import cn.krismile.persagy.webstack.pojo.context.UserInfoContext;
import cn.krismile.persagy.webstack.pojo.domain.BizWebsiteDO;
import cn.krismile.persagy.webstack.pojo.domain.SysAdminDO;
import cn.krismile.persagy.webstack.pojo.dto.WebsiteDTO;
import cn.krismile.persagy.webstack.pojo.query.WebsiteQuery;
import cn.krismile.persagy.webstack.validate.ValidateGroup;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.validation.Validator;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * 业务-网站表 ServiceImpl
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Service
@Transactional(readOnly = true)
public class BizWebsiteServiceImpl extends BaseServiceImpl<BizWebsiteMapper, BizWebsiteDO> implements BizWebsiteService {

    @Resource
    private Validator validator;
    @Resource
    private SysAdminService adminService;
    @Resource
    private BizRelWebsiteSysAdminService relWebsiteSysAdminService;
    @Resource
    private BizRelNavWebsiteSysAdminService relNavWebsiteSysAdminService;

    @Override
    public List<WebsiteDTO> listByUser(@NonNull List<Long> relNavIds) throws RequestServerException {
        return WebsiteConvert.INSTANCE.toDto(getBaseMapper().list(UserInfoContext.userId(), relNavIds,
                null, null, null, null));
    }

    @Override
    public List<WebsiteDTO> listByUser(@NonNull WebsiteQuery query) throws RequestServerException {
        return WebsiteConvert.INSTANCE.toDto(getBaseMapper().list(
                UserInfoContext.userId(), Collections.singletonList(query.getRelNavId()), query.getWebsiteTitle(),
                query.getWebsiteTagName(), query.getWebsiteLink(), query.getWebsiteDescription()));
    }

    @Override
    public List<WebsiteDTO> listByInternal() {
        return WebsiteConvert.INSTANCE.toDto(lambdaQuery()
                .eq(BizWebsiteDO::getStatus, ResourceStatusEnum.APPROVED)
                .eq(BizWebsiteDO::getType, ResourceTypeEnum.INTERNAL).list());
    }

    @Override
    public List<WebsiteDTO> listByPublic() {
        return WebsiteConvert.INSTANCE.toDto(lambdaQuery()
                .eq(BizWebsiteDO::getStatus, ResourceStatusEnum.APPROVED)
                .eq(BizWebsiteDO::getType, ResourceTypeEnum.PUBLIC).list());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean add(@NonNull WebsiteDTO dto)
            throws MybatisServiceException, RequestServerException, AuthorityException, EnumIllegalArgumentException {
        validator.validate(dto, ValidateGroup.Update.class);
        // 检查操作权限
        checkPermission(OperationTypeEnum.ADD, null, dto.getWebsiteType());

        // 新增网站
        BizWebsiteDO addWebsite = new BizWebsiteDO();
        fillData(dto, addWebsite);
        // 解析网站类型并设置网站状态
        addWebsite.setStatus(analyzeStatus(dto.getWebsiteType()));
        addWebsite.setRelCreateAdminId(UserInfoContext.userId());
        checkOperation(saveValidate(addWebsite));

        // 新增网站用户关联
        checkOperation(relWebsiteSysAdminService.relation(addWebsite.getId(), dto.getWebsiteOrderWeight()));

        // 新增导航栏网站关联
        return relNavWebsiteSysAdminService.relation(dto.getRelNavId(), addWebsite.getId());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean edit(@NonNull WebsiteDTO dto)
            throws MybatisServiceException, RequestServerException, AuthorityException, EnumIllegalArgumentException {
        validator.validate(dto, ValidateGroup.Update.class);

        BizWebsiteDO editWebsite = getBaseMapper().get(true, dto.getWebsiteId());
        // 检查操作权限
        checkPermission(OperationTypeEnum.EDIT, editWebsite.getId(), editWebsite.getType());
        // 当修改了排序权重时进行更新排序权重
        if (!Objects.equals(dto.getWebsiteOrderWeight(), editWebsite.getOrderWeight())) {
            checkOperation(relWebsiteSysAdminService.editSort(editWebsite.getId(), dto.getWebsiteOrderWeight()));
        }
        fillData(dto, editWebsite);
        // 解析网站类型并设置网站状态
        editWebsite.setStatus(analyzeStatus(dto.getWebsiteType()));
        return updateByIdValidate(editWebsite);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean del(@Nullable List<Long> ids)
            throws RequestServerException, AuthorityException, EnumIllegalArgumentException {
        if (CollectionUtils.isNotEmpty(ids)) {
            for (Long id : ids) {
                BizWebsiteDO delData = getBaseMapper().get(true, id);
                // 如果类型为用户时同时删除该导航栏
                if (ResourceTypeEnum.USER == delData.getType()) {
                    checkPermission(OperationTypeEnum.DEL, delData.getId(), delData.getType());
                    checkOperation(removeByIdValidate(delData.getId()));
                }
                // 删除关联
                relWebsiteSysAdminService.del(delData.getId());
            }
        }
        return removeByIdIgnore(ids);
    }

    /**
     * 填充数据
     *
     * @param source 原始对象
     * @param target 填充对象
     * @throws RequestServerException 用户ID获取失败
     * @since 1.0.0
     */
    private void fillData(@NonNull WebsiteDTO source, @NonNull BizWebsiteDO target) throws RequestServerException {
        Assert.notNull(source, "Source can not be null");
        Assert.notNull(target, "Target can not be null");
        target.setTitle(source.getWebsiteTitle());
        // TODO 缩略图不存在时使用默认的缩略图
        target.setImage(StringUtils.isNotBlank(source.getWebsiteImage()) ? source.getWebsiteImage() : "DIV IMAGE");
        target.setLink(source.getWebsiteLink());
        ObjectExecute.notBlankExecute(source.getWebsiteTagName(), res -> target.setTagName(source.getWebsiteTagName()));
        ObjectExecute.notBlankExecute(source.getWebsiteDescription(), res -> target.setDescription(source.getWebsiteDescription()));
        target.setType(source.getWebsiteType());
        target.setStatus(source.getWebsiteStatus());
    }

    /**
     * 检查网站操作权限
     *
     * @param operationType 操作类型
     * @param websiteId     网站ID, 当网站类型为 {@link ResourceTypeEnum#PUBLIC} 时不能为空
     * @param websiteType   网站类型, 为 {@code null} 时不进行检查
     * @throws RequestServerException       用户ID获取失败
     * @throws AuthorityException           非网站拥有者无法进行操作
     * @throws EnumIllegalArgumentException 未知的枚举类型
     * @since 1.0.0
     */
    private void checkPermission(@NonNull OperationTypeEnum operationType,
                                 @Nullable Long websiteId, @Nullable ResourceTypeEnum websiteType)
            throws RequestServerException, AuthorityException, EnumIllegalArgumentException {
        Assert.notNull(operationType, "OperationType can not be null");
        if (Objects.nonNull(websiteType)) {
            switch (websiteType) {
                case INTERNAL:
                    if (operationType.isMatch(OperationTypeEnum.ADD, OperationTypeEnum.EDIT, OperationTypeEnum.DEL)) {
                        adminService.checkIsInternal(null, () -> true);
                    }
                    break;
                case PUBLIC:
                    if (operationType.isMatch(OperationTypeEnum.EDIT, OperationTypeEnum.DEL)) {
                        Assert.notNull(websiteId, "WebsiteId can not be null");
                        BizWebsiteDO dbWebsite = getByIdValidate(websiteId);
                        if (!Objects.equals(dbWebsite.getRelCreateAdminId(), UserInfoContext.userId())) {
                            throw new AuthorityException(AuthorityErrorEnum.ACCESS_BLOCKED, "非网站拥有者无法进行操作");
                        }
                    }
                    break;
                case USER:
                    break;
                default:
                    throw BaseEnum.DEFAULT_ANALYZE_EXCEPTION.apply(websiteType.getClass().getName(), websiteType);
            }
        }
    }

    /**
     * 解析网站状态
     *
     * @param websiteType 网站类型
     * @return 网站状态
     * @since 1.0.0
     */
    @SuppressWarnings("DuplicatedCode")
    private ResourceStatusEnum analyzeStatus(@Nullable ResourceTypeEnum websiteType) {
        if (Objects.isNull(websiteType)) {
            return null;
        }
        SysAdminDO cacheAdmin = adminService.getCacheById(UserInfoContext.userId());
        // 用户类型内置用户状态默认通过
        if (ResourceTypeEnum.USER == websiteType || BooleanUtils.isTrue(cacheAdmin.getInternal())) {
            return ResourceStatusEnum.APPROVED;
        } else {
            // 其他类型默认置为待审核
            return ResourceStatusEnum.UNAPPROVED;
        }
    }
}
