package cn.krismile.boot.framework.autoconfigure.web.converter;

import cn.krismile.boot.framework.autoconfigure.web.KsWebAutoConfiguration;
import cn.krismile.boot.framework.core.constant.PatternConstant;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.autoconfigure.jackson.JacksonProperties;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.util.ClassUtils;

import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

/**
 * Jackson自动配置
 *
 * <p>该类自动配置了 {@code Jackson} 的常用功能
 * 已默认配置的如下:
 * <ul>
 *     <li>{@link BigInteger} 自动序列化为 {@link String} 字符串</li>
 *     <li>{@link Long} 自动序列化为 {@link String} 字符串</li>
 *     <li>{@link Date} 如未配置格式则自动序列化为 {@code yyyy-MM-dd HH:mm:ss} 字符串</li>
 *     <li>{@link LocalDateTime} 如未配置格式则自动序列化为 {@code yyyy-MM-dd HH:mm:ss} 字符串</li>
 *     <li>{@link LocalDate} 自动序列化为 {@code yyyy-MM-dd} 字符串</li>
 *     <li>{@link LocalTime} 自动序列化为 {@code HH:mm:ss} 字符串</li>
 * </ul>
 * <p>该类采用 {@link Jackson2ObjectMapperBuilderCustomizer} 的原因是默认 {@code Jackson} 已存在一些默认配置内容,
 * 通过此种方式可以实现在不修改默认配置的情况下插入我们自定义的配置
 *
 * @author JiYinchuan
 * @see KsWebAutoConfiguration
 * @since 1.0.0
 */
public class JacksonObjectMapperBuilder implements Jackson2ObjectMapperBuilderCustomizer {

    /**
     * 默认时区
     */
    private static final TimeZone DEFAULT_TIME_ZONE = TimeZone.getTimeZone("Asia/Shanghai");

    /**
     * Jackson配置文件
     */
    private final JacksonProperties jacksonProperties;

    /**
     * 构造器
     *
     * @param jacksonProperties Jackson配置文件
     * @since 1.0.0
     */
    public JacksonObjectMapperBuilder(JacksonProperties jacksonProperties) {
        this.jacksonProperties = jacksonProperties;
    }

    @Override
    public void customize(Jackson2ObjectMapperBuilder jacksonObjectMapperBuilder) {
        jacksonObjectMapperBuilder.serializerByType(BigInteger.class, new ToStringSerializer());
        jacksonObjectMapperBuilder.serializerByType(Long.class, new ToStringSerializer());

        jacksonObjectMapperBuilder.serializerByType(LocalTime.class,
                new LocalTimeSerializer(DateTimeFormatter.ofPattern(PatternConstant.Date.NORM_TIME_PATTERN)));
        jacksonObjectMapperBuilder.serializerByType(LocalDate.class,
                new LocalDateSerializer(DateTimeFormatter.ofPattern(PatternConstant.Date.NORM_DATE_PATTERN)));
        jacksonObjectMapperBuilder.serializerByType(LocalDateTime.class,
                new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(PatternConstant.Date.NORM_DATETIME_PATTERN)));

        jacksonObjectMapperBuilder.deserializerByType(LocalTime.class,
                new LocalTimeDeserializer(DateTimeFormatter.ofPattern(PatternConstant.Date.NORM_TIME_PATTERN)));
        jacksonObjectMapperBuilder.deserializerByType(LocalDate.class,
                new LocalDateDeserializer(DateTimeFormatter.ofPattern(PatternConstant.Date.NORM_DATE_PATTERN)));
        jacksonObjectMapperBuilder.deserializerByType(LocalDateTime.class,
                new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern(PatternConstant.Date.NORM_DATETIME_PATTERN)));

        if (Objects.isNull(jacksonProperties.getTimeZone())) {
            jacksonObjectMapperBuilder.timeZone(DEFAULT_TIME_ZONE);
        }

        String dateFormat = jacksonProperties.getDateFormat();
        if (Objects.isNull(dateFormat)) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(PatternConstant.Date.NORM_DATETIME_PATTERN);
            TimeZone timeZone = jacksonProperties.getTimeZone();
            if (Objects.isNull(timeZone)) {
                timeZone = DEFAULT_TIME_ZONE;
            }
            simpleDateFormat.setTimeZone(timeZone);
            jacksonObjectMapperBuilder.dateFormat(simpleDateFormat);
        } else {
            try {
                Class<?> dateFormatClass = ClassUtils.forName(dateFormat, null);
                jacksonObjectMapperBuilder.dateFormat((DateFormat) BeanUtils.instantiateClass(dateFormatClass));
            } catch (ClassNotFoundException ex) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
                TimeZone timeZone = this.jacksonProperties.getTimeZone();
                if (timeZone == null) {
                    timeZone = DEFAULT_TIME_ZONE;
                }
                simpleDateFormat.setTimeZone(timeZone);
                jacksonObjectMapperBuilder.dateFormat(simpleDateFormat);
                jacksonObjectMapperBuilder.serializerByType(LocalDateTime.class,
                        new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(dateFormat)));
                jacksonObjectMapperBuilder.deserializerByType(LocalDateTime.class,
                        new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern(dateFormat)));
            }
        }
    }
}
