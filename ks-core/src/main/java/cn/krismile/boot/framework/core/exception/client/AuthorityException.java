package cn.krismile.boot.framework.core.exception.client;

import cn.krismile.boot.framework.core.enumeration.error.client.AuthorityErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 访问权限自定义异常
 *
 * @author JiYinchuan
 * @see AuthorityErrorEnum
 * @since 1.0.0
 */
public class AuthorityException extends ApplicationException {

    private static final long serialVersionUID = -7183292490313738345L;

    public AuthorityException(AuthorityErrorEnum authorityErrorEnum) {
        super(authorityErrorEnum);
    }

    public AuthorityException(AuthorityErrorEnum authorityErrorEnum, String userTip) {
        super(authorityErrorEnum, userTip);
    }
}
