package cn.krismile.persagy.webstack.function.business.service.impl;

import cn.krismile.boot.framework.core.enumeration.error.client.RequestParamErrorEnum;
import cn.krismile.boot.framework.core.exception.client.RequestParamException;
import cn.krismile.boot.framework.core.exception.client.RequestServerException;
import cn.krismile.boot.framework.core.util.Assert;
import cn.krismile.boot.framework.mybatisplus.exception.MybatisServiceException;
import cn.krismile.boot.framework.mybatisplus.service.BaseServiceImpl;
import cn.krismile.persagy.webstack.function.business.mapper.BizRelWebsiteSysAdminMapper;
import cn.krismile.persagy.webstack.function.business.service.BizRelWebsiteSysAdminService;
import cn.krismile.persagy.webstack.function.business.service.BizWebsiteService;
import cn.krismile.persagy.webstack.pojo.context.UserInfoContext;
import cn.krismile.persagy.webstack.pojo.domain.BizRelWebsiteSysAdminDO;
import cn.krismile.persagy.webstack.pojo.dto.WebsiteSortDTO;
import org.springframework.context.annotation.Lazy;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.validation.Validator;
import java.util.Objects;

/**
 * 业务-站点-用户关联表 ServiceImpl
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Service
public class BizRelWebsiteSysAdminServiceImpl extends BaseServiceImpl<BizRelWebsiteSysAdminMapper, BizRelWebsiteSysAdminDO> implements BizRelWebsiteSysAdminService {

    @Resource
    private Validator validator;
    @Resource
    @Lazy
    private BizWebsiteService websiteService;

    @Override
    public boolean checkRelation(@NonNull Long websiteId)
            throws MybatisServiceException, RequestServerException {
        checkWebsite(false, websiteId);
        return getBaseMapper().count(websiteId, UserInfoContext.userId()) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean relation(@NonNull Long websiteId, @NonNull Integer orderWeight)
            throws MybatisServiceException, RequestServerException {
        checkWebsite(false, websiteId);
        checkIdempotency(getBaseMapper().count(websiteId, UserInfoContext.userId()));
        Assert.notNull(orderWeight, "OrderWeight can not be null");

        BizRelWebsiteSysAdminDO addData = new BizRelWebsiteSysAdminDO();
        addData.setOrderWeight(orderWeight);
        addData.setRelWebsiteId(websiteId);
        addData.setRelSysAdminId(UserInfoContext.userId());
        return saveValidate(addData);
    }

    @Override
    public boolean editSort(@NonNull Long websiteId, @NonNull Integer orderWeight)
            throws MybatisServiceException, RequestParamException, RequestServerException {
        checkWebsite(true, websiteId);
        Assert.notNull(orderWeight, "OrderWeight can not be null");

        BizRelWebsiteSysAdminDO editData = checkExistsAndGet(getBaseMapper()
                .get(true, websiteId, UserInfoContext.userId()), DEFAULT_NOT_FOUND_MESSAGE);
        if (Objects.equals(editData.getOrderWeight(), orderWeight)) {
            return true;
        }
        editData.setOrderWeight(orderWeight);
        return updateByIdValidate(editData);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean sortExchange(@NonNull WebsiteSortDTO dto)
            throws MybatisServiceException, RequestParamException, RequestServerException {
        validator.validate(dto);
        checkWebsite(true, dto.getSourceWebsiteId());
        checkWebsite(true, dto.getTargetWebsiteId());

        BizRelWebsiteSysAdminDO sourceRelWebsite = checkExistsAndGet(getBaseMapper()
                .get(true, dto.getSourceWebsiteId(), UserInfoContext.userId()), DEFAULT_NOT_FOUND_MESSAGE);
        int sourceWebsiteSortWeight = sourceRelWebsite.getOrderWeight();

        BizRelWebsiteSysAdminDO targetRelWebsite = checkExistsAndGet(getBaseMapper()
                .get(true, dto.getTargetWebsiteId(), UserInfoContext.userId()), DEFAULT_NOT_FOUND_MESSAGE);

        // 调换源网站和目标网站的排序权重
        sourceRelWebsite.setOrderWeight(targetRelWebsite.getOrderWeight());
        checkOperation(updateByIdValidate(sourceRelWebsite));

        targetRelWebsite.setOrderWeight(sourceWebsiteSortWeight);
        return updateByIdValidate(targetRelWebsite);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean del(@NonNull Long websiteId)
            throws MybatisServiceException, RequestParamException, RequestServerException {
        Assert.notNull(websiteId, "WebsiteId can not be null");
        checkWebsite(true, websiteId);
        return removeById(getBaseMapper().get(true, websiteId, UserInfoContext.userId()));
    }

    /**
     * 检查网站
     *
     * @param isCheckRelation 是否检查关联
     * @param websiteId       网站ID
     * @throws MybatisServiceException 导航栏不存在
     * @throws RequestParamException   未找到用户关联的网站
     * @throws RequestServerException  用户ID获取失败
     * @since 1.0.0
     */
    private void checkWebsite(boolean isCheckRelation, @NonNull Long websiteId)
            throws MybatisServiceException, RequestParamException, RequestServerException {
        // 检查用户是否关联该网站
        if (isCheckRelation && !checkRelation(websiteId)) {
            throw new RequestParamException(RequestParamErrorEnum.INVALID_INPUT, DEFAULT_NOT_FOUND_MESSAGE);
        }
        // 检查网站是否存在
        checkExists(websiteService.isExistsByIdValidate(websiteId), BizWebsiteService.DEFAULT_NOT_FOUND_MESSAGE);
    }
}
