package cn.krismile.persagy.webstack.function.business.service;

import cn.krismile.boot.framework.core.exception.client.RequestParamException;
import cn.krismile.boot.framework.core.exception.client.RequestServerException;
import cn.krismile.boot.framework.mybatisplus.exception.MybatisServiceException;
import cn.krismile.boot.framework.mybatisplus.service.BaseService;
import cn.krismile.persagy.webstack.pojo.domain.BizRelWebsiteSysAdminDO;
import cn.krismile.persagy.webstack.pojo.dto.WebsiteSortDTO;
import org.springframework.lang.NonNull;

/**
 * 业务-站点-用户关联表 Service
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface BizRelWebsiteSysAdminService extends BaseService<BizRelWebsiteSysAdminDO> {

    String DEFAULT_NOT_FOUND_MESSAGE = "未找到用户关联的网站";

    /**
     * 检查当前用户是否关联此网站
     *
     * @param websiteId 网站ID
     * @return 是否已关联
     * @throws MybatisServiceException 网站不存在|未找到用户关联的网站
     * @throws RequestServerException  用户ID获取失败
     * @since 1.0.0
     */
    boolean checkRelation(@NonNull Long websiteId) throws MybatisServiceException, RequestServerException;

    /**
     * 关联网站和用户
     *
     * @param websiteId   网站ID
     * @param orderWeight 网站权重
     * @return 操作是否成功
     * @throws MybatisServiceException 网站不存在|未找到用户关联的网站
     * @throws RequestServerException  用户ID获取失败
     * @since 1.0.0
     */
    boolean relation(@NonNull Long websiteId, @NonNull Integer orderWeight)
            throws MybatisServiceException, RequestServerException;

    /**
     * 设置排序权重
     *
     * @param websiteId   网站ID
     * @param orderWeight 排序权重
     * @return 操作是否成功
     * @throws MybatisServiceException 导航栏不存在
     * @throws RequestParamException   未找到用户关联的网站
     * @throws RequestServerException  用户ID获取失败
     * @since 1.0.0
     */
    boolean editSort(@NonNull Long websiteId, @NonNull Integer orderWeight)
            throws MybatisServiceException, RequestServerException;

    /**
     * 网站排序
     *
     * @param dto 网站排序DTO
     * @return 操作是否成功
     * @throws MybatisServiceException 导航栏不存在
     * @throws RequestParamException   未找到用户关联的网站
     * @throws RequestServerException  用户ID获取失败
     * @since 1.0.0
     */
    boolean sortExchange(@NonNull WebsiteSortDTO dto)
            throws MybatisServiceException, RequestServerException;

    /**
     * 删除
     *
     * @param websiteId 网站ID
     * @return 操作是否成功
     * @throws MybatisServiceException 导航栏不存在
     * @throws RequestParamException   未找到用户关联的网站
     * @throws RequestServerException  用户ID获取失败
     * @since 1.0.0
     */
    boolean del(@NonNull Long websiteId) throws MybatisServiceException, RequestServerException;

}
