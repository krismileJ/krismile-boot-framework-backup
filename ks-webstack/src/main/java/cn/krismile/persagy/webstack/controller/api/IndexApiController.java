package cn.krismile.persagy.webstack.controller.api;

import cn.krismile.boot.framework.core.response.ResultVO;
import cn.krismile.boot.framework.core.response.vo.MultiVO;
import cn.krismile.boot.framework.core.util.common.ProjectUtils;
import cn.krismile.persagy.webstack.function.business.service.WebService;
import cn.krismile.persagy.webstack.pojo.vo.web.NavVO;
import org.apache.commons.io.FileUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * IndexController
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@RestController
@RequestMapping("/site")
public class IndexApiController {

    @Resource
    private WebService webService;

    @GetMapping("/list/data/json")
    public String listDataJson() throws IOException {
        File jsonFile = new File(ProjectUtils.getResourcePath("data.json"));
        return FileUtils.readFileToString(jsonFile, StandardCharsets.UTF_8);
    }

    @GetMapping("/list/data")
    public MultiVO<NavVO> listData() {
        return ResultVO.okMulti(webService.listCache());
    }
}
