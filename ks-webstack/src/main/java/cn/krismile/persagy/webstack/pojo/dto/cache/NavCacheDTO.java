package cn.krismile.persagy.webstack.pojo.dto.cache;

import cn.krismile.persagy.webstack.pojo.dto.WebsiteDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 导航栏网站缓存DTO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
public class NavCacheDTO implements Serializable {

    private static final long serialVersionUID = -7705654060881113390L;

    /**
     * 关联导航栏ID
     */
    private Long relNavId;

    /**
     * 关联导航栏名称
     */
    private String relNavName;

    /**
     * 关联网站信息
     */
    private List<WebsiteDTO> relWebsiteId;

}
