package cn.krismile.boot.framework.autoconfigure.aop.properties;

import cn.krismile.boot.framework.autoconfigure.KrismileProperties;
import cn.krismile.boot.framework.autoconfigure.aop.properties.children.KsRequestLogProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * AOP配置文件
 *
 * <pre>
 *     # ------------------------- KrismileConfig -------------------------
 *     krismile:
 *       aop:
 *         # 请求日志配置
 *         request-log:
 *           # 是否启用
 *           enable: true
 *           # DEBUG模式下打印的请求头名称数组, 忽略大小写, 为 [null] 时将打印所有请求头
 *           debug-print-header-names:
 * </pre>
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = KsAopProperties.KEY)
public class KsAopProperties {

    public static final String KEY = KrismileProperties.KEY + "." + "aop";

    /**
     * 请求日志配置文件
     */
    @NestedConfigurationProperty
    private KsRequestLogProperties requestLog = new KsRequestLogProperties();

}
