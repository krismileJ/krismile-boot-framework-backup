package cn.krismile.boot.framework.mybatisplus.generate.config;

import lombok.Data;

/**
 * MyBatisPlus基本配置文件
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
public class CodeBasicConfig {

    /**
     * 作者
     */
    private String author = "ks-boot4j";

    /**
     * 从xxx版本开始
     */
    private String since = "1.0.0";

    /**
     * 输出包类型
     */
    private OutputType outputType = OutputType.ALL;

    /**
     * 是否开启Swagger注解支持
     */
    private Boolean enableSwagger = false;

    /**
     * 输出包类型
     */
    public enum OutputType {

        /**
         * 输出所有包
         */
        ALL,

        /**
         * 仅输出实体包
         */
        ONLY_DOMAIN,

        /**
         * 输出除实体外所有包
         */
        ALL_EXCLUDE_DOMAIN

    }
}
