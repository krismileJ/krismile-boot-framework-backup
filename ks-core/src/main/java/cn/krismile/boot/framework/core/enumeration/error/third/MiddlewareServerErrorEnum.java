package cn.krismile.boot.framework.core.enumeration.error.third;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import cn.krismile.boot.framework.core.enumeration.error.client.RequestParamErrorEnum;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 中间件服务出错异常枚举
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public enum MiddlewareServerErrorEnum implements BaseEnum<String> {

    /**
     * 中间件服务出错
     */
    MIDDLEWARE_SERVICE_ERROR("C0100", "中间件服务出错"),

    /**
     * 服务出错
     */
    RPC_SERVER_ERROR("C0110", "RPC 服务出错"),

    /**
     * RPC 服务未找到
     */
    RPC_SERVICE_NOT_FOUND("C0111", "RPC 服务未找到"),

    /**
     * RPC 服务未注册
     */
    RPC_SERVICE_NOT_REGISTER("C0112", "RPC 服务未注册"),

    /**
     * 接口不存在
     */
    API_NOT_FOUND("C0113", "接口不存在"),

    /**
     * 消息服务出错
     */
    MESSAGE_SERVICE_ERROR("C0120", "消息服务出错"),

    /**
     * 消息投递出错
     */
    MESSAGE_SEND_ERROR("C0121", "消息投递出错"),

    /**
     * 消息消费出错
     */
    MESSAGE_CONSUME_ERROR("C0122", "消息消费出错"),

    /**
     * 消息订阅出错
     */
    MESSAGE_SUBSCRIBE_ERROR("C0123", "消息订阅出错"),

    /**
     * 消息分组未查到
     */
    MESSAGE_GROUP_NOT_FOUND("C0124", "消息分组未查到"),

    /**
     * 缓存服务出错
     */
    CACHE_SERVICE_ERROR("C0130", "缓存服务出错"),

    /**
     * key 长度超过限制
     */
    CACHE_KEY_EXCEED_LIMIT("C0131", "key 长度超过限制"),

    /**
     * value 长度超过限制
     */
    CACHE_VALUE_EXCEED_LIMIT("C0132", "value 长度超过限制"),

    /**
     * 存储容量已满
     */
    CACHE_STORAGE_FULL("C0133", "存储容量已满"),

    /**
     * 不支持的数据格式
     */
    CACHE_NOT_SUPPORT_DATA_FORMAT("C0134", "不支持的数据格式"),

    /**
     * 配置服务出错
     */
    CONFIG_SERVICE_ERROR("C0140", "配置服务出错"),

    /**
     * 网络资源服务出错
     */
    NETWORK_RESOURCE_SERVICE_ERROR("C0150", "网络资源服务出错"),

    /**
     * VPN 服务出错
     */
    VPN_SERVICE_ERROR("C0151", "VPN 服务出错"),

    /**
     * CDN 服务出错
     */
    CDN_SERVICE_ERROR("C0152", "CDN 服务出错"),

    /**
     * 域名解析服务出错
     */
    DNS_SERVICE_ERROR("C0153", "域名解析服务出错"),

    /**
     * 网关服务出错
     */
    GATEWAY_SERVICE_ERROR("C0154", "网关服务出错");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    MiddlewareServerErrorEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static RequestParamErrorEnum analyze(String value) {
        return BaseEnum.analyze(value, RequestParamErrorEnum.class);
    }
}
