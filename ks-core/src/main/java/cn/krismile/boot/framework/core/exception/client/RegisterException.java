package cn.krismile.boot.framework.core.exception.client;

import cn.krismile.boot.framework.core.enumeration.error.client.RegisterErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 用户注册自定义异常
 *
 * @author JiYinchuan
 * @see RegisterErrorEnum
 * @since 1.0.0
 */
public class RegisterException extends ApplicationException {

    private static final long serialVersionUID = -4618282956966958195L;

    public RegisterException(RegisterErrorEnum registerErrorEnum) {
        super(registerErrorEnum);
    }

    public RegisterException(RegisterErrorEnum registerErrorEnum, String userTip) {
        super(registerErrorEnum, userTip);
    }
}
