package cn.krismile.boot.framework.core.enumeration.error.server;

import com.fasterxml.jackson.annotation.JsonCreator;
import cn.krismile.boot.framework.core.enumeration.BaseEnum;

/**
 * 系统执行超时异常枚举
 *
 * @author JiYinchuan
 * @see cn.krismile.boot.framework.core.exception.server.ExecutionException
 * @since 1.0.0
 */
public enum ExecutionErrorEnum implements BaseEnum<String> {

    /**
     * 系统执行超时
     */
    EXECUTION_TIMEOUT("B0100", "系统执行超时"),

    /**
     * 系统订单处理超时
     */
    EXECUTION_ORDER_TIMEOUT("B0101", "系统订单处理超时");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    ExecutionErrorEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static ExecutionErrorEnum analyze(String value) {
        return BaseEnum.analyze(value, ExecutionErrorEnum.class);
    }
}
