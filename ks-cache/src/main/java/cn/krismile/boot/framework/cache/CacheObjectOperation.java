package cn.krismile.boot.framework.cache;

import cn.krismile.boot.framework.core.annotation.jsr.Nullable;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.TimeoutUtils;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

/**
 * 对象缓存操作
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface CacheObjectOperation {

    /**
     * 获取值
     *
     * @param key 键
     * @param <V> 值类型
     * @return 值, 当键不存在或在 {@code Pipelined/Transaction} 模式时将返回 {@code null}
     * @since 1.0.0
     */
    @Nullable
    <V> V get(String key);

    /**
     * 设置值
     *
     * @param key   键
     * @param value 值
     * @since 1.0.0
     */
    void set(String key, Object value);

    /**
     * 设置值并指定过期时间
     *
     * @param key        键
     * @param value      值
     * @param expireTime 过期时间, 小于等于 [0] 将永不过期
     * @param timeUnit   时间单位
     * @since 1.0.0
     */
    void set(String key, Object value, long expireTime, TimeUnit timeUnit);

    /**
     * 设置值并指定过期时间
     *
     * @param key        键
     * @param value      值
     * @param expireTime 过期时间
     * @since 1.0.0
     */
    default void set(String key, Object value, Duration expireTime) {
        if (TimeoutUtils.hasMillis(expireTime)) {
            this.set(key, value, expireTime.toMillis(), TimeUnit.MILLISECONDS);
        } else {
            this.set(key, value, expireTime.getSeconds(), TimeUnit.SECONDS);
        }
    }

    /**
     * 设置值并指定过期时间
     *
     * @param key        键
     * @param value      值
     * @param expireTime 过期时间, 单位 [秒], 小于等于 [0] 将永不过期
     * @since 1.0.0
     */
    default void set(String key, Object value, long expireTime) {
        this.set(key, value, expireTime, TimeUnit.SECONDS);
    }

    /**
     * 指定键的值进行递增
     *
     * @param key    键
     * @param factor 递增因子, 必须大于 [0]
     * @return 递增后的值, 不存在则小于 [0], 在 {@code Pipelined/Transaction} 模式时将返回 {@code null}
     * @see RedisConnection#isPipelined()
     * @see RedisConnection#isQueueing()
     * @since 1.0.0
     */
    @Nullable
    Long increment(String key, long factor);

    /**
     * 指定键的值进行递减
     *
     * @param key    键
     * @param factor 递减因子, 必须大于 [0]
     * @return 递减后的值, 不存在则小于 [0], 在 {@code Pipelined/Transaction} 模式时将返回 {@code null}
     * @see RedisConnection#isPipelined()
     * @see RedisConnection#isQueueing()
     * @since 1.0.0
     */
    @Nullable
    Long decrement(String key, long factor);

}
