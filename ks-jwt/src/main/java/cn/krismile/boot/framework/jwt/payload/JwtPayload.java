package cn.krismile.boot.framework.jwt.payload;

import cn.krismile.boot.framework.jwt.util.JwtUtils;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * JWT载荷
 *
 * <p>该类为JWT标准载荷+自定义载荷, 其中包含了两个自定义参数, 对JWT相关操作请参考 {@link JwtUtils}
 * <ul>
 *     <li>userId: 用户ID</li>
 *     <li>internal: 是否为系统内置</li>
 * </ul>
 *
 * @author JiYinchuan
 * @see JwtUtils
 * @since 1.0.0
 */
@Data
@Accessors(chain = true)
public class JwtPayload implements Serializable {

    private static final long serialVersionUID = -8088545075745163517L;
    public static final String USER_ID = "userId";
    public static final String INTERNAL = "internal";

    // ------------------------- JWT标准字段 -------------------------

    /**
     * JWT ID
     * <p>唯一标识（UUID）
     */
    private String jti;

    /**
     * Issuer
     * <p>签发者
     */
    private String iss;

    /**
     * Issuer at
     * <p>签发时间（时间戳）
     */
    private LocalDateTime iat;

    /**
     * Expire
     * <p>过期时间（时间戳）
     */
    private LocalDateTime exp;

    /**
     * Not Before
     * <p>如果当前时间在此时间之前则token不被接受
     */
    private LocalDateTime nbf;

    /**
     * Audience
     * <p>接收方
     */
    private List<String> aud;

    /**
     * Subject
     * <p>面向的用户
     */
    private String sub;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 是否为内置
     */
    private Boolean internal;

}
