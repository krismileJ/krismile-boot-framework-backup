// package cn.krismile.boot.framework.test.flyway;
//
// import org.flywaydb.core.api.MigrationVersion;
// import org.flywaydb.core.api.migration.Context;
// import org.flywaydb.core.api.migration.JavaMigration;
// import org.springframework.jdbc.core.JdbcTemplate;
// import org.springframework.stereotype.Component;
//
// import java.sql.Connection;
//
// /**
//  * MyMigrate
//  *
//  * @author JiYinchuan
//  * @since 1.0.0
//  */
// @Component
// public class MyMigrate implements JavaMigration {
//
//     @Override
//     public MigrationVersion getVersion() {
//         return null;
//     }
//
//     @Override
//     public String getDescription() {
//         return null;
//     }
//
//     @Override
//     public Integer getChecksum() {
//         return null;
//     }
//
//     @Override
//     public boolean isUndo() {
//         return false;
//     }
//
//     @Override
//     public boolean isBaselineMigration() {
//         return false;
//     }
//
//     @Override
//     public boolean canExecuteInTransaction() {
//         return true;
//     }
//
//     @Override
//     public void migrate(Context context) throws Exception {
//         Connection connection = context.getConnection();
//         JdbcTemplate jdbcTemplate = new JdbcTemplate(context.getConfiguration().getDataSource());
//     }
// }
