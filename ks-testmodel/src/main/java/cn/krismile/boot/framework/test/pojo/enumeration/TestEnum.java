package cn.krismile.boot.framework.test.pojo.enumeration;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;

/**
 * 测试枚举
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public enum TestEnum implements BaseEnum<String> {

    /**
     * 一切 ok
     */
    OK("00000", "OK");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    TestEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return this.value;
    }

    // @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    // public static TestEnum analyze(String value) {
    //     return BaseEnum.analyze(value, TestEnum.class);
    // }
}
