package cn.krismile.persagy.webstack.function.business.service;

import cn.krismile.boot.framework.core.exception.client.RequestServerException;
import cn.krismile.boot.framework.mybatisplus.exception.MybatisServiceException;
import cn.krismile.persagy.webstack.pojo.domain.BizRelNavWebsiteSysAdminDO;
import cn.krismile.boot.framework.mybatisplus.service.BaseService;
import org.springframework.lang.NonNull;

/**
 * 业务-导航-站点-用户关联表 Service
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface BizRelNavWebsiteSysAdminService extends BaseService<BizRelNavWebsiteSysAdminDO> {

    /**
     * 关联导航栏|网站|用户
     *
     * @param navId     导航ID
     * @param websiteId 网站ID
     * @return 操作是否成功
     * @throws MybatisServiceException 导航栏ID错误|网站ID错误
     * @throws RequestServerException  用户ID获取失败
     * @since 1.0.0
     */
    boolean relation(@NonNull Long navId, @NonNull Long websiteId) throws RequestServerException;

}
