package cn.krismile.persagy.webstack.controller.web.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * AdminController
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @RequestMapping("/login")
    public String index() {
        return "/admin/login";
    }
}
