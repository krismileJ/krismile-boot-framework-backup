package cn.krismile.boot.framework.mybatisplus.service;

import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.boot.framework.core.annotation.jsr.Nullable;
import cn.krismile.boot.framework.core.exception.ApplicationException;
import cn.krismile.boot.framework.core.page.DefaultListPage;
import cn.krismile.boot.framework.core.page.Pageable;
import cn.krismile.boot.framework.core.query.PageQuery;
import cn.krismile.boot.framework.core.util.Assert;
import cn.krismile.boot.framework.mybatisplus.domain.BaseDO;
import cn.krismile.boot.framework.mybatisplus.enumeration.error.MybatisServiceErrorEnum;
import cn.krismile.boot.framework.mybatisplus.exception.MybatisServiceException;
import cn.krismile.boot.framework.mybatisplus.service.check.CheckProvider;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * MybatisService基类
 *
 * <p>该类为 [MybatisService] 基类, 对 [MybatisPlus] 自带的 {@link IService} 进行了进一步的封装,
 * 增强业务效果, 采用该类的方法以替代 {@link IService} 中的方法
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface BaseService<T extends BaseDO> extends IService<T>, CheckProvider {

    /**
     * 查询锁后缀
     */
    String QUERY_LOCK = " for update";

    // ------------------------- Ignore -------------------------

    /**
     * 忽略ID获取是否存在
     *
     * @param id ID
     * @return 是否存在 [true: 存在, false: 不存在]
     * @since 1.0.0
     */
    default boolean isExistsByIdIgnore(@Nullable Long id) {
        if (Objects.isNull(id)) {
            return false;
        }
        return this.countByIdValidate(id) != 0;
    }

    /**
     * 批量忽略ID获取是否存在
     *
     * @param ids IDs
     * @return 是否存在 [true: 存在, false: 不存在]
     * @since 1.0.0
     */
    default boolean isExistsByIdIgnore(@Nullable Collection<Long> ids) {
        if (Objects.isNull(ids) || ids.isEmpty()) {
            return false;
        }
        return this.countByIdValidate(ids) != 0;
    }

    /**
     * 忽略ID获取数量
     *
     * @param id ID
     * @return 数量
     * @since 1.0.0
     */
    default long countByIdIgnore(@Nullable Long id) {
        if (Objects.isNull(id)) {
            return -1;
        }
        return this.countByIdValidate(id);
    }

    /**
     * 忽略ID获取数量
     *
     * @param ids ID
     * @return 数量
     * @since 1.0.0
     */
    default long countByIdIgnore(@Nullable Collection<Long> ids) {
        if (Objects.isNull(ids) || ids.isEmpty()) {
            return -1;
        }
        return this.countByIdValidate(ids);
    }

    /**
     * 忽略ID并获取DO
     *
     * @param id ID
     * @return DO
     * @since 1.0.0
     */
    @Nullable
    default T getByIdIgnore(@Nullable Long id) {
        return this.getByIdIgnoreOpt(id).orElse(null);
    }

    /**
     * 忽略ID并获取DO
     *
     * @param id ID
     * @return DO
     * @since 1.0.0
     */
    default Optional<T> getByIdIgnoreOpt(@Nullable Long id) {
        if (Objects.isNull(id)) {
            return Optional.empty();
        }
        return Optional.ofNullable(this.getOne(Wrappers.<T>query().eq(BaseDO.ID, id)));
    }

    /**
     * 忽略IDs并获取DO
     *
     * @param ids IDs
     * @return DOs
     * @since 1.0.0
     */
    default List<T> listByIdIgnore(@Nullable Collection<Long> ids) {
        if (Objects.isNull(ids) || ids.isEmpty()) {
            return Collections.emptyList();
        }
        return listByIds(ids);
    }

    /**
     * 忽略结果并获取DO
     *
     * @param wrapper wrapper
     * @return DOs
     * @since 1.0.0
     */
    @Nullable
    default List<T> listIgnore(Wrapper<T> wrapper) {
        List<T> result = this.list(wrapper);
        if (Objects.isNull(result) || result.isEmpty()) {
            return Collections.emptyList();
        }
        return result;
    }

    /**
     * 忽略PageRequest并获取Pageable
     * 默认根据 {@link BaseDO#getUpdateTime()} 进行排序
     *
     * @param pageQuery pageQuery
     * @return Pageable
     * @since 1.0.0
     */
    default Pageable<List<T>> pageIgnore(@Nullable PageQuery pageQuery) {
        if (Objects.isNull(pageQuery)) {
            return new DefaultListPage<>(new PageQuery());
        }
        Page<T> page = this.page(new Page<T>(pageQuery.getPageNo(), pageQuery.getPageSize())
                .addOrder(OrderItem.desc(BaseService.toUnderlineCase(BaseDO.UPDATE_TIME))));
        return new DefaultListPage<>(pageQuery, page.getTotal(), page.getRecords());
    }

    /**
     * 忽略PageRequest并获取Pageable
     *
     * @param pageQuery pageQuery
     * @return Pageable
     * @since 1.0.0
     */
    default Pageable<List<T>> pageIgnore(@Nullable IPage<T> pageQuery) {
        if (Objects.isNull(pageQuery)) {
            return new DefaultListPage<>(new PageQuery());
        }
        IPage<T> page = this.page(pageQuery);
        return new DefaultListPage<>(page.getCurrent(), page.getSize(), page.getTotal(), page.getRecords());
    }

    /**
     * 忽略实体类ID并保存
     *
     * @param entity 实体类
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @since 1.0.0
     */
    default Boolean saveIgnore(@Nullable T entity) {
        if (Objects.isNull(entity)) {
            return null;
        }
        return this.save(BaseService.clearEntityDefaultParameterAndGet(entity,
                true, true, true));
    }

    /**
     * 批量忽略验证实体类ID并保存
     *
     * @param entities 实体对象集合
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @since 1.0.0
     */
    default boolean saveIgnore(@Nullable Collection<T> entities) {
        return this.saveIgnore(entities, 1000);
    }

    /**
     * 批量忽略验证实体类ID并保存
     *
     * @param entities  实体对象集合
     * @param batchSize 插入批次数量
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @since 1.0.0
     */
    default boolean saveIgnore(@Nullable Collection<T> entities, int batchSize) {
        if (Objects.isNull(entities) || entities.isEmpty()) {
            return false;
        }
        return this.saveBatch(BaseService.clearEntityDefaultParameterAndGet(entities,
                true, true, true), batchSize);
    }

    /**
     * 忽略验证实体类ID并更新
     *
     * @param entity 实体类
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @since 1.0.0
     */
    default boolean updateByIdIgnore(@Nullable T entity) {
        if (Objects.isNull(entity)) {
            return false;
        }
        return this.updateById(clearEntityDefaultParameterAndGet(entity));
    }

    /**
     * 批量验证实体类ID并更新
     *
     * @param entities 实体对象集合
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @since 1.0.0
     */
    default boolean updateByIdIgnore(@Nullable Collection<T> entities) {
        return this.updateByIdIgnore(entities, 1000);
    }

    /**
     * 批量验证实体类ID并更新
     *
     * @param entities  实体对象集合
     * @param batchSize 插入批次数量
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @since 1.0.0
     */
    default boolean updateByIdIgnore(@Nullable Collection<T> entities, int batchSize) {
        if (Objects.isNull(entities) || entities.isEmpty()) {
            return false;
        }
        return this.updateBatchById(clearEntityDefaultParameterAndGet(entities), batchSize);
    }

    /**
     * 忽略实体类ID并删除
     *
     * @param id ID
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @since 1.0.0
     */
    default boolean removeByIdIgnore(@Nullable Long id) {
        if (Objects.isNull(id)) {
            return false;
        }
        return this.removeById(id);
    }

    /**
     * 批量忽略实体类ID并删除
     *
     * @param ids ID集合
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @since 1.0.0
     */
    default boolean removeByIdIgnore(@Nullable Collection<Long> ids) {
        if (Objects.isNull(ids) || ids.isEmpty()) {
            return false;
        }
        return this.removeByIds(ids);
    }

    // ------------------------- Validate -------------------------

    /**
     * 验证ID获取是否存在
     *
     * @param id ID
     * @return 是否存在 [true: 存在, false: 不存在]
     * @throws MybatisServiceException ID为空
     * @since 1.0.0
     */
    default boolean isExistsByIdValidate(Long id) throws MybatisServiceException {
        return this.isExistsByIdValidate(id, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 验证ID获取是否存在
     *
     * @param id           ID
     * @param errorUserTip 用户提示信息
     * @return 是否存在 [true: 存在, false: 不存在]
     * @throws MybatisServiceException ID为空
     * @since 1.0.0
     */
    default boolean isExistsByIdValidate(@Nullable Long id, String errorUserTip)
            throws MybatisServiceException {
        if (Objects.isNull(id)) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL, errorUserTip);
        }
        return this.countByIdValidate(id) != 0;
    }

    /**
     * 批量验证ID获取是否存在
     *
     * @param ids IDs
     * @return 是否存在 [true: 存在, false: 不存在]
     * @throws MybatisServiceException ID为空
     * @since 1.0.0
     */
    default boolean isExistsByIdValidate(@Nullable Collection<Long> ids) throws MybatisServiceException {
        return this.isExistsByIdValidate(ids, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 批量验证ID获取是否存在
     *
     * @param ids          IDs
     * @param errorUserTip 用户提示信息
     * @return 是否存在 [true: 存在, false: 不存在]
     * @throws MybatisServiceException ID为空
     * @since 1.0.0
     */
    default boolean isExistsByIdValidate(@Nullable Collection<Long> ids, String errorUserTip)
            throws MybatisServiceException {
        if (Objects.isNull(ids) || ids.isEmpty()) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL, errorUserTip);
        }
        return this.countByIdValidate(ids) != 0;
    }

    /**
     * 验证ID获取数量
     *
     * @param id ID
     * @return 数量
     * @throws MybatisServiceException ID为空
     * @since 1.0.0
     */
    default long countByIdValidate(@Nullable Long id) throws MybatisServiceException {
        return this.countByIdValidate(id, null);
    }

    /**
     * 验证ID获取数量
     *
     * @param id           ID
     * @param errorUserTip 用户提示信息
     * @return 数量
     * @throws MybatisServiceException ID为空
     * @since 1.0.0
     */
    default long countByIdValidate(@Nullable Long id, @Nullable String errorUserTip) throws MybatisServiceException {
        if (Objects.isNull(id)) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL, errorUserTip);
        }
        return this.count(Wrappers.<T>query().select(BaseDO.ID).eq(BaseDO.ID, id));
    }

    /**
     * 批量验证ID获取数量
     *
     * @param ids IDs
     * @return 数量
     * @throws MybatisServiceException ID为空
     * @since 1.0.0
     */
    default long countByIdValidate(@Nullable Collection<Long> ids) throws MybatisServiceException {
        return this.countByIdValidate(ids, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 批量验证ID获取数量
     *
     * @param ids          IDs
     * @param errorUserTip 用户提示信息
     * @return 数量
     * @throws MybatisServiceException ID为空
     * @since 1.0.0
     */
    default long countByIdValidate(@Nullable Collection<Long> ids, @Nullable String errorUserTip)
            throws MybatisServiceException {
        if (Objects.isNull(ids) || ids.isEmpty()) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL, errorUserTip);
        }
        return this.count(Wrappers.<T>query().select(BaseDO.ID).in(BaseDO.ID, ids));
    }

    /**
     * 验证ID并获取DO
     *
     * @param id ID
     * @return DO
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default T getByIdValidate(@Nullable Long id) throws MybatisServiceException {
        return this.getByIdValidate(id, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 验证ID并获取DO
     *
     * @param id           ID
     * @param errorUserTip 用户提示信息
     * @return DO
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default T getByIdValidate(@Nullable Long id, @Nullable String errorUserTip) throws MybatisServiceException {
        if (Objects.isNull(id)) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL, errorUserTip);
        }
        Optional<T> entity = Optional.ofNullable(this.getOne(Wrappers.<T>query().eq(BaseDO.ID, id)));
        if (!entity.isPresent()) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.RESULT_IS_NULL, errorUserTip);
        }
        return entity.get();
    }

    /**
     * 验证IDs并获取DOs
     *
     * @param ids IDs
     * @return DOs
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default List<T> listByIdValidate(@Nullable Collection<Long> ids) throws MybatisServiceException {
        return this.listByIdValidate(ids, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 条件构造器获取DOs
     *
     * @param wrapper wrapper
     * @return DOs
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default List<T> listByIdValidate(Wrapper<T> wrapper) throws MybatisServiceException {
        return this.listValidate(wrapper, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 验证IDs并获取DOs
     *
     * @param ids          IDs
     * @param errorUserTip 用户提示信息
     * @return DOs
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default List<T> listByIdValidate(@Nullable Collection<Long> ids, @Nullable String errorUserTip)
            throws MybatisServiceException {
        if (Objects.isNull(ids) || ids.isEmpty()) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL, errorUserTip);
        }
        List<T> entities = this.list(Wrappers.<T>query().in(BaseDO.ID, ids));
        if (Objects.isNull(entities) || entities.isEmpty()) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.RESULT_SET_IS_NULL, errorUserTip);
        }
        return entities;
    }

    /**
     * 条件构造器获取DOs
     *
     * @param wrapper      wrapper
     * @param errorUserTip 用户提示信息
     * @return DOs
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default List<T> listValidate(Wrapper<T> wrapper, @Nullable String errorUserTip)
            throws MybatisServiceException {
        List<T> entities = this.list(wrapper);
        if (Objects.isNull(entities) || entities.isEmpty()) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL, errorUserTip);
        }
        return entities;
    }

    /**
     * 验证PageRequest并获取Pageable
     * 默认根据 {@link BaseDO#getUpdateTime()} 进行排序
     *
     * @param pageQuery pageQuery
     * @return Pageable
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default Pageable<List<T>> pageValidate(@Nullable PageQuery pageQuery) throws MybatisServiceException {
        return this.pageValidate(pageQuery, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 验证PageRequest并获取Pageable
     * 默认根据 {@link BaseDO#getUpdateTime()} 进行排序
     *
     * @param pageQuery    pageQuery
     * @param errorUserTip 用户提示信息
     * @return Pageable
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default Pageable<List<T>> pageValidate(@Nullable PageQuery pageQuery, @Nullable String errorUserTip)
            throws MybatisServiceException {
        if (Objects.isNull(pageQuery)) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL, errorUserTip);
        }
        Page<T> page = this.page(new Page<T>(pageQuery.getPageNo(), pageQuery.getPageSize())
                .addOrder(OrderItem.desc(BaseService.toUnderlineCase(BaseDO.UPDATE_TIME))));
        return new DefaultListPage<>(page.getCurrent(), page.getSize(), page.getTotal(), page.getRecords());
    }

    /**
     * 验证PageRequest并获取Pageable
     *
     * @param pageQuery pageQuery
     * @return Pageable
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default Pageable<List<T>> pageValidate(@Nullable IPage<T> pageQuery) throws MybatisServiceException {
        return this.pageValidate(pageQuery, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 验证PageRequest并获取Pageable
     *
     * @param pageQuery    pageQuery
     * @param errorUserTip 用户提示信息
     * @return Pageable
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default Pageable<List<T>> pageValidate(@Nullable IPage<T> pageQuery, @Nullable String errorUserTip)
            throws MybatisServiceException {
        if (Objects.isNull(pageQuery)) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL, errorUserTip);
        }

        IPage<T> page = this.page(pageQuery);
        return new DefaultListPage<>(page.getCurrent(), page.getSize(), page.getTotal(), page.getRecords());
    }

    /**
     * 验证实体类ID并保存
     *
     * @param entity 实体类
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException 参数为空|参数错误
     * @since 1.0.0
     */
    default boolean saveValidate(@Nullable T entity) throws MybatisServiceException {
        return this.saveValidate(entity, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 验证实体类ID并保存
     *
     * @param entity       实体类
     * @param errorUserTip 用户提示信息
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException 参数为空|参数错误
     * @since 1.0.0
     */
    default boolean saveValidate(@Nullable T entity, @Nullable String errorUserTip)
            throws MybatisServiceException {
        if (Objects.isNull(entity)) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL, errorUserTip);
        }
        return this.save(BaseService.clearEntityDefaultParameterAndGet(entity,
                true, true, true));
    }

    /**
     * 批量验证实体类ID并保存
     *
     * @param entities 实体对象集合
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException 参数为空|参数错误
     * @since 1.0.0
     */
    default boolean saveValidate(@Nullable Collection<T> entities) throws MybatisServiceException {
        return this.saveValidate(entities, 1000, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 批量验证实体类ID并保存
     *
     * @param entities     实体对象集合
     * @param errorUserTip 用户提示信息
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException 参数为空|参数错误
     * @since 1.0.0
     */
    default boolean saveValidate(@Nullable Collection<T> entities, @Nullable String errorUserTip)
            throws MybatisServiceException {
        return this.saveValidate(entities, 1000, errorUserTip);
    }

    /**
     * 批量验证实体类ID并保存
     *
     * @param entities  实体对象集合
     * @param batchSize 插入批次数量
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException 参数为空|参数错误
     * @since 1.0.0
     */
    default boolean saveValidate(@Nullable Collection<T> entities, int batchSize) throws MybatisServiceException {
        return this.saveValidate(entities, batchSize, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 批量验证实体类ID并保存
     *
     * @param entities     实体对象集合
     * @param batchSize    插入批次数量
     * @param errorUserTip 用户提示信息
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException 参数为空|参数错误
     * @since 1.0.0
     */
    default boolean saveValidate(@Nullable Collection<T> entities, int batchSize, @Nullable String errorUserTip)
            throws MybatisServiceException {
        if (Objects.isNull(entities) || entities.isEmpty()) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL, errorUserTip);
        }
        return this.saveBatch(BaseService.clearEntityDefaultParameterAndGet(entities,
                true, true, true), batchSize);
    }

    /**
     * 批量验证实体类ID并更新
     *
     * @param entity 实体类
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException 参数为空|参数错误
     * @since 1.0.0
     */
    default boolean updateByIdValidate(@Nullable T entity) throws MybatisServiceException {
        if (Objects.isNull(entity)) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL);
        }
        if (this.countByIdValidate(entity.getId()) == 0) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.RESULT_IS_NULL);
        }
        return this.updateByIdValidate(entity, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 验证实体类ID并更新
     *
     * @param entity       实体类
     * @param errorUserTip 用户提示信息
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException 参数为空|参数错误
     * @since 1.0.0
     */
    default boolean updateByIdValidate(@Nullable T entity, @Nullable String errorUserTip)
            throws MybatisServiceException {
        if (Objects.isNull(entity)) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL, errorUserTip);
        }
        if (this.countByIdValidate(entity.getId()) == 0) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.RESULT_IS_NULL, errorUserTip);
        }
        return this.updateById(clearEntityDefaultParameterAndGet(entity));
    }

    /**
     * 验证实体类ID并更新
     *
     * @param entities 实体对象集合
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException 参数为空|参数错误
     * @since 1.0.0
     */
    default boolean updateByIdValidate(@Nullable Collection<T> entities) throws MybatisServiceException {
        return this.updateByIdValidate(entities, 1000);
    }

    /**
     * 批量验证实体类ID并更新
     *
     * @param entities     实体对象集合
     * @param errorUserTip 用户提示信息
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException 参数为空|参数错误
     * @since 1.0.0
     */
    default boolean updateByIdValidate(@Nullable Collection<T> entities, @Nullable String errorUserTip)
            throws MybatisServiceException {
        return this.updateByIdValidate(entities, 1000, errorUserTip);
    }

    /**
     * 批量验证实体类ID并更新
     *
     * @param entities  实体对象集合
     * @param batchSize 插入批次数量
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException 参数为空|参数错误
     * @since 1.0.0
     */
    default boolean updateByIdValidate(@Nullable Collection<T> entities, int batchSize)
            throws MybatisServiceException {
        return this.updateByIdValidate(entities, batchSize, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 批量验证实体类ID并更新
     *
     * @param entities     实体对象集合
     * @param batchSize    插入批次数量
     * @param errorUserTip 用户提示信息
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException 参数为空|参数错误
     * @since 1.0.0
     */
    default boolean updateByIdValidate(@Nullable Collection<T> entities, int batchSize, @Nullable String errorUserTip)
            throws MybatisServiceException {
        if (Objects.isNull(entities) || entities.isEmpty()) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL, errorUserTip);
        }
        List<Long> ids = entities.stream().map(BaseDO::getId).collect(Collectors.toList());
        if (this.countByIdValidate(ids) != entities.size()) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.RESULT_IS_NULL, errorUserTip);
        }
        return this.updateBatchById(clearEntityDefaultParameterAndGet(entities), batchSize);
    }

    /**
     * 验证实体类ID并删除
     *
     * @param id ID
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default boolean removeByIdValidate(@Nullable Long id) throws MybatisServiceException {
        return this.removeByIdValidate(id, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 验证实体类ID并删除
     *
     * @param id           ID
     * @param errorUserTip 用户提示信息
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default boolean removeByIdValidate(@Nullable Long id, @Nullable String errorUserTip)
            throws MybatisServiceException {
        if (this.countByIdValidate(id) == 0) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.RESULT_IS_NULL, errorUserTip);
        }
        return this.removeById(id);
    }

    /**
     * 批量验证实体类ID并删除
     *
     * @param ids ID集合
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default boolean removeByIdValidate(@Nullable Collection<Long> ids) throws MybatisServiceException {
        return this.removeByIdValidate(ids, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 批量验证实体类ID并删除
     *
     * @param ids          ID集合
     * @param errorUserTip 用户提示信息
     * @return 操作是否成功 [null: 未执行, true: 成功, false: 失败]
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default boolean removeByIdValidate(@Nullable Collection<Long> ids, @Nullable String errorUserTip)
            throws MybatisServiceException {
        if (Objects.isNull(ids) || this.countByIdValidate(ids) != ids.size()) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.RESULT_IS_NULL, errorUserTip);
        }
        return this.removeByIds(ids);
    }

    // ------------------------- QueryLock -------------------------

    /**
     * 获取并锁定数据
     *
     * @param id ID
     * @return DO
     * @since 1.0.0
     */
    default T getAndLockIgnore(@Nullable Long id) {
        return this.getAndLockIgnoreOpt(id).orElse(null);
    }

    /**
     * 获取并锁定数据
     *
     * @param id ID
     * @return DO
     * @since 1.0.0
     */
    default Optional<T> getAndLockIgnoreOpt(@Nullable Long id) {
        if (Objects.isNull(id)) {
            return Optional.empty();
        }
        return Optional.ofNullable(this.getOne(Wrappers.<T>query().eq(BaseDO.ID, id).last(QUERY_LOCK)));
    }

    /**
     * 获取并锁定数据
     *
     * @param id ID
     * @return DO
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default T getAndLockValidate(@Nullable Long id) throws MybatisServiceException {
        return this.getAndLockValidate(id, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 获取并锁定数据
     *
     * @param id           ID
     * @param errorUserTip 用户提示信息
     * @return DO
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default T getAndLockValidate(@Nullable Long id, @Nullable String errorUserTip) throws MybatisServiceException {
        if (Objects.isNull(id)) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL, errorUserTip);
        }
        return this.getAndLockIgnoreOpt(id).orElseThrow(() ->
                new MybatisServiceException(MybatisServiceErrorEnum.RESULT_IS_NULL, errorUserTip));
    }

    /**
     * 获取并锁定数据
     *
     * @param ids ID集合
     * @return DO
     * @since 1.0.0
     */
    default Collection<T> getAndLockIgnore(@Nullable Collection<Long> ids) {
        return this.getAndLockIgnoreOpt(ids).orElse(Collections.emptyList());
    }

    /**
     * 获取并锁定数据
     *
     * @param ids ID集合
     * @return DO
     * @since 1.0.0
     */
    default Optional<Collection<T>> getAndLockIgnoreOpt(@Nullable Collection<Long> ids) {
        if (Objects.isNull(ids) || ids.isEmpty()) {
            return Optional.empty();
        }
        return Optional.ofNullable(this.list(Wrappers.<T>query().in(BaseDO.ID, ids).last(QUERY_LOCK)));
    }

    /**
     * 获取并锁定数据
     *
     * @param ids ID集合
     * @return DO
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default Collection<T> getAndLockValidate(@Nullable Collection<Long> ids) throws MybatisServiceException {
        return this.getAndLockValidate(ids, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 获取并锁定数据
     *
     * @param ids          ID集合
     * @param errorUserTip 用户提示信息
     * @return DO
     * @throws MybatisServiceException ID为空|ID错误
     * @since 1.0.0
     */
    default Collection<T> getAndLockValidate(@Nullable Collection<Long> ids, @Nullable String errorUserTip) throws MybatisServiceException {
        if (Objects.isNull(ids) || ids.isEmpty()) {
            throw new MybatisServiceException(MybatisServiceErrorEnum.ID_IS_NULL, errorUserTip);
        }
        return this.getAndLockIgnoreOpt(ids).orElseThrow(() ->
                new MybatisServiceException(MybatisServiceErrorEnum.RESULT_IS_NULL, errorUserTip));
    }

    // ------------------------- Util -------------------------

    /**
     * 清除实体类默认参数值
     *
     * @param entity 实体类
     * @param <T>    实体类类型
     * @return 操作后的实体类
     * @since 1.0.0
     */
    static <T extends BaseDO> T clearEntityDefaultParameterAndGet(T entity) {
        return BaseService.clearEntityDefaultParameterAndGet(entity,
                false, true, true);
    }

    /**
     * 批量清除实体类默认参数值
     *
     * @param entities 实体对象集合
     * @param <T>      实体类类型
     * @return 操作后的实体类
     * @since 1.0.0
     */
    static <T extends BaseDO> List<T> clearEntityDefaultParameterAndGet(Collection<T> entities) {
        return entities.stream().map(entity -> BaseService.clearEntityDefaultParameterAndGet(entity,
                false, true, true)).collect(Collectors.toList());
    }

    /**
     * 清除实体类默认参数值
     *
     * @param entity            实体类
     * @param isClearId         是否清除ID
     * @param isClearCreateTime 是否清除创建时间
     * @param isClearModifyTime 是否清除更新时间
     * @param <T>               实体类类型
     * @return 操作后的实体类
     * @since 1.0.0
     */
    static <T extends BaseDO> T clearEntityDefaultParameterAndGet(
            T entity, boolean isClearId, boolean isClearCreateTime, boolean isClearModifyTime) {
        if (isClearId) {
            entity.setId(null);
        }
        if (isClearCreateTime) {
            entity.setCreateTime(null);
        }
        if (isClearModifyTime) {
            entity.setUpdateTime(null);
        }
        return entity;
    }

    /**
     * 批量清除实体类默认参数值
     *
     * @param entities          实体对象集合
     * @param isClearId         是否清除ID
     * @param isClearCreateTime 是否清除创建时间
     * @param isClearModifyTime 是否清除更新时间
     * @param <T>               实体类类型
     * @return 清除参数后的实体对象集合
     * @since 1.0.0
     */
    static <T extends BaseDO> Collection<T> clearEntityDefaultParameterAndGet(
            Collection<T> entities, boolean isClearId, boolean isClearCreateTime, boolean isClearModifyTime) {
        return entities.stream().map(entity -> BaseService.clearEntityDefaultParameterAndGet(entity,
                isClearId, isClearCreateTime, isClearModifyTime)).collect(Collectors.toList());
    }

    /**
     * 简易驼峰转下划线
     *
     * @param value 驼峰字符串
     * @return 下划线字符串
     * @since 1.0.0
     */
    static String toUnderlineCase(@NonNull String value) {
        Assert.notNull(value, "UnderlineCase value must not be null");

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < value.length(); i++) {
            char c = value.charAt(i);
            if (Character.isUpperCase(c)) {
                result.append("_").append(Character.toLowerCase(c));
            } else {
                result.append(c);
            }
        }
        return result.toString();
    }
}
