package cn.krismile.persagy.webstack.function.business.service;

import cn.krismile.boot.framework.core.exception.client.RequestParamException;
import cn.krismile.boot.framework.core.exception.client.RequestServerException;
import cn.krismile.boot.framework.mybatisplus.exception.MybatisServiceException;
import cn.krismile.persagy.webstack.pojo.domain.BizRelNavSysAdminDO;
import cn.krismile.boot.framework.mybatisplus.service.BaseService;
import cn.krismile.persagy.webstack.pojo.dto.WebsiteSortDTO;
import org.springframework.lang.NonNull;

/**
 * 业务-导航-用户关联表 Service
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface BizRelNavSysAdminService extends BaseService<BizRelNavSysAdminDO> {

    String DEFAULT_NOT_FOUND_MESSAGE = "未找到用户关联的导航栏";

    /**
     * 检查当前用户是否关联此导航栏
     *
     * @param navId 导航栏ID
     * @return 是否已关联
     * @throws MybatisServiceException 导航栏不存在|未找到用户关联的导航栏
     * @throws RequestServerException  用户ID获取失败
     * @since 1.0.0
     */
    boolean checkRelation(@NonNull Long navId) throws MybatisServiceException, RequestServerException;

    /**
     * 关联导航栏和用户
     *
     * @param navId       导航栏ID
     * @param orderWeight 导航栏权重
     * @return 操作是否成功
     * @throws MybatisServiceException 导航栏不存在|未找到用户关联的导航栏
     * @throws RequestServerException  用户ID获取失败
     * @since 1.0.0
     */
    boolean relation(@NonNull Long navId, @NonNull Integer orderWeight)
            throws MybatisServiceException, RequestServerException;

    /**
     * 设置排序权重
     *
     * @param navId       导航栏ID
     * @param orderWeight 排序权重
     * @return 操作是否成功
     * @throws MybatisServiceException 导航栏不存在
     * @throws RequestParamException   未找到用户关联的导航栏
     * @throws RequestServerException  用户ID获取失败
     * @since 1.0.0
     */
    boolean editSort(@NonNull Long navId, @NonNull Integer orderWeight)
            throws MybatisServiceException, RequestServerException;

    /**
     * 导航栏排序
     *
     * @param dto 导航栏排序DTO
     * @return 操作是否成功
     * @throws MybatisServiceException 导航栏不存在
     * @throws RequestParamException   未找到用户关联的导航栏
     * @throws RequestServerException  用户ID获取失败
     * @since 1.0.0
     */
    boolean sortExchange(@NonNull WebsiteSortDTO dto)
            throws MybatisServiceException, RequestServerException;

    /**
     * 删除
     *
     * @param navId 导航栏ID
     * @return 操作是否成功
     * @throws MybatisServiceException 导航栏不存在
     * @throws RequestParamException   未找到用户关联的导航栏
     * @throws RequestServerException  用户ID获取失败
     * @since 1.0.0
     */
    boolean del(@NonNull Long navId) throws MybatisServiceException, RequestServerException;

}
