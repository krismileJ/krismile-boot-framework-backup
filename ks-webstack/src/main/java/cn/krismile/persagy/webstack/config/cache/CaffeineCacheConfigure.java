package cn.krismile.persagy.webstack.config.cache;

import com.alibaba.fastjson2.JSON;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.Expiry;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.index.qual.NonNegative;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.convert.DurationStyle;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.time.Duration;

/**
 * Caffeine缓存配置类
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@SpringBootConfiguration
@Slf4j
public class CaffeineCacheConfigure extends CachingConfigurerSupport {

    private static final String LOG_TAG = "本地缓存";

    /**
     * 缓存默认过期时间
     */
    public static final Duration DEFAULT_CACHE_EXPIRE_TIME = Duration.ofMinutes(1);

    @Bean
    @Override
    public CacheManager cacheManager() {
        CaffeineCacheManager caffeineCacheManager = new CaffeineCacheManager();
        caffeineCacheManager.setCaffeine(Caffeine.newBuilder()
                .initialCapacity(5)
                .maximumSize(100)
                .recordStats()
                .expireAfter(new Expiry<Object, Object>() {
                    @Override
                    public long expireAfterCreate(@NonNull Object key, @NonNull Object value, long currentTime) {
                        // 每次新建缓存时自动解析缓存过期时间
                        return cacheExpireTime(key);
                    }

                    @Override
                    public long expireAfterUpdate(@NonNull Object key, @NonNull Object value, long currentTime, @NonNegative long currentDuration) {
                        // 每次新建缓存时自动解析缓存过期时间
                        return cacheExpireTime(key);
                    }

                    @Override
                    public long expireAfterRead(@NonNull Object key, @NonNull Object value, long currentTime, @NonNegative long currentDuration) {
                        // 当每次读取缓存后依然使用原有的缓存剩余时间, 不进行重置
                        return currentDuration;
                    }
                })
        );
        return caffeineCacheManager;
    }

    @Bean
    @Override
    public CacheErrorHandler errorHandler() {
        return new CacheErrorHandler() {
            @Override
            public void handleCacheGetError(@NonNull RuntimeException exception, @NonNull Cache cache, @NonNull Object key) {
                log.warn("[{}] 获取缓存失败 [cacheName: {}, key: {}, value: {}]", LOG_TAG,
                        cache.getName(), key, JSON.toJSONString(cache.get(key)), exception);
            }

            @Override
            public void handleCachePutError(@NonNull RuntimeException exception, @NonNull Cache cache, @NonNull Object key, @Nullable Object value) {
                log.warn("[{}] 更新缓存失败 [cacheName: {}, key: {}, originValue: {}, targetValue: {}]", LOG_TAG,
                        cache.getName(), key, JSON.toJSONString(cache.get(key)), JSON.toJSONString(value), exception);
            }

            @Override
            public void handleCacheEvictError(@NonNull RuntimeException exception, @NonNull Cache cache, @NonNull Object key) {
                log.warn("[{}] 删除缓存失败 [cacheName: {}, key: {}, value: {}]", LOG_TAG,
                        cache.getName(), key, JSON.toJSONString(cache.get(key)), exception);
            }

            @Override
            public void handleCacheClearError(@NonNull RuntimeException exception, @NonNull Cache cache) {
                log.warn("[{}] 清空缓存失败 [cacheName: {}]", LOG_TAG, cache.getName(), exception);
            }
        };
    }

    /**
     * 获取缓存过期时间
     *
     * <p>支持的单位:
     * <ul>
     *     <li>纳秒: {@code ns}</li>
     *     <li>微秒: {@code us}</li>
     *     <li>毫秒: {@code ms}</li>
     *     <li>秒: {@code s}</li>
     *     <li>分钟: {@code m}</li>
     *     <li>小时: {@code h}</li>
     *     <li>天: {@code d}</li>
     * </ul>
     *
     * @param key 缓存Key
     * @return 缓存过期时间, 单位为纳秒, 当 {@code key} 解析失败时返回 {@link CaffeineCacheConfigure#DEFAULT_CACHE_EXPIRE_TIME} 默认缓存过期时间
     * @since 1.0.0
     */
    private long cacheExpireTime(@NonNull Object key) {
        try {
            String keyString = key.toString();
            String time = keyString.substring(keyString.lastIndexOf("-") + 1);
            // noinspection AlibabaUndefineMagicConstant
            if ("*".equals(time)) {
                return Long.MAX_VALUE;
            }
            return DurationStyle.detectAndParse(time).toNanos();
        } catch (Exception e) {
            log.warn("[{}] 解析过期时间失败, 将使用默认过期时间 [key: {}, defaultExpireTime: {}s]",
                    LOG_TAG, key, DEFAULT_CACHE_EXPIRE_TIME.getSeconds());
            return DEFAULT_CACHE_EXPIRE_TIME.toNanos();
        }
    }
}
