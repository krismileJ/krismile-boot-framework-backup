package cn.krismile.persagy.webstack.function.system.mapper;

import cn.krismile.persagy.webstack.pojo.domain.SysAdminDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 系统-用户表 Mapper 接口
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Mapper
@Repository
public interface SysAdminMapper extends BaseMapper<SysAdminDO> {


    /**
     * 有效的ID列表查询
     *
     * @return 有效的ID列表
     * @since 1.0.0
     */
    @Select("select sa.id from sys_admin sa where sa.status = 1")
    List<Long> listIdsInEnable();

}
