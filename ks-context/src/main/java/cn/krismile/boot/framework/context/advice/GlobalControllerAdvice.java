package cn.krismile.boot.framework.context.advice;

import cn.krismile.boot.framework.context.storage.LocalContextStorage;
import cn.krismile.boot.framework.context.util.IpUtils;
import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.boot.framework.core.annotation.jsr.Nullable;
import cn.krismile.boot.framework.core.constant.key.ContextKey;
import cn.krismile.boot.framework.core.enumeration.error.client.RequestParamErrorEnum;
import cn.krismile.boot.framework.core.enumeration.error.client.RequestServerErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;
import cn.krismile.boot.framework.core.exception.EnumIllegalArgumentException;
import cn.krismile.boot.framework.core.exception.StackTraceException;
import cn.krismile.boot.framework.core.exception.ThirdPartyException;
import cn.krismile.boot.framework.core.response.ResultVO;
import cn.krismile.boot.framework.core.response.vo.BaseVO;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.text.ParseException;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 全局Controller异常处理
 *
 * <p>该类具体实现了全局请求异常拦截, 对于不同的异常有不同的拦截并返回, 无论是自定义异常/校验异常/未知异常等都具有详细的实现,
 * 拦截后给予客户端友好返回, 同时将异常规范的写出到日志文件中, 便于项目运行中通过日志排查问题
 * <p>允许输出用户自定义信息, 请在 {@link LocalContextStorage} 中添加 {@code key} 为 {@link ContextKey#DIV_REQUEST_INFO} 的数据
 * 例如: {@code LocalStorage.set(GlobalKey.Context.REQUEST_INFO, "This is my request info.")}</p>
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@RestControllerAdvice
public class GlobalControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalControllerAdvice.class);

    private static final String LOG_TAG = "KS-GlobalAdvice-Controller";

    private static final Pattern ENUM_ILLEGAL_ARGUMENT_PRINT_PATTERN = Pattern.compile("(\\[.*])\\s");

    // ------------------------- 基础验证 -------------------------

    /**
     * 自定义异常全局捕获
     *
     * @param e       {@link cn.krismile.boot.framework.core.exception.ApplicationException}
     * @param request {@link javax.servlet.http.HttpServletRequest}
     * @return 结果响应
     * @since 1.0.0
     */
    @ExceptionHandler(ApplicationException.class)
    public BaseVO generalErrorHandle(ApplicationException e, HttpServletRequest request) {
        String clientIp = IpUtils.getIpv4Address(request);
        LOGGER.warn("[{}] ------------------------- 自定义异常信息 ------------------------- [Begin]", LOG_TAG);
        printStackTraceFormat(request.getRequestURI(), request.getMethod(), e, e.getCause(),
                "自定义异常信息", clientIp, getRequestInfo(), null, false);
        LOGGER.warn("[{}] ------------------------- 自定义异常信息 ------------------------- [ End ]", LOG_TAG);
        return ResultVO.base(e.getBaseEnum(), e.getUserTip());
    }

    /**
     * 堆栈异常全局捕获
     *
     * @param e       {@link cn.krismile.boot.framework.core.exception.StackTraceException}
     * @param request {@link javax.servlet.http.HttpServletRequest}
     * @return 结果响应
     * @since 1.0.0
     */
    @ExceptionHandler(StackTraceException.class)
    public BaseVO stackTracerErrorHandler(StackTraceException e, HttpServletRequest request) {
        String clientIp = IpUtils.getIpv4Address(request);
        LOGGER.error("[{}] ------------------------- 堆栈异常信息 ------------------------- [Begin]", LOG_TAG);
        LOGGER.error("[{}] 堆栈异常信息 [requestUri: {}, requestMethod: {}, clientIp: {}, requestInfo: {}, errorMessage: {}]",
                LOG_TAG, request.getRequestURI(), request.getMethod(), clientIp, getRequestInfo(), e.getLocalizedMessage(), e);
        LOGGER.error("[{}] ------------------------- 堆栈异常信息 ------------------------- [ End ]", LOG_TAG, e);
        return ResultVO.base(RequestServerErrorEnum.SERVICE_ERROR, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 第三方异常全局捕获
     *
     * @param e       {@link cn.krismile.boot.framework.core.exception.ThirdPartyException}
     * @param request {@link javax.servlet.http.HttpServletRequest}
     * @return 结果响应
     * @since 1.0.0
     */
    @ExceptionHandler(ThirdPartyException.class)
    public BaseVO thirdPartyErrorHandler(ThirdPartyException e, HttpServletRequest request) {
        String clientIp = IpUtils.getIpv4Address(request);
        LOGGER.error("[{}] ------------------------- 第三方异常信息 ------------------------- [Begin]", LOG_TAG);
        LOGGER.error("[{}] 第三方异常信息 [requestUri: {}, requestMethod: {}, clientIp: {}, requestInfo: {}, errorCode: {}, errorMessage: {}]",
                LOG_TAG, request.getRequestURI(), request.getMethod(), clientIp, getRequestInfo(), e.getErrorCode(), e.getErrorMessage(), e);
        LOGGER.error("[{}] ------------------------- 第三方异常信息 ------------------------- [ End ]", LOG_TAG);
        return ResultVO.base(RequestServerErrorEnum.SERVICE_ERROR.value(),
                e.getLocalizedMessage(), ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 系统异常全局捕获
     *
     * @param e       {@link java.lang.Exception}
     * @param request {@link javax.servlet.http.HttpServletRequest}
     * @return 结果响应
     * @since 1.0.0
     */
    @ExceptionHandler(Exception.class)
    public BaseVO serverErrorHandler(Exception e, HttpServletRequest request) {
        String clientIp = IpUtils.getIpv4Address(request);
        LOGGER.error("[{}] ------------------------- 系统异常信息 ------------------------- [Begin]", LOG_TAG);
        LOGGER.error("[{}] 系统异常信息 [requestUri: {}, requestMethod: {}, clientIp: {}, requestInfo: {}, errorMessage: {}]",
                LOG_TAG, request.getRequestURI(), request.getMethod(), clientIp, getRequestInfo(), e.getLocalizedMessage(), e);
        LOGGER.error("[{}] ------------------------- 系统异常信息 ------------------------- [ End ]", LOG_TAG, e);
        return ResultVO.base(RequestServerErrorEnum.SERVICE_ERROR, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    // ------------------------- 请求参数验证 -------------------------

    /**
     * 参数为空全局捕获
     * <p>RequestType = <b>{@link org.springframework.web.bind.annotation.RequestParam}</b>
     * <p>捕获来自 {@link org.springframework.web.bind.annotation.RequestParam}
     *
     * @param e       {@link org.springframework.web.bind.MissingServletRequestParameterException}
     * @param request {@link javax.servlet.http.HttpServletRequest}
     * @return 结果响应
     * @since 1.0.0
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public BaseVO validationExceptionHandler(MissingServletRequestParameterException e, HttpServletRequest request) {
        String clientIp = IpUtils.getIpv4Address(request);
        LOGGER.warn("[{}] ------------------------- 参数为空 ------------------------- [Begin]", LOG_TAG);
        LOGGER.warn("[{}] 参数为空 [requestUri: {}, requestMethod: {}, clientIp: {}, requestInfo: {}, errorMessage: {}]",
                LOG_TAG, request.getRequestURI(), request.getMethod(), clientIp, getRequestInfo(), e.getLocalizedMessage());
        LOGGER.warn("[{}] ------------------------- 参数为空 ------------------------- [ End ]", LOG_TAG);
        return ResultVO.base(RequestParamErrorEnum.PARAMETER_EMPTY.value(),
                e.getLocalizedMessage(), e.getLocalizedMessage());
    }

    /**
     * 参数转换异常全局捕获
     *
     * @param e       {@link org.springframework.web.method.annotation.MethodArgumentTypeMismatchException}
     * @param request {@link javax.servlet.http.HttpServletRequest}
     * @return 结果响应
     * @since 1.0.0
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public JSONObject validationExceptionHandler(MethodArgumentTypeMismatchException e, HttpServletRequest request) {
        String clientIp = IpUtils.getIpv4Address(request);
        LOGGER.warn("[{}] ------------------------- 参数转换异常 ------------------------- [Begin]", LOG_TAG);
        String errorMessage = this.printStackTraceFormat(request.getRequestURI(), request.getMethod(), e, e.getCause(),
                "参数转换异常", clientIp, getRequestInfo(), e.getLocalizedMessage(), false);
        LOGGER.warn("[{}] ------------------------- 参数转换异常 ------------------------- [ End ]", LOG_TAG);
        return new JSONObject()
                .fluentPut(BaseVO.ERROR_CODE, RequestParamErrorEnum.PARAMETER_FORMAT_NOT_MATCH.value())
                .fluentPut(BaseVO.ERROR_MESSAGE, errorMessage)
                .fluentPut(BaseVO.USER_TIP, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 验证异常全局捕获
     * <p>RequestType = <b>{@link org.springframework.web.bind.annotation.RequestParam}</b>
     * <p>捕获来自 {@link javax.validation.constraints}
     *
     * @param e       {@link javax.validation.ValidationException}
     * @param request {@link javax.servlet.http.HttpServletRequest}
     * @return 结果响应
     * @since 1.0.0
     */
    @ExceptionHandler(ValidationException.class)
    public JSONObject validationExceptionHandler(ValidationException e, HttpServletRequest request) {
        String clientIp = IpUtils.getIpv4Address(request);
        LOGGER.warn("[{}] ------------------------- 验证异常 ------------------------- [Begin]", LOG_TAG);
        LOGGER.warn("[{}] 验证异常 [requestUri: {}, requestMethod: {}, clientIp: {}, requestInfo: {}, errorMessage: {}]",
                LOG_TAG, request.getRequestURI(), request.getMethod(), clientIp, getRequestInfo(), e.getLocalizedMessage());
        boolean isPrintStackTraceDetail = false;
        List<String> userTipMessages = new ArrayList<>();
        if (e instanceof ConstraintViolationException) {
            Set<ConstraintViolation<?>> constraintViolations = ((ConstraintViolationException) e).getConstraintViolations();
            for (ConstraintViolation<?> constraintViolation : constraintViolations) {
                userTipMessages.add(constraintViolation.getMessage());
            }
        } else {
            isPrintStackTraceDetail = true;
            userTipMessages.add(e.getLocalizedMessage());
        }
        this.printStackTraceFormat(request.getRequestURI(), request.getMethod(), e, e.getCause(),
                "验证异常", clientIp, getRequestInfo(), null, isPrintStackTraceDetail);
        LOGGER.warn("[{}] ------------------------- 验证异常 ------------------------- [ End ]", LOG_TAG);
        return new JSONObject()
                .fluentPut(BaseVO.ERROR_CODE, RequestParamErrorEnum.INVALID_INPUT.value())
                .fluentPut(BaseVO.ERROR_MESSAGE, e.getLocalizedMessage())
                .fluentPut(BaseVO.USER_TIP, String.join(",", userTipMessages));
    }

    /**
     * 请求体异常全局捕获
     * <p>RequestType = <b>{@link org.springframework.web.bind.annotation.RequestBody}</b>
     * <p>Also throw to {@link InvalidFormatException}
     *
     * @param e       {@link org.springframework.http.converter.HttpMessageNotReadableException}
     * @param request {@link javax.servlet.http.HttpServletRequest}
     * @return 结果响应
     * @see com.fasterxml.jackson.databind.DeserializationContext#weirdStringException(String, Class, String)
     * <p>Enum deserialize failed exception is unsolved
     * @since 1.0.0
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public JSONObject validationExceptionHandler(HttpMessageNotReadableException e, HttpServletRequest request) {
        String clientIp = IpUtils.getIpv4Address(request);
        LOGGER.warn("[{}] ------------------------- 请求体异常 ------------------------- [Begin]", LOG_TAG);
        String errorMessage = this.printStackTraceFormat(request.getRequestURI(), request.getMethod(), e, e.getCause(),
                "请求体异常", clientIp, getRequestInfo(), e.getLocalizedMessage(), false);
        LOGGER.warn("[{}] 请求体异常 [requestUri: {}, requestMethod: {}, clientIp: {}, requestInfo: {}, errorMessage: {}]",
                LOG_TAG, request.getRequestURI(), request.getMethod(), clientIp, getRequestInfo(), errorMessage);
        LOGGER.warn("[{}] ------------------------- 请求体异常 ------------------------- [ End ]", LOG_TAG);
        return new JSONObject()
                .fluentPut(BaseVO.ERROR_CODE, RequestParamErrorEnum.PARAMETER_FORMAT_NOT_MATCH.value())
                .fluentPut(BaseVO.ERROR_MESSAGE, errorMessage)
                .fluentPut(BaseVO.USER_TIP, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 绑定异常全局捕获
     *
     * <p>RequestType = <b>{@link org.springframework.web.bind.annotation.RequestParam}</b>
     *
     * @param e       {@link org.springframework.validation.BindException}
     * @param request {@link javax.servlet.http.HttpServletRequest}
     * @return 结果响应
     * @since 1.0.0
     */
    @ExceptionHandler(BindException.class)
    public JSONObject validationExceptionHandler(BindException e, HttpServletRequest request) {
        String clientIp = IpUtils.getIpv4Address(request);
        LOGGER.warn("[{}] ------------------------- 绑定异常 ------------------------- [Begin]", LOG_TAG);
        List<String> errorMessages = analyzeAndPrintBindErrorMessage(e, request.getRequestURI(), request.getMethod(),
                "绑定异常", clientIp, getRequestInfo());
        LOGGER.warn("[{}] ------------------------- 绑定异常 ------------------------- [ End ]", LOG_TAG);
        return new JSONObject()
                .fluentPut(BaseVO.ERROR_CODE, RequestParamErrorEnum.INVALID_INPUT.value())
                .fluentPut(BaseVO.ERROR_MESSAGE, String.join(",", errorMessages))
                .fluentPut(BaseVO.USER_TIP, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 请求体验证全局捕获
     * <p>RequestType = <b>{@link org.springframework.web.bind.annotation.RequestBody}</b>
     * <p>捕获来自 {@link javax.validation.constraints}
     *
     * @param e       {@link org.springframework.web.bind.MethodArgumentNotValidException}
     * @param request {@link javax.servlet.http.HttpServletRequest}
     * @return 结果响应
     * @since 1.0.0
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public JSONObject validationExceptionHandler(MethodArgumentNotValidException e, HttpServletRequest request) {
        String clientIp = IpUtils.getIpv4Address(request);
        LOGGER.warn("[{}] ------------------------- 请求体验证异常 ------------------------- [Begin]", LOG_TAG);
        List<String> errorMessages = analyzeAndPrintBindErrorMessage(e, request.getRequestURI(), request.getMethod(),
                "请求体验证异常", clientIp, getRequestInfo());
        LOGGER.warn("[{}] ------------------------- 请求体验证异常 ------------------------- [ End ]", LOG_TAG);
        return new JSONObject()
                .fluentPut(BaseVO.ERROR_CODE, RequestParamErrorEnum.INVALID_INPUT.value())
                .fluentPut(BaseVO.ERROR_MESSAGE, String.join(", ", errorMessages))
                .fluentPut(BaseVO.USER_TIP, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 方法不被允许全局捕获
     *
     * @param e       HttpRequestMethodNotSupportedException
     * @param request {@link javax.servlet.http.HttpServletRequest}
     * @return 结果响应
     * @since 1.0.0
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public JSONObject validationExceptionHandler(HttpRequestMethodNotSupportedException e, HttpServletRequest request) {
        String clientIp = IpUtils.getIpv4Address(request);
        LOGGER.warn("[{}] ------------------------- 方法不被允许 ------------------------- [Begin]", LOG_TAG);
        LOGGER.warn("[{}] 方法不被允许 [requestUri: {}, requestMethod: {}, clientIp: {}, requestInfo: {}, errorMessage: {}]",
                LOG_TAG, request.getRequestURI(), request.getMethod(), clientIp, getRequestInfo(), e.getLocalizedMessage());
        this.printStackTraceFormat(request.getRequestURI(), request.getMethod(), e, e.getCause(),
                "方法不被允许", clientIp, getRequestInfo(), null, false);
        LOGGER.warn("[{}] ------------------------- 方法不被允许 ------------------------- [ End ]", LOG_TAG);
        return new JSONObject()
                .fluentPut(BaseVO.ERROR_CODE, RequestParamErrorEnum.INVALID_INPUT.value())
                .fluentPut(BaseVO.ERROR_MESSAGE, e.getLocalizedMessage())
                .fluentPut(BaseVO.USER_TIP, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 格式化异常信息|堆栈异常打印
     *
     * <ul>
     *     <li>
     *         <b>EnumIllegalArgumentException</b> -
     *         为了保证输出安全, 采用正则表达式 {@code "(\\[.*])\\s"}
     *         来匹配 {@link cn.krismile.boot.framework.core.enumeration.BaseEnum#analyze(Object, Class)}
     *         中解析异常所抛出的 {@link EnumIllegalArgumentException} 异常信息, 将异常信息的全限定名进行删除后输出
     *     </li>
     *     <li>
     *         <b>DateTimeParseException</b> - 时间解析异常直接打印原始信息
     *     </li>
     * </ul>
     *
     * @param requestUri              请求地址
     * @param requestMethod           请求方法
     * @param tp                      {@code t} 父节点, 即原始节点
     * @param t                       {@code tp} 子节点, 当子节点未 {@code null} 时则不进行递归打印
     * @param tag                     日志标签
     * @param clientIp                客户端IP
     * @param requestInfo             自定义请求信息
     * @param originalErrorMessage    原始错误信息
     * @param isPrintStackTraceDetail 是否输出堆栈详细信息
     * @return 格式化后的错误信息
     * @see EnumIllegalArgumentException
     * @since 1.0.0
     */
    @Nullable
    private String printStackTraceFormat(
            @NonNull String requestUri, @NonNull String requestMethod,
            @NonNull Throwable tp, @Nullable Throwable t,
            @NonNull String tag, @NonNull String clientIp,
            @Nullable Object requestInfo, @Nullable String originalErrorMessage,
            boolean isPrintStackTraceDetail) {
        if (t == null) {
            if (tp instanceof EnumIllegalArgumentException) {
                Matcher matcher = ENUM_ILLEGAL_ARGUMENT_PRINT_PATTERN.matcher(tp.getLocalizedMessage());
                originalErrorMessage = tp.getLocalizedMessage();
                while (matcher.find()){
                    originalErrorMessage = originalErrorMessage.replace(matcher.group(), "");
                }
            } else if (tp instanceof JsonParseException) {
                originalErrorMessage = tp.getLocalizedMessage();
            } else if (tp instanceof ParseException) {
                originalErrorMessage = tp.getLocalizedMessage();
            } else if (tp instanceof DateTimeParseException) {
                originalErrorMessage = tp.getLocalizedMessage().split(":")[0];
            } else if (tp instanceof InvalidFormatException) {
                Object originValue = ((InvalidFormatException) tp).getValue();
                originalErrorMessage = String.format("Cannot deserialize value from \"%s\"", originValue);
            }
            Throwable printThrowable = isPrintStackTraceDetail ? tp : null;
            LOGGER.warn("[{}] {} [requestUri: {}, requestMethod: {}, clientIp, {}, requestInfo: {}, errorMessage: {}]",
                    LOG_TAG, tag, requestUri, requestMethod, clientIp, requestInfo,
                    tp.getLocalizedMessage(), printThrowable);
            return originalErrorMessage;
        } else {
            return this.printStackTraceFormat(requestUri, requestMethod, t, t.getCause(),
                    tag, clientIp, requestInfo, originalErrorMessage, isPrintStackTraceDetail);
        }
    }

    /**
     * 获取请求信息
     *
     * @return 请求信息
     * @since 1.0.0
     */
    @Nullable
    private String getRequestInfo() {
        return JSON.toJSONString(LocalContextStorage.get(ContextKey.DIV_REQUEST_INFO));
    }

    /**
     * 解析并打印验证错误消息
     *
     * @param e             绑定异常
     * @param requestUri    请求地址
     * @param requestMethod 请求方法
     * @param tag           日志标签
     * @param clientIp      客户端IP
     * @param requestInfo   自定义请求信息
     * @return 解析后的验证错误信息
     * @since 1.0.0
     */
    private List<String> analyzeAndPrintBindErrorMessage(
            @NonNull BindException e, @NonNull String requestUri,
            @NonNull String requestMethod, @NonNull String tag,
            @NonNull String clientIp, @Nullable Object requestInfo) {
        List<String> result = new ArrayList<>();
        for (FieldError fieldError : e.getFieldErrors()) {
            result.add(String.format("%s: %s", fieldError.getField(), fieldError.getDefaultMessage()));
            LOGGER.warn("[{}] {} [requestUri: {}, requestMethod: {}, clientIp: {}, requestInfo: {}, errorType: @{}, errorMessage: {}, field: {}={}]",
                    LOG_TAG, tag, requestUri, requestMethod, clientIp, requestInfo, fieldError.getCode(),
                    fieldError.getDefaultMessage(), fieldError.getField(), fieldError.getRejectedValue());
        }
        if (!result.isEmpty()) {
            return result;
        }

        for (ObjectError globalError : e.getGlobalErrors()) {
            result.add(String.format("%s: %s", globalError.getObjectName(), globalError.getDefaultMessage()));
            LOGGER.warn("[{}] {} [requestUri: {}, requestMethod: {}, clientIp: {}, requestInfo: {}, errorType: @{}, errorMessage: {}, object: {}]",
                    LOG_TAG, tag, requestUri, requestMethod, clientIp, requestInfo, globalError.getCode(),
                    globalError.getDefaultMessage(), globalError.getObjectName());
        }
        if (!result.isEmpty()) {
            return result;
        }

        result.add(printStackTraceFormat(requestUri, requestMethod, e, e.getCause(),
                tag, clientIp, requestInfo, e.getLocalizedMessage(), false));
        return result;
    }
}
