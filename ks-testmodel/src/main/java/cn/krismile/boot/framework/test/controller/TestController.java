package cn.krismile.boot.framework.test.controller;

import cn.krismile.boot.framework.cache.redis.RedisCache;
import cn.krismile.boot.framework.core.exception.ThirdPartyException;
import cn.krismile.boot.framework.test.pojo.TestDTO;
import cn.krismile.boot.framework.test.pojo.TestEntity;
import cn.krismile.boot.framework.test.service.TestService;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import java.time.Duration;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * TODO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@RestController
//@Validated
public class TestController {

    @Autowired
    private TestService testService;
    @Autowired
    private TestEntity testEntity;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;
    @Autowired
    private RedisCache redisCache;

    @GetMapping("/a")
    public Object a(@RequestParam Boolean isForce) throws InterruptedException {
        throw new ThirdPartyException("", "");
        // return isForce;
    }
    @GetMapping("/set")
    public Object set() {
        System.out.println(redisTemplate.opsForHash().hasKey("111", "111222"));
        redisTemplate.opsForHash().put("111", "111222", "this is value");
        redisTemplate.opsForHash().put("111", "222", null);
        System.out.println(redisTemplate.getExpire("111"));
        redisTemplate.expire("111", Duration.ofSeconds(50));
        System.out.println(redisTemplate.getExpire("111", TimeUnit.MINUTES));
        redisTemplate.expire("111", Duration.ofSeconds(70));
        System.out.println(redisTemplate.getExpire("111", TimeUnit.MINUTES));
        redisTemplate.expire("111", Duration.ofSeconds(600));
        System.out.println(redisTemplate.getExpire("111", TimeUnit.MINUTES));
        return null;
    }

    @GetMapping("/date")
    public PostEntity date(@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date date, @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") Date date1) {
        PostEntity postEntity = new PostEntity();
        postEntity.setDate(date);
        return postEntity;
    }

    @GetMapping("/get")
    public Object get(Integer param1, @NotBlank String param2) {
        testService.test();
        testEntity.setId("123");
        testEntity.getId();
        return param1 + param2;
    }

    @GetMapping("/convert")
    public Object get() {
        TestEntity testEntity1 = new TestEntity();
        testEntity1.setId("123");
        testEntity1.setName("321");
        return testEntity1;
    }

    @GetMapping("/get/a.js")
    public Object geta() {
        return "OOO000";
    }

    @PostMapping("/post")
    public Object post(@RequestBody @Validated PostEntity postEntity) {
        return postEntity;
    }

    @GetMapping("/valid")
    public Object valid(TestDTO postEntity) {
        testService.test(postEntity, null);
        return null;
    }

    @PostMapping("/time")
    public Object time(@RequestBody TestDTO postEntity) {
        return postEntity;
    }

    @PostMapping("/upload")
    public Object upload(@RequestBody MultipartFile file) {
        return "This is a file.";
    }

    @Data
    @Accessors(chain = true)
    public static class PostEntity {
        @NotBlank
        private String id;
        private String name;
        private Date date;
    }
}
