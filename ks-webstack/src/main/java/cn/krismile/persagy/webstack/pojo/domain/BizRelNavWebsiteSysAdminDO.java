package cn.krismile.persagy.webstack.pojo.domain;

import cn.krismile.boot.framework.mybatisplus.domain.BaseDO;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 业务-导航-站点-用户关联表
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("biz_rel_nav_website_sys_admin")
@Schema(title = "BizRelNavWebsiteSysAdminDO实体类", description = "业务-导航-站点-用户关联表")
public class BizRelNavWebsiteSysAdminDO extends BaseDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "关联导航ID")
    @TableField("rel_nav_id")
    private Long relNavId;

    @Schema(description = "关联站点ID")
    @TableField("rel_website_id")
    private Long relWebsiteId;

    @Schema(description = "关联系统用户ID")
    @TableField("rel_sys_admin_id")
    private Long relSysAdminId;

}
