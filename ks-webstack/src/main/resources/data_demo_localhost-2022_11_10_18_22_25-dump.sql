-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: persagy_webstack
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `biz_nav`
--

DROP TABLE IF EXISTS `biz_nav`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `biz_nav` (
  `id` bigint NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标题名称',
  `tag_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '标签名称',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图标样式',
  `type` varchar(255) NOT NULL DEFAULT 'USER' COMMENT '类型 [INTERNAL: 内部, USER: 用户, PUBLIC: 公共]',
  `status` varchar(255) NOT NULL DEFAULT 'UNAPPROVED' COMMENT '状态 [UNAPPROVED: 待审核, APPROVED: 已审核]',
  `parent_id` bigint DEFAULT '-1' COMMENT '自关联上级导航栏ID, 第一级导航栏为 [-1]',
  `rel_create_admin_id` bigint NOT NULL COMMENT '关联创建用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='业务-导航栏表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_nav`
--

LOCK TABLES `biz_nav` WRITE;
/*!40000 ALTER TABLE `biz_nav` DISABLE KEYS */;
INSERT INTO `biz_nav` (`id`, `create_time`, `update_time`, `title`, `tag_name`, `icon`, `type`, `status`, `parent_id`, `rel_create_admin_id`) VALUES (1589826967046946818,'2022-11-08 11:47:41','2022-11-09 16:24:34','公司网站',NULL,'fa fa-building','USER','APPROVED',-1,1556564615838572545),(1589826967361519618,'2022-11-08 11:47:41','2022-11-08 11:47:41','高效办公',NULL,'fa fa-hand-o-right','USER','APPROVED',-1,1556564615838572545),(1589826967554457603,'2022-11-08 11:47:41','2022-11-08 11:47:41','运行环境',NULL,'fa fa-dashboard','USER','APPROVED',-1,1556564615838572545),(1589826967814504449,'2022-11-08 11:47:41','2022-11-08 11:47:41','界面设计',NULL,'fa fa-desktop','USER','APPROVED',-1,1556564615838572545),(1589826967877419011,'2022-11-08 11:47:41','2022-11-08 11:47:41','开发通用',NULL,'fa fa-code-fork','USER','APPROVED',-1,1556564615838572545),(1589826968070356995,'2022-11-08 11:47:41','2022-11-08 11:47:41','前端开发',NULL,'fa fa-cubes','USER','APPROVED',-1,1556564615838572545),(1589826968133271556,'2022-11-08 11:47:41','2022-11-08 11:47:41','测试部署',NULL,'fa fa-bug','USER','APPROVED',-1,1556564615838572545),(1589826968326209541,'2022-11-08 11:47:41','2022-11-08 11:47:41','云平台',NULL,'fa fa-cloud','USER','APPROVED',-1,1556564615838572545),(1589826968326209542,'2022-11-08 11:47:41','2022-11-08 11:47:41','123',NULL,'fa fa-cloud','INTERNAL','APPROVED',-1,1556564615838572545),(1589826968326209543,'2022-11-08 11:47:41','2022-11-08 11:47:41','321',NULL,'fa fa-cloud','PUBLIC','APPROVED',-1,1556564615838572545);
/*!40000 ALTER TABLE `biz_nav` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `biz_rel_nav_sys_admin`
--

DROP TABLE IF EXISTS `biz_rel_nav_sys_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `biz_rel_nav_sys_admin` (
  `id` bigint NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `order_weight` int NOT NULL DEFAULT '0' COMMENT '排序权重, 默认值为 [0]',
  `rel_nav_id` bigint NOT NULL COMMENT '关联导航ID',
  `rel_sys_admin_id` bigint NOT NULL COMMENT '关联系统用户ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `biz_rel_nav_sys_admin_rel_sys_admin_id_rel_nav_id_uindex` (`rel_sys_admin_id`,`rel_nav_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='业务-导航-用户关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_rel_nav_sys_admin`
--

LOCK TABLES `biz_rel_nav_sys_admin` WRITE;
/*!40000 ALTER TABLE `biz_rel_nav_sys_admin` DISABLE KEYS */;
INSERT INTO `biz_rel_nav_sys_admin` (`id`, `create_time`, `update_time`, `order_weight`, `rel_nav_id`, `rel_sys_admin_id`) VALUES (1589826967097278466,'2022-11-08 11:47:41','2022-11-09 16:24:34',0,1589826967046946818,1556564615838572545),(1589826967361519619,'2022-11-08 11:47:41','2022-11-08 11:47:41',1,1589826967361519618,1556564615838572545),(1589826967554457604,'2022-11-08 11:47:41','2022-11-08 11:47:41',2,1589826967554457603,1556564615838572545),(1589826967814504450,'2022-11-08 11:47:41','2022-11-08 11:47:41',3,1589826967814504449,1556564615838572545),(1589826967877419012,'2022-11-08 11:47:41','2022-11-08 11:47:41',4,1589826967877419011,1556564615838572545),(1589826968070356996,'2022-11-08 11:47:41','2022-11-08 11:47:41',5,1589826968070356995,1556564615838572545),(1589826968196186113,'2022-11-08 11:47:41','2022-11-08 11:47:41',6,1589826968133271556,1556564615838572545),(1589826968393318401,'2022-11-08 11:47:41','2022-11-08 11:47:41',7,1589826968326209541,1556564615838572545);
/*!40000 ALTER TABLE `biz_rel_nav_sys_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `biz_rel_nav_website_sys_admin`
--

DROP TABLE IF EXISTS `biz_rel_nav_website_sys_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `biz_rel_nav_website_sys_admin` (
  `id` bigint NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `rel_nav_id` bigint NOT NULL COMMENT '关联导航ID',
  `rel_website_id` bigint NOT NULL COMMENT '关联站点ID',
  `rel_sys_admin_id` bigint NOT NULL COMMENT '关联系统用户ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `biz_rel_nav_website_sys_admin_admin_id_nav_id_website_id_uindex` (`rel_sys_admin_id`,`rel_nav_id`,`rel_website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='业务-导航-站点-用户关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_rel_nav_website_sys_admin`
--

LOCK TABLES `biz_rel_nav_website_sys_admin` WRITE;
/*!40000 ALTER TABLE `biz_rel_nav_website_sys_admin` DISABLE KEYS */;
INSERT INTO `biz_rel_nav_website_sys_admin` (`id`, `create_time`, `update_time`, `rel_nav_id`, `rel_website_id`, `rel_sys_admin_id`) VALUES (1589826967164387329,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826967046946818,1589826967097278467,1556564615838572545),(1589826967227301890,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826967046946818,1589826967164387330,1556564615838572545),(1589826967227301893,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826967046946818,1589826967227301891,1556564615838572545),(1589826967294410756,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826967046946818,1589826967294410754,1556564615838572545),(1589826967424434178,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826967361519618,1589826967361519620,1556564615838572545),(1589826967487348739,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826967361519618,1589826967424434179,1556564615838572545),(1589826967554457602,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826967361519618,1589826967487348740,1556564615838572545),(1589826967621566466,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826967554457603,1589826967554457605,1556564615838572545),(1589826967684481025,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826967554457603,1589826967621566467,1556564615838572545),(1589826967684481028,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826967554457603,1589826967684481026,1556564615838572545),(1589826967747395588,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826967554457603,1589826967747395586,1556564615838572545),(1589826967877419010,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826967814504449,1589826967814504451,1556564615838572545),(1589826967944527874,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826967877419011,1589826967877419013,1556564615838572545),(1589826968007442435,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826967877419011,1589826967944527875,1556564615838572545),(1589826968070356994,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826967877419011,1589826968007442436,1556564615838572545),(1589826968133271555,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826968070356995,1589826968133271553,1556564615838572545),(1589826968263294978,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826968133271556,1589826968196186114,1556564615838572545),(1589826968263294981,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826968133271556,1589826968263294979,1556564615838572545),(1589826968326209540,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826968133271556,1589826968326209538,1556564615838572545),(1589826968393318404,'2022-11-08 11:47:41','2022-11-08 11:47:41',1589826968326209541,1589826968393318402,1556564615838572545),(1589826968456232963,'2022-11-08 11:47:42','2022-11-08 11:47:42',1589826968326209541,1589826968456232961,1556564615838572545);
/*!40000 ALTER TABLE `biz_rel_nav_website_sys_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `biz_rel_website_sys_admin`
--

DROP TABLE IF EXISTS `biz_rel_website_sys_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `biz_rel_website_sys_admin` (
  `id` bigint NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `order_weight` int NOT NULL DEFAULT '0' COMMENT '排序权重, 默认值为 [0]',
  `rel_website_id` bigint NOT NULL COMMENT '关联站点ID',
  `rel_sys_admin_id` bigint NOT NULL COMMENT '关联系统用户ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `biz_rel_website_sys_admin_rel_sys_admin_id_rel_website_id_uindex` (`rel_sys_admin_id`,`rel_website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='业务-站点-用户关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_rel_website_sys_admin`
--

LOCK TABLES `biz_rel_website_sys_admin` WRITE;
/*!40000 ALTER TABLE `biz_rel_website_sys_admin` DISABLE KEYS */;
INSERT INTO `biz_rel_website_sys_admin` (`id`, `create_time`, `update_time`, `order_weight`, `rel_website_id`, `rel_sys_admin_id`) VALUES (1589826967097278468,'2022-11-08 11:47:41','2022-11-09 16:35:58',0,1589826967097278467,1556564615838572545),(1589826967164387331,'2022-11-08 11:47:41','2022-11-09 16:37:06',1,1589826967164387330,1556564615838572545),(1589826967227301892,'2022-11-08 11:47:41','2022-11-08 18:13:25',2,1589826967227301891,1556564615838572545),(1589826967294410755,'2022-11-08 11:47:41','2022-11-09 16:37:15',3,1589826967294410754,1556564615838572545),(1589826967424434177,'2022-11-08 11:47:41','2022-11-08 11:47:41',0,1589826967361519620,1556564615838572545),(1589826967487348738,'2022-11-08 11:47:41','2022-11-08 11:47:41',1,1589826967424434179,1556564615838572545),(1589826967487348741,'2022-11-08 11:47:41','2022-11-08 11:47:41',2,1589826967487348740,1556564615838572545),(1589826967621566465,'2022-11-08 11:47:41','2022-11-08 11:47:41',0,1589826967554457605,1556564615838572545),(1589826967621566468,'2022-11-08 11:47:41','2022-11-08 11:47:41',1,1589826967621566467,1556564615838572545),(1589826967684481027,'2022-11-08 11:47:41','2022-11-08 11:47:41',2,1589826967684481026,1556564615838572545),(1589826967747395587,'2022-11-08 11:47:41','2022-11-08 11:47:41',3,1589826967747395586,1556564615838572545),(1589826967814504452,'2022-11-08 11:47:41','2022-11-08 11:47:41',0,1589826967814504451,1556564615838572545),(1589826967944527873,'2022-11-08 11:47:41','2022-11-08 11:47:41',0,1589826967877419013,1556564615838572545),(1589826968007442434,'2022-11-08 11:47:41','2022-11-08 11:47:41',1,1589826967944527875,1556564615838572545),(1589826968007442437,'2022-11-08 11:47:41','2022-11-08 11:47:41',2,1589826968007442436,1556564615838572545),(1589826968133271554,'2022-11-08 11:47:41','2022-11-08 11:47:41',0,1589826968133271553,1556564615838572545),(1589826968196186115,'2022-11-08 11:47:41','2022-11-08 11:47:41',0,1589826968196186114,1556564615838572545),(1589826968263294980,'2022-11-08 11:47:41','2022-11-08 11:47:41',1,1589826968263294979,1556564615838572545),(1589826968326209539,'2022-11-08 11:47:41','2022-11-08 11:47:41',2,1589826968326209538,1556564615838572545),(1589826968393318403,'2022-11-08 11:47:41','2022-11-08 11:47:41',0,1589826968393318402,1556564615838572545),(1589826968456232962,'2022-11-08 11:47:41','2022-11-08 11:47:41',1,1589826968456232961,1556564615838572545);
/*!40000 ALTER TABLE `biz_rel_website_sys_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `biz_website`
--

DROP TABLE IF EXISTS `biz_website`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `biz_website` (
  `id` bigint NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标题名称',
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '缩略图',
  `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '跳转链接',
  `tag_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '标签名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '描述信息',
  `type` varchar(255) NOT NULL DEFAULT 'USER' COMMENT '类型 [INTERNAL: 内部, USER: 用户, PUBLIC: 公共]',
  `status` varchar(255) NOT NULL DEFAULT 'UNAPPROVED' COMMENT '状态 [UNAPPROVED: 待审核, APPROVED: 已审核]',
  `rel_create_admin_id` bigint NOT NULL COMMENT '关联创建用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='业务-网站表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `biz_website`
--

LOCK TABLES `biz_website` WRITE;
/*!40000 ALTER TABLE `biz_website` DISABLE KEYS */;
INSERT INTO `biz_website` (`id`, `create_time`, `update_time`, `title`, `image`, `link`, `tag_name`, `description`, `type`, `status`, `rel_create_admin_id`) VALUES (1589826967097278467,'2022-11-08 11:47:41','2022-11-09 16:35:58','公司官网','persagy','http://www.persagy.com/',NULL,'公司官网地址','USER','APPROVED',1556564615838572545),(1589826967164387330,'2022-11-08 11:47:41','2022-11-09 16:37:06','公司邮箱','网易','http://mail.persagy.com/',NULL,'网易企业邮箱','USER','APPROVED',1556564615838572545),(1589826967227301891,'2022-11-08 11:47:41','2022-11-08 11:47:41','泛微系统','泛微系统','http://glxt.persagy.com:8080/',NULL,'公司OA系统','USER','APPROVED',1556564615838572545),(1589826967294410754,'2022-11-08 11:47:41','2022-11-09 16:37:15','智思云','智思云','https://www.zhisiyun.com/gisiweb/',NULL,'智思云','USER','APPROVED',1556564615838572545),(1589826967361519620,'2022-11-08 11:47:41','2022-11-08 11:47:41','Coding','coding','https://persagy2021.coding.net/p/ziguanxitongchanpinzu/',NULL,'一站式软件研发管理平台','USER','APPROVED',1556564615838572545),(1589826967424434179,'2022-11-08 11:47:41','2022-11-08 11:47:41','Thoughts','Thoughts','https://thoughts.teambition.com/',NULL,'办公协作文档','USER','APPROVED',1556564615838572545),(1589826967487348740,'2022-11-08 11:47:41','2022-11-08 11:47:41','数智化运营管理平台v4.5-830-1030项目看板(v3)','dingding','https://qr.dingtalk.com/page/yunpan?route=previewDentry&spaceId=5857347608&fileId=59099785981&type=file',NULL,'点击我快速打开文件 (需要有对应权限)','USER','APPROVED',1556564615838572545),(1589826967554457605,'2022-11-08 11:47:41','2022-11-08 11:47:41','开发环境','persagy','http://120.53.133.61/','内网','开发环境地址','USER','APPROVED',1556564615838572545),(1589826967621566467,'2022-11-08 11:47:41','2022-11-08 11:47:41','开发环境-运维管理平台','persagy','http://120.53.133.61/fbms/','内网','开发环境-运维管理平台地址','USER','APPROVED',1556564615838572545),(1589826967684481026,'2022-11-08 11:47:41','2022-11-08 11:47:41','演示环境','persagy','http://doimp.persagy.com/saasFrame/',NULL,'演示环境地址','USER','APPROVED',1556564615838572545),(1589826967747395586,'2022-11-08 11:47:41','2022-11-08 11:47:41','演示环境-运维管理平台','persagy','http://doimp.persagy.com/fbms/',NULL,'演示环境-运维管理平台地址','USER','APPROVED',1556564615838572545),(1589826967814504451,'2022-11-08 11:47:41','2022-11-08 11:47:41','Figma','figma','https://www.figma.com/',NULL,'免费在线设计协作工具','USER','APPROVED',1556564615838572545),(1589826967877419013,'2022-11-08 11:47:41','2022-11-08 11:47:41','Apifox','Apifox','https://www.apifox.cn/web/main/',NULL,'接口文档管理','USER','APPROVED',1556564615838572545),(1589826967944527875,'2022-11-08 11:47:41','2022-11-08 11:47:41','GitLab','Gitlab','http://192.168.100.214:5580/dashboard/projects/','内网','GitLab代码仓库','USER','APPROVED',1556564615838572545),(1589826968007442436,'2022-11-08 11:47:41','2022-11-08 11:47:41','BDTP-数据服务','building','http://82.157.26.241:9101/','内网','Building Digital Twin Platform','USER','APPROVED',1556564615838572545),(1589826968133271553,'2022-11-08 11:47:41','2022-11-08 11:47:41','Meri Design System','meri-design-system','http://www.persagy.com/meri/',NULL,'「梅里雪山」设计系统','USER','APPROVED',1556564615838572545),(1589826968196186114,'2022-11-08 11:47:41','2022-11-08 11:47:41','BUG管理系统','bug','http://39.106.182.122:3000/projects','内网','BUG管理系统','USER','APPROVED',1556564615838572545),(1589826968263294979,'2022-11-08 11:47:41','2022-11-08 11:47:41','Jenkins','jenkins','http://192.168.100.95/blue/organizations/jenkins/common-develop-CICD/activity/','内网','Jenkins持续集成平台','USER','APPROVED',1556564615838572545),(1589826968326209538,'2022-11-08 11:47:41','2022-11-08 11:47:41','K8S','K8S','https://192.168.100.223/p/c-fd9bh:p-mpfbf/workloads/','内网','K8S容器部署','USER','APPROVED',1556564615838572545),(1589826968393318402,'2022-11-08 11:47:41','2022-11-08 11:47:41','阿里云','aliyun','https://signin.aliyun.com/login.htm',NULL,'阿里云平台','USER','APPROVED',1556564615838572545),(1589826968456232961,'2022-11-08 11:47:41','2022-11-08 11:47:41','腾讯云','腾讯云','https://cloud.tencent.com/login',NULL,'腾讯云平台','USER','APPROVED',1556564615838572545),(1589826968456232962,'2022-11-08 11:47:41','2022-11-08 11:47:41','123','33','https://cloud.tencent.com/login',NULL,'腾讯云平台','INTERNAL','APPROVED',1556564615838572545);
/*!40000 ALTER TABLE `biz_website` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_admin`
--

DROP TABLE IF EXISTS `sys_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_admin` (
  `id` bigint NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `account` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户帐号',
  `real_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '真实姓名',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '手机号码',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '邮箱地址',
  `status` smallint NOT NULL DEFAULT '1' COMMENT '用户状态 [0: 禁用, 1: 启用]',
  `web_code` varchar(255) DEFAULT NULL COMMENT '页面授权码',
  `is_internal` tinyint NOT NULL DEFAULT '0' COMMENT '是否为内置 [0: 否, 1: 是]',
  `rel_department_id` bigint NOT NULL COMMENT '关联部门ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统-用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_admin`
--

LOCK TABLES `sys_admin` WRITE;
/*!40000 ALTER TABLE `sys_admin` DISABLE KEYS */;
INSERT INTO `sys_admin` (`id`, `create_time`, `update_time`, `account`, `real_name`, `phone`, `email`, `status`, `web_code`, `is_internal`, `rel_department_id`) VALUES (1556564615838572545,'2022-08-08 16:54:59','2022-08-08 16:54:59','admin','Krismile','15181743604','jyc@krismile.cn',1,'123456',0,-1);
/*!40000 ALTER TABLE `sys_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_department`
--

DROP TABLE IF EXISTS `sys_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_department` (
  `id` bigint NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '部门名称',
  `parent_id` bigint NOT NULL DEFAULT '0' COMMENT '自关联上级部门ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统-部门表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_department`
--

LOCK TABLES `sys_department` WRITE;
/*!40000 ALTER TABLE `sys_department` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_menu`
--

DROP TABLE IF EXISTS `sys_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_menu` (
  `id` bigint NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单名称',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '路由地址',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '图标样式',
  `isMulti` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为多级菜单 [0: 否, 1: 是]',
  `parent_id` bigint NOT NULL DEFAULT '0' COMMENT '自关联父级菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统-菜单表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_menu`
--

LOCK TABLES `sys_menu` WRITE;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_permission`
--

DROP TABLE IF EXISTS `sys_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_permission` (
  `id` bigint NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '权限名称',
  `mark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '权限标识',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '权限描述',
  `rel_menu_id` bigint NOT NULL COMMENT '关联菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统-权限表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_permission`
--

LOCK TABLES `sys_permission` WRITE;
/*!40000 ALTER TABLE `sys_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_rel_role_menu`
--

DROP TABLE IF EXISTS `sys_rel_role_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_rel_role_menu` (
  `id` bigint NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL COMMENT '描述信息',
  `rel_role_id` bigint NOT NULL COMMENT '关联角色ID',
  `rel_menu_id` bigint DEFAULT NULL COMMENT '关联菜单ID',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `sys_rel_role_menu_rel_role_id_rel_menu_id_uindex` (`rel_role_id`,`rel_menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统-角色-菜单关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_rel_role_menu`
--

LOCK TABLES `sys_rel_role_menu` WRITE;
/*!40000 ALTER TABLE `sys_rel_role_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_rel_role_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_rel_role_permission`
--

DROP TABLE IF EXISTS `sys_rel_role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_rel_role_permission` (
  `id` bigint NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL COMMENT '描述信息',
  `rel_role_id` bigint NOT NULL COMMENT '关联角色ID',
  `rel_permission_id` bigint NOT NULL COMMENT '关联权限ID',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `sys_rel_role_permission_rel_role_id_rel_permission_id_uindex` (`rel_role_id`,`rel_permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统-角色-权限关联表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_rel_role_permission`
--

LOCK TABLES `sys_rel_role_permission` WRITE;
/*!40000 ALTER TABLE `sys_rel_role_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_rel_role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role` (
  `id` bigint NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名称',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '角色描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='系统-角色表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-10 18:22:25
