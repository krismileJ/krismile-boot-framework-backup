package cn.krismile.boot.framework.context;

/**
 * 执行顺序
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public class ExecuteOrder {


    /**
     * AOP执行顺序
     *
     * @author JiYinchuan
     * @since 1.0.0
     */
    public static class Aop {

        /**
         * 请求日志记录执行顺序
         */
        public static final int REQUEST_LOG = 100;

    }

    /**
     * 过滤器执行顺序
     *
     * <p><p><b>Warning:</b> 如果过滤器包装了servlet则必须大于或小于 {@code OrderedFilter.REQUEST_WRAPPER_FILTER_MAX_ORDER},
     * <a href="https://docs.spring.io/spring-boot/docs/2.6.7/reference/htmlsingle/#web.servlet.embedded-container.servlets-filters-listeners.beans">点击查看官方文档说明</a></p>
     *
     * @author JiYinchuan
     * @since 1.0.0
     */
    public static class Filter {

        /**
         * 请求体包装执行顺序
         */
        public static final int REQUEST_BODY_WRAPPER = -10000;

        /**
         * XSS攻击防御包装器执行顺序
         */
        public static final int XSS_WRAPPER = -9000;

    }
}
