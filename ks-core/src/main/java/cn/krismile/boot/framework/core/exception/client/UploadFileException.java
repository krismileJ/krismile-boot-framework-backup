package cn.krismile.boot.framework.core.exception.client;


import cn.krismile.boot.framework.core.enumeration.error.client.UploadFileErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 用户上传文件自定义异常
 *
 * @author JiYinchuan
 * @see UploadFileErrorEnum
 * @since 1.0.0
 */
public class UploadFileException extends ApplicationException {

    private static final long serialVersionUID = -6495930469682939645L;

    public UploadFileException(UploadFileErrorEnum uploadFileErrorEnum) {
        super(uploadFileErrorEnum);
    }

    public UploadFileException(UploadFileErrorEnum uploadFileErrorEnum, String userTip) {
        super(uploadFileErrorEnum, userTip);
    }
}
