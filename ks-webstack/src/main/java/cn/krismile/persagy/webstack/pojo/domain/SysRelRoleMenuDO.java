package cn.krismile.persagy.webstack.pojo.domain;

import cn.krismile.boot.framework.mybatisplus.domain.BaseDO;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 系统-角色-菜单关联表
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_rel_role_menu")
@Schema(title = "SysRelRoleMenuDO对象", description = "系统-角色-菜单关联表")
public class SysRelRoleMenuDO extends BaseDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "关联角色ID")
    @TableField("rel_role_id")
    private Long relRoleId;

    @Schema(description = "关联菜单ID")
    @TableField("rel_menu_id")
    private Long relMenuId;

    @Schema(description = "描述信息")
    @TableField("description")
    private String description;

}
