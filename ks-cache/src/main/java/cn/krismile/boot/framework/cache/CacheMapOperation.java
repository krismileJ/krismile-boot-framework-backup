package cn.krismile.boot.framework.cache;

import cn.krismile.boot.framework.core.annotation.jsr.Nullable;

import java.util.Map;

/**
 * 键值对缓存操作
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface CacheMapOperation {

    /**
     * 获取值
     *
     * @param key  键
     * @param item 项
     * @param <V>  值类型
     * @return 值
     * @since 1.1.0.211021
     */
    @Nullable
    <V> V get(String key, String item);

    /**
     * 获取项与值
     *
     * @param key 键
     * @return 对应的多个键值
     * @since 1.1.0.211021
     */
    @Nullable
    Map<Object, Object> get(String key);

    /**
     * 设置值
     *
     * @param key 键
     * @param map 对应多个键值
     * @since 1.1.0.211021
     */
    void set(String key, Map<String, Object> map);

    /**
     * 设置值并指定过期时间
     *
     * @param key        键
     * @param map        对应多个键与项
     * @param expireTime 过期时间, 单位 [秒], 小于等于 [0] 将永不过期
     * @since 1.1.0.211021
     */
    void set(String key, Map<String, Object> map, long expireTime);

    /**
     * 设置值
     *
     * @param key   键
     * @param item  项
     * @param value 值
     * @since 1.1.0.211021
     */
    void set(String key, String item, Object value);

    /**
     * 设置值并指定过期时间
     *
     * @param key        键
     * @param item       项
     * @param value      值
     * @param expireTime 过期时间, 单位 [秒]
     * @since 1.1.0.211021
     */
    void set(String key, String item, Object value, long expireTime);

    /**
     * 删除项与值
     *
     * @param key  键
     * @param item 项
     * @since 1.1.0.211021
     */
    void del(String key, Object... item);

    /**
     * 判断项是否存在
     *
     * @param key  键
     * @param item 项
     * @return 是否存在 [true: 存在, false: 不存在]
     * @since 1.1.0.211021
     */
    boolean hasItem(String key, String item);

    /**
     * 执行项的值进行递增
     *
     * @param key    键
     * @param item   项
     * @param factor 递增因子, 必须大于 [0]
     * @return 递增后的值
     * @since 1.1.0.211021
     */
    double increment(String key, String item, double factor);

    /**
     * 执行项的值进行递减
     *
     * @param key    键
     * @param item   项
     * @param factor 递减因子, 必须大于 [0]
     * @return 递减后的值
     * @since 1.1.0.211021
     */
    double decrement(String key, String item, double factor);

}
