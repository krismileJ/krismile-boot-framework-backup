package cn.krismile.boot.framework.core.enumeration.error.third;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import cn.krismile.boot.framework.core.enumeration.error.client.RequestParamErrorEnum;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 第三方系统执行超时异常枚举
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public enum ThirdExecuteTimeoutErrorEnum implements BaseEnum<String> {

    /**
     * 第三方系统执行超时
     */
    THIRD_EXECUTE_TIMEOUT("C0200", "第三方系统执行超时"),

    /**
     * RPC 执行超时
     */
    RPC_EXECUTE_TIMEOUT("C0210", "RPC 执行超时"),

    /**
     * 消息投递超时
     */
    MESSAGE_SEND_TIMEOUT("C0220", "消息投递超时"),

    /**
     * 缓存服务超时
     */
    CACHE_SERVICE_TIMEOUT("C0230", "缓存服务超时"),

    /**
     * 配置服务超时
     */
    CONFIG_SERVICE_TIMEOUT("C0240", "配置服务超时"),

    /**
     * 数据库服务超时
     */
    DATABASE_SERVICE_TIMEOUT("C0250", "数据库服务超时");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    ThirdExecuteTimeoutErrorEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static RequestParamErrorEnum analyze(String value) {
        return BaseEnum.analyze(value, RequestParamErrorEnum.class);
    }
}
