package cn.krismile.persagy.webstack.function.business.service.impl;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import cn.krismile.boot.framework.core.enumeration.common.OperationTypeEnum;
import cn.krismile.boot.framework.core.enumeration.error.client.AuthorityErrorEnum;
import cn.krismile.boot.framework.core.enumeration.error.client.RequestParamErrorEnum;
import cn.krismile.boot.framework.core.exception.EnumIllegalArgumentException;
import cn.krismile.boot.framework.core.exception.client.AuthorityException;
import cn.krismile.boot.framework.core.exception.client.RequestParamException;
import cn.krismile.boot.framework.core.exception.client.RequestServerException;
import cn.krismile.boot.framework.core.util.Assert;
import cn.krismile.boot.framework.mybatisplus.exception.MybatisServiceException;
import cn.krismile.boot.framework.mybatisplus.service.BaseServiceImpl;
import cn.krismile.persagy.webstack.convert.NavConvert;
import cn.krismile.persagy.webstack.enumeration.business.ResourceStatusEnum;
import cn.krismile.persagy.webstack.enumeration.business.ResourceTypeEnum;
import cn.krismile.persagy.webstack.function.business.mapper.BizNavMapper;
import cn.krismile.persagy.webstack.function.business.service.BizNavService;
import cn.krismile.persagy.webstack.function.business.service.BizRelNavSysAdminService;
import cn.krismile.persagy.webstack.function.system.service.SysAdminService;
import cn.krismile.persagy.webstack.pojo.context.UserInfoContext;
import cn.krismile.persagy.webstack.pojo.domain.BizNavDO;
import cn.krismile.persagy.webstack.pojo.domain.SysAdminDO;
import cn.krismile.persagy.webstack.pojo.dto.NavDTO;
import cn.krismile.persagy.webstack.validate.ValidateGroup;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.validation.Validator;
import java.util.List;
import java.util.Objects;

/**
 * 业务-导航栏表 ServiceImpl
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Service
@Transactional(readOnly = true)
public class BizNavServiceImpl extends BaseServiceImpl<BizNavMapper, BizNavDO> implements BizNavService {

    @Resource
    private Validator validator;
    @Resource
    private SysAdminService adminService;
    @Resource
    private BizRelNavSysAdminService relNavSysAdminService;

    @Override
    public List<NavDTO> listByUser() throws RequestServerException {
        return NavConvert.INSTANCE.toDto(getBaseMapper().listByAdmin(UserInfoContext.userId()));
    }

    @Override
    public List<NavDTO> listByInternal() {
        return NavConvert.INSTANCE.toDto(getBaseMapper()
                .list(UserInfoContext.userId(), ResourceTypeEnum.INTERNAL, ResourceStatusEnum.APPROVED));
    }

    @Override
    public List<NavDTO> listByPublic() {
        return NavConvert.INSTANCE.toDto(getBaseMapper()
                .list(UserInfoContext.userId(), ResourceTypeEnum.PUBLIC, ResourceStatusEnum.APPROVED));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean add(@NonNull NavDTO dto) throws MybatisServiceException, RequestServerException, AuthorityException {
        validator.validate(dto, ValidateGroup.Insert.class);

        // 模版库创建时直接关联即可
        if (Objects.nonNull(dto.getTemplateId())) {
            checkOperation(relNavSysAdminService.relation(dto.getTemplateId(), dto.getNavOrderWeight()));
            return true;
        }
        checkNav(dto.getNavParentId());
        // 检查操作权限
        checkPermission(OperationTypeEnum.EDIT, null, dto.getNavType());

        BizNavDO addNav = NavConvert.INSTANCE.toDo(dto);
        // 解析导航栏类型并设置导航栏状态
        addNav.setStatus(analyzeStatus(dto.getNavType()));
        addNav.setRelCreateAdminId(UserInfoContext.userId());
        checkOperation(saveValidate(addNav));

        // 新增导航栏用户关联
        return relNavSysAdminService.relation(addNav.getId(), dto.getNavOrderWeight());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean edit(@NonNull NavDTO dto) throws MybatisServiceException, RequestServerException, AuthorityException {
        validator.validate(dto, ValidateGroup.Update.class);

        checkNav(dto.getNavParentId());

        BizNavDO editNav = getBaseMapper().get(true, dto.getNavId());
        // 检查操作权限
        checkPermission(OperationTypeEnum.EDIT, editNav.getId(), editNav.getType());
        // 当修改了排序权重时进行更新排序权重
        if (!Objects.equals(dto.getNavOrderWeight(), editNav.getOrderWeight())) {
            checkOperation(relNavSysAdminService.editSort(editNav.getId(), dto.getNavOrderWeight()));
        }
        editNav = NavConvert.INSTANCE.toDo(dto, editNav);
        // 解析导航栏类型并设置导航栏状态
        editNav.setStatus(analyzeStatus(dto.getNavType()));
        return updateByIdValidate(editNav);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean del(@Nullable List<Long> ids) throws RequestServerException, AuthorityException {
        if (CollectionUtils.isNotEmpty(ids)) {
            for (Long id : ids) {
                BizNavDO delData = getBaseMapper().get(true, id);
                // 如果类型为用户时同时删除该导航栏
                if (ResourceTypeEnum.USER == delData.getType()) {
                    checkPermission(OperationTypeEnum.DEL, delData.getId(), delData.getType());
                    checkOperation(removeByIdValidate(delData.getId()));
                }
                // 删除关联
                relNavSysAdminService.del(delData.getId());
            }
        }
        return true;
    }

    /**
     * 校验导航栏
     *
     * @param id 导航栏ID, 当 {@code id} 为 {@code -1} 时代表第一级导航栏
     * @throws RequestParamException 上级导航栏ID不存在
     * @since 1.0.0
     */
    private void checkNav(@NonNull Long id) throws RequestParamException {
        if (id != -1 && isExistsByIdValidate(id)) {
            throw new RequestParamException(RequestParamErrorEnum.INVALID_INPUT, "上级导航栏ID不存在");
        }
    }

    /**
     * 检查导航栏操作权限
     *
     * @param operationType 操作类型
     * @param navId         导航栏ID, 当网站类型为 {@link ResourceTypeEnum#PUBLIC} 时不能为空
     * @param navType       导航栏类型, 为 {@code null} 时不进行检查
     * @throws RequestServerException       用户ID获取失败
     * @throws AuthorityException           非网站拥有者无法进行操作
     * @throws EnumIllegalArgumentException 未知的枚举类型
     * @since 1.0.0
     */
    private void checkPermission(@NonNull OperationTypeEnum operationType,
                                 @Nullable Long navId, @Nullable ResourceTypeEnum navType)
            throws RequestServerException, AuthorityException, EnumIllegalArgumentException {
        Assert.notNull(operationType, "OperationType can not be null");
        if (Objects.nonNull(navType)) {
            switch (navType) {
                case INTERNAL:
                    if (operationType.isMatch(OperationTypeEnum.ADD, OperationTypeEnum.EDIT, OperationTypeEnum.DEL)) {
                        adminService.checkIsInternal(null, () -> true);
                    }
                    break;
                case PUBLIC:
                    if (operationType.isMatch(OperationTypeEnum.EDIT, OperationTypeEnum.DEL)) {
                        Assert.notNull(navId, "NavType can not be null");
                        BizNavDO dbNav = getByIdValidate(navId);
                        if (!Objects.equals(dbNav.getRelCreateAdminId(), UserInfoContext.userId())) {
                            throw new AuthorityException(AuthorityErrorEnum.ACCESS_BLOCKED, "非导航栏拥有者无法进行操作");
                        }
                    }
                    break;
                case USER:
                    break;
                default:
                    throw BaseEnum.DEFAULT_ANALYZE_EXCEPTION.apply(navType.getClass().getName(), navType);
            }
        }
    }

    /**
     * 解析导航栏状态
     *
     * @param navType 导航栏类型
     * @return 导航栏状态
     * @since 1.0.0
     */
    @SuppressWarnings("DuplicatedCode")
    private ResourceStatusEnum analyzeStatus(@Nullable ResourceTypeEnum navType) {
        if (Objects.isNull(navType)) {
            return null;
        }
        SysAdminDO cacheAdmin = adminService.getCacheById(UserInfoContext.userId());
        // 用户类型内置用户状态默认通过
        if (ResourceTypeEnum.USER == navType || BooleanUtils.isTrue(cacheAdmin.getInternal())) {
            return ResourceStatusEnum.APPROVED;
        } else {
            // 其他类型默认置为待审核
            return ResourceStatusEnum.UNAPPROVED;
        }
    }
}
