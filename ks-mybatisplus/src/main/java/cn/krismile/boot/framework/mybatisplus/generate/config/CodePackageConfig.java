package cn.krismile.boot.framework.mybatisplus.generate.config;

import lombok.Data;

/**
 * MyBatisPlus包配置
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
public class CodePackageConfig {

    /**
     * 输出包名
     */
    private String outputPackage;

    /**
     * 父包模块名
     */
    private String parentModelName;

    /**
     * 实体类包名
     */
    private String entityPackageName = "pojo.domain";

    /**
     * 数据访问层包名
     */
    private String mapperPackageName = "mapper";

    /**
     * 数据访问层XML包名
     */
    private String mapperXmlPackageName = "mapper/xml";

    /**
     * 业务逻辑层包名
     */
    private String servicePackageName = "service";

    /**
     * 业务逻辑实现层包名
     */
    private String serviceImplPackageName = "service.impl";

    /**
     * 界面层包名
     */
    private String controllerPackageName = "controller";

    /**
     * 输出根目录绝对路径, 默认当前项目
     */
    private String outputDir = System.getProperty("user.dir").concat("/src/main/java/");

    /**
     * 输出根目录资源绝对路径, 默认当前项目
     */
    private String outputResourcesDir = System.getProperty("user.dir").concat("/src/main/resources/");

}
