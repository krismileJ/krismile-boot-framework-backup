package cn.krismile.persagy.webstack.function.business.mapper;

import cn.krismile.persagy.webstack.pojo.domain.BizRelNavWebsiteSysAdminDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 业务-导航-站点-用户关联表 Mapper 接口
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Mapper
@Repository
public interface BizRelNavWebsiteSysAdminMapper extends BaseMapper<BizRelNavWebsiteSysAdminDO> {

}
