package cn.krismile.persagy.webstack.function.business.service.impl;

import cn.krismile.boot.framework.core.enumeration.error.client.RequestParamErrorEnum;
import cn.krismile.boot.framework.core.exception.client.RequestParamException;
import cn.krismile.boot.framework.core.exception.client.RequestServerException;
import cn.krismile.boot.framework.core.util.Assert;
import cn.krismile.boot.framework.mybatisplus.exception.MybatisServiceException;
import cn.krismile.boot.framework.mybatisplus.service.BaseServiceImpl;
import cn.krismile.persagy.webstack.function.business.mapper.BizRelNavSysAdminMapper;
import cn.krismile.persagy.webstack.function.business.service.BizNavService;
import cn.krismile.persagy.webstack.function.business.service.BizRelNavSysAdminService;
import cn.krismile.persagy.webstack.pojo.context.UserInfoContext;
import cn.krismile.persagy.webstack.pojo.domain.BizRelNavSysAdminDO;
import cn.krismile.persagy.webstack.pojo.dto.WebsiteSortDTO;
import org.springframework.context.annotation.Lazy;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * 业务-导航-用户关联表 ServiceImpl
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Service
public class BizRelNavSysAdminServiceImpl extends BaseServiceImpl<BizRelNavSysAdminMapper, BizRelNavSysAdminDO> implements BizRelNavSysAdminService {

    @Resource
    @Lazy
    private BizNavService navService;

    @Override
    public boolean checkRelation(@NonNull Long navId) throws MybatisServiceException, RequestServerException {
        checkNav(false, navId);
        return getBaseMapper().count(navId, UserInfoContext.userId()) > 0;
    }

    @Override
    public boolean relation(@NonNull Long navId, @NonNull Integer orderWeight)
            throws MybatisServiceException, RequestServerException {
        checkNav(false, navId);
        checkIdempotency(getBaseMapper().count(navId, UserInfoContext.userId()));
        Assert.notNull(orderWeight, "OrderWeight can not be null");

        BizRelNavSysAdminDO addData = new BizRelNavSysAdminDO();
        addData.setOrderWeight(orderWeight);
        addData.setRelNavId(navId);
        addData.setRelSysAdminId(UserInfoContext.userId());
        return saveValidate(addData);
    }

    @Override
    public boolean editSort(@NonNull Long navId, @NonNull Integer orderWeight)
            throws MybatisServiceException, RequestParamException, RequestServerException {
        checkNav(true, navId);
        Assert.notNull(orderWeight, "OrderWeight can not be null");

        BizRelNavSysAdminDO editData = checkExistsAndGet(getBaseMapper()
                .get(true, navId, UserInfoContext.userId()), DEFAULT_NOT_FOUND_MESSAGE);
        if (Objects.equals(editData.getOrderWeight(), orderWeight)) {
            return true;
        }
        editData.setOrderWeight(orderWeight);
        return updateByIdValidate(editData);
    }

    @Override
    public boolean sortExchange(@NonNull WebsiteSortDTO dto)
            throws MybatisServiceException, RequestParamException, RequestServerException {
        // TODO
        return false;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean del(@NonNull Long navId)
            throws MybatisServiceException, RequestParamException, RequestServerException {
        Assert.notNull(navId, "NavId can not be null");
        checkNav(true, navId);
        return removeById(getBaseMapper().get(true, navId, UserInfoContext.userId()));
    }

    /**
     * 检查导航栏
     *
     * @param isCheckRelation 是否检查关联
     * @param navId           导航栏ID
     * @throws MybatisServiceException 导航栏不存在
     * @throws RequestParamException   未找到用户关联的导航栏
     * @throws RequestServerException  用户ID获取失败
     * @since 1.0.0
     */
    private void checkNav(boolean isCheckRelation, @NonNull Long navId)
            throws MybatisServiceException, RequestParamException, RequestServerException {
        // 检查用户是否关联该导航栏
        if (isCheckRelation && !checkRelation(navId)) {
            throw new RequestParamException(RequestParamErrorEnum.INVALID_INPUT, DEFAULT_NOT_FOUND_MESSAGE);
        }
        // 检查导航栏是否存在
        checkExists(navService.isExistsByIdValidate(navId), BizNavService.DEFAULT_NOT_FOUND_MESSAGE);
    }
}
