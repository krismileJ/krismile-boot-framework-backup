package cn.krismile.boot.framework.core.enumeration.common;

import com.fasterxml.jackson.annotation.JsonCreator;
import cn.krismile.boot.framework.core.enumeration.BaseEnum;

/**
 * 性别枚举
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public enum SexEnum implements BaseEnum<String> {

    /**
     * 未知
     */
    UNKNOWN("UNKNOWN", "UNKNOWN"),

    /**
     * 男
     */
    MAN("MAN", "男"),

    /**
     * 女
     */
    WOMAN("WOMAN", "女");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    SexEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static SexEnum analyze(String value) {
        return BaseEnum.analyze(value, SexEnum.class);
    }
}
