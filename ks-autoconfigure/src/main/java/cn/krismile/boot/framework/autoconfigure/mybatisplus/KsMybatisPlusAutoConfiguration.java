package cn.krismile.boot.framework.autoconfigure.mybatisplus;

import cn.krismile.boot.framework.autoconfigure.mybatisplus.filter.DruidAdvertFilter;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.alibaba.druid.spring.boot.autoconfigure.properties.DruidStatProperties;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MybatisPlus自动配置
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Configuration(proxyBeanMethods = false)
public class KsMybatisPlusAutoConfiguration {

    /**
     * Druid广告拦截配置
     *
     * <p>该过滤器主要用于去掉 [druid] 网页中的底部广告内容
     *
     * @author JiYinchuan
     * @see DruidAdvertFilter
     * @since 1.0.0
     */
    @Configuration(proxyBeanMethods = false)
    @ConditionalOnClass(DruidDataSourceAutoConfigure.class)
    @AutoConfigureAfter(DruidDataSourceAutoConfigure.class)
    @ConditionalOnProperty(name = "spring.datasource.druid.stat-view-servlet.enabled", havingValue = "true", matchIfMissing = true)
    static class DruidAdvertConfiguration {

        @Bean
        public FilterRegistrationBean<DruidAdvertFilter> removeDruidAdvert(DruidStatProperties properties) {
            // 获取Web监控页面参数
            DruidStatProperties.StatViewServlet config = properties.getStatViewServlet();
            // 提取common.js配置路径
            String pattern = StringUtils.isNotBlank(config.getUrlPattern()) ? config.getUrlPattern() : "/druid/*";
            String commonJsPattern = pattern.replaceAll("\\*", "js/common.js");

            FilterRegistrationBean<DruidAdvertFilter> registrationBean = new FilterRegistrationBean<>();
            registrationBean.setFilter(new DruidAdvertFilter());
            registrationBean.addUrlPatterns(commonJsPattern);
            return registrationBean;
        }
    }
}
