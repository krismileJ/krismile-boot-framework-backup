package cn.krismile.boot.framework.core.exception.client;

import cn.krismile.boot.framework.core.enumeration.error.client.PrivacyErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 用户隐私未授权自定义异常
 *
 * @author JiYinchuan
 * @see PrivacyErrorEnum
 * @since 1.0.0
 */
public class PrivacyException extends ApplicationException {

    private static final long serialVersionUID = -3027009957856883785L;

    public PrivacyException(PrivacyErrorEnum privacyErrorEnum) {
        super(privacyErrorEnum);
    }

    public PrivacyException(PrivacyErrorEnum privacyErrorEnum, String userTip) {
        super(privacyErrorEnum, userTip);
    }
}
