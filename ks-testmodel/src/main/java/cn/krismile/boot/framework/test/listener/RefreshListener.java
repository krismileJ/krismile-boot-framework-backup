package cn.krismile.boot.framework.test.listener;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

/**
 * TODO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Component
public class RefreshListener implements ApplicationListener<ApplicationReadyEvent> {
    @Override
    public void onApplicationEvent(@NonNull ApplicationReadyEvent event) {
        System.out.println(event);
    }
}
