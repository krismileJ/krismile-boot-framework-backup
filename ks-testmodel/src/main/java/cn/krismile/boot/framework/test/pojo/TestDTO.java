package cn.krismile.boot.framework.test.pojo;

import cn.krismile.boot.framework.test.pojo.enumeration.TestEnum;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * TODO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
public class TestDTO {

    private String myId;

    private String myName;

    private LocalDateTime localDateTime;

    private TestEnum testEnum;

}
