package cn.krismile.persagy.webstack.function.system.service;

import cn.krismile.persagy.webstack.pojo.domain.SysRoleDO;
import cn.krismile.boot.framework.mybatisplus.service.BaseService;

/**
 * 系统-角色表 Service
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface SysRoleService extends BaseService<SysRoleDO> {

}
