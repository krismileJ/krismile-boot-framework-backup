package cn.krismile.persagy.webstack.enumeration.business;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import com.baomidou.mybatisplus.annotation.IEnum;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 资源状态枚举
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public enum ResourceStatusEnum implements BaseEnum<String>, IEnum<String> {

    /**
     * 待审核
     */
    UNAPPROVED("UNAPPROVED", "待审核"),

    /**
     * 已审核
     */
    APPROVED("APPROVED", "已审核");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    ResourceStatusEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static ResourceStatusEnum analyze(String value) {
        return BaseEnum.analyze(value, ResourceStatusEnum.class);
    }
}
