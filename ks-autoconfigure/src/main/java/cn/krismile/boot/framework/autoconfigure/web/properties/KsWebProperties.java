package cn.krismile.boot.framework.autoconfigure.web.properties;

import cn.krismile.boot.framework.autoconfigure.KrismileProperties;
import cn.krismile.boot.framework.autoconfigure.web.properties.children.KsJacksonProperties;
import cn.krismile.boot.framework.autoconfigure.web.properties.children.KsResponseProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * 上下文配置文件
 *
 * <pre>
 *     # ------------------------- KrismileConfig -------------------------
 *     krismile:
 *       web:
 *         # 请求响应配置
 *         response:
 *           # 默认成功用户提示信息
 *           default-success-user-tip: OK
 *           # 默认异常用户提示信息
 *           default-error-user-tip: 网络开小差了, 请稍后再试 (╯﹏╰)
 *         # Jackson配置
 *         jackson:
 *           # 是否开启配置
 *           enable: true
 * </pre>
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = KsWebProperties.KEY)
public class KsWebProperties {

    public static final String KEY = KrismileProperties.KEY + "." + "web";

    /**
     * 请求响应配置文件
     */
    @NestedConfigurationProperty
    private KsResponseProperties response = new KsResponseProperties();

    /**
     * Jackson配置文件
     */
    @NestedConfigurationProperty
    private KsJacksonProperties jackson = new KsJacksonProperties();

}
