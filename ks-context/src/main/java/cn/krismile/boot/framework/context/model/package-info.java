/**
 * [ks-context] Model
 */
@NonNullApi
package cn.krismile.boot.framework.context.model;

import cn.krismile.boot.framework.core.annotation.jsr.NonNullApi;