package cn.krismile.boot.framework.autoconfigure.web.converter;


import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.boot.framework.core.constant.PatternConstant;
import cn.krismile.boot.framework.core.util.inner.StringUtils;
import org.springframework.core.convert.converter.Converter;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * LocalDate自定义转换器
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public class LocalDateConverter implements Converter<String, LocalDate> {

    @Override
    public LocalDate convert(@NonNull String s) {
        try {
            return LocalDate.parse(s, DateTimeFormatter.ofPattern(PatternConstant.Date.NORM_DATE_PATTERN));
        }catch (DateTimeParseException e) {
            if (StringUtils.isParsable(s)) {
                return Instant.ofEpochMilli(Long.parseLong(s)).atZone(ZoneId.systemDefault()).toLocalDate();
            }
            throw e;
        }
    }
}
