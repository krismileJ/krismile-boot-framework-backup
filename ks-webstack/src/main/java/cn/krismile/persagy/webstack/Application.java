package cn.krismile.persagy.webstack;

import cn.hutool.extra.spring.SpringUtil;
import cn.krismile.boot.framework.autoconfigure.filter.annotation.EnableGlobalCors;
import cn.krismile.boot.framework.core.util.common.UUIDUtils;
import cn.krismile.boot.framework.jwt.payload.JwtPayload;
import cn.krismile.boot.framework.jwt.util.JwtUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.time.LocalDateTime;
import java.util.Collections;

/**
 * Application
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@SpringBootApplication
@EnableGlobalCors
@Slf4j
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        printSwagger();
        printToken();
    }

    @SneakyThrows
    private static void printSwagger() {
        Environment env = SpringUtil.getApplicationContext().getEnvironment();
        log.info("-------------------------------------------------- Swagger --------------------------------------------------");
        // noinspection HttpUrlsUsage
        log.info("API文档地址: http://{}:{}{}{}/doc.html",
                InetAddress.getLocalHost().getHostAddress(),
                env.getProperty("server.port"),
                env.getProperty("server.servlet.context-path", ""),
                env.getProperty("spring.mvc.servlet.path", ""));
        log.info("-------------------------------------------------- Swagger --------------------------------------------------");
    }

    private static void printToken() {
        String token = JwtUtils.createToken("1556564615838572545", "123456", new JwtPayload()
                .setJti(UUIDUtils.random())
                .setIss("JiYinchuan")
                .setIat(LocalDateTime.now())
                .setExp(LocalDateTime.now().plusDays(1))
                .setNbf(LocalDateTime.now())
                .setAud(Collections.singletonList("Test Account"))
                .setSub("Admin User")
                .setUserId(1556564615838572545L)
                .setInternal(true));
        log.info("[Init] Token: {}", token);
    }
}
