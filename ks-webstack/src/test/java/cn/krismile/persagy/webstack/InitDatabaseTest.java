// package cn.krismile.persagy.webstack;
//
// import cn.krismile.boot.framework.core.util.common.ProjectUtils;
// import cn.krismile.persagy.webstack.enumeration.business.YesNoEnum;
// import cn.krismile.persagy.webstack.function.business.service.*;
// import cn.krismile.persagy.webstack.function.system.service.SysAdminService;
// import cn.krismile.persagy.webstack.pojo.domain.*;
// import com.alibaba.fastjson2.JSON;
// import com.alibaba.fastjson2.JSONArray;
// import com.alibaba.fastjson2.JSONObject;
// import org.apache.commons.collections4.CollectionUtils;
// import org.apache.commons.io.FileUtils;
// import org.apache.commons.lang3.StringUtils;
// import org.junit.jupiter.api.Test;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.context.SpringBootTest;
//
// import javax.annotation.PostConstruct;
// import java.io.File;
// import java.io.FileNotFoundException;
// import java.io.IOException;
// import java.nio.charset.StandardCharsets;
// import java.util.List;
// import java.util.Objects;
//
// /**
//  * 初始化数据库数据
//  *
//  * @author JiYinchuan
//  * @since 1.0.0
//  */
// @SpringBootTest
// public class InitDatabaseTest {
//
//     private File dataFile;
//
//     @PostConstruct
//     public void init() throws FileNotFoundException {
//         dataFile = new File(ProjectUtils.getResourcePath("data.json"));
//     }
//
//     @Autowired
//     private BizNavService navService;
//     @Autowired
//     private BizWebsiteService websiteService;
//     @Autowired
//     private SysAdminService adminService;
//     @Autowired
//     private BizRelNavSysAdminService relNavSysAdminService;
//     @Autowired
//     private BizRelWebsiteSysAdminService relWebsiteSysAdminService;
//     @Autowired
//     private BizRelNavWebsiteSysAdminService relNavWebsiteSysAdminService;
//
//     @Test
//     void initDatabase() throws IOException {
//         Long adminId = 1556564615838572545L;
//         navService.lambdaUpdate().remove();
//         websiteService.lambdaUpdate().remove();
//         // noinspection ConstantConditions
//         if (Objects.nonNull(adminId)) {
//             relNavSysAdminService.lambdaUpdate()
//                     .eq(BizRelNavSysAdminDO::getRelSysAdminId, adminId)
//                     .remove();
//             relWebsiteSysAdminService.lambdaUpdate()
//                     .eq(BizRelWebsiteSysAdminDO::getRelSysAdminId, adminId)
//                     .remove();
//             relNavWebsiteSysAdminService.lambdaUpdate()
//                     .eq(BizRelNavWebsiteSysAdminDO::getRelSysAdminId, adminId)
//                     .remove();
//         }
//         execute(JSON.parseArray(FileUtils.readFileToString(dataFile, StandardCharsets.UTF_8)), null, adminId);
//     }
//
//     @Test
//     void initAdmin() {
//         SysAdminDO sysAdminDO = new SysAdminDO();
//         sysAdminDO.setAccount("admin");
//         sysAdminDO.setRealName("Krismile");
//         sysAdminDO.setPhone("15181743604");
//         sysAdminDO.setEmail("jyc@krismile.cn");
//         sysAdminDO.setStatus(YesNoEnum.YES);
//         sysAdminDO.setRelDepartmentId(-1L);
//
//         adminService.save(sysAdminDO);
//
//         List<BizNavDO> dbNavs = navService.list();
//         List<BizWebsiteDO> dbWebSites = websiteService.list();
//         for (BizNavDO dbNav : dbNavs) {
//             for (BizWebsiteDO dbWebSite : dbWebSites) {
//                 BizRelNavWebsiteSysAdminDO bizRelNavWebsiteSysAdminDO = new BizRelNavWebsiteSysAdminDO();
//                 bizRelNavWebsiteSysAdminDO.setRelNavId(dbNav.getId());
//                 bizRelNavWebsiteSysAdminDO.setRelWebsiteId(dbWebSite.getId());
//                 bizRelNavWebsiteSysAdminDO.setRelSysAdminId(sysAdminDO.getId());
//                 relNavWebsiteSysAdminService.save(bizRelNavWebsiteSysAdminDO);
//             }
//         }
//     }
//
//     private void execute(JSONArray jsonArray, Long parentId, Long adminId) {
//         for (int i = 0; i < jsonArray.size(); i++) {
//             JSONObject navObject = jsonArray.getJSONObject(i);
//             String navIcon = navObject.getString("class");
//             String navTitle = navObject.getString("title");
//             String navTagName = navObject.getString("tagName");
//             JSONArray navChildren = navObject.getJSONArray("children");
//             JSONArray navWebSites = navObject.getJSONArray("webSites");
//
//             BizNavDO addNav = new BizNavDO();
//             addNav.setTitle(StringUtils.isNotBlank(navTitle) ? navTitle : null);
//             addNav.setTagName(StringUtils.isNotBlank(navTagName) ? navTagName : null);
//             addNav.setIcon(StringUtils.isNotBlank(navIcon) ? navIcon : null);
//             addNav.setParentId(Objects.nonNull(parentId) ? parentId : null);
//
//             navService.saveValidate(addNav);
//             if (Objects.nonNull(adminId)) {
//                 BizRelNavSysAdminDO bizRelNavSysAdminDO = new BizRelNavSysAdminDO();
//                 bizRelNavSysAdminDO.setOrderWeight(i);
//                 bizRelNavSysAdminDO.setRelNavId(addNav.getId());
//                 bizRelNavSysAdminDO.setRelSysAdminId(adminId);
//                 relNavSysAdminService.saveValidate(bizRelNavSysAdminDO);
//             }
//
//             if (CollectionUtils.isNotEmpty(navChildren)) {
//                 execute(navChildren, addNav.getId(), adminId);
//             }
//
//             if (CollectionUtils.isNotEmpty(navWebSites)) {
//                 for (int i1 = 0; i1 < navWebSites.size(); i1++) {
//                     JSONObject websiteObject = navWebSites.getJSONObject(i1);
//                     String webTitle = websiteObject.getString("title");
//                     String webDescription = websiteObject.getString("description");
//                     String webTagName = websiteObject.getString("tagName");
//                     String webImage = websiteObject.getString("image");
//                     String webLink = websiteObject.getString("link");
//
//                     BizWebsiteDO addWebsite = new BizWebsiteDO();
//                     addWebsite.setTitle(StringUtils.isNotBlank(webTitle) ? webTitle : null);
//                     addWebsite.setDescription(StringUtils.isNotBlank(webDescription) ? webDescription : null);
//                     addWebsite.setTagName(StringUtils.isNotBlank(webTagName) ? webTagName : null);
//                     addWebsite.setImage(StringUtils.isNotBlank(webImage) ? webImage : null);
//                     addWebsite.setLink(StringUtils.isNotBlank(webLink) ? webLink : null);
//
//                     websiteService.saveValidate(addWebsite);
//                     if (Objects.nonNull(adminId)) {
//                         BizRelWebsiteSysAdminDO bizRelWebsiteSysAdminDO = new BizRelWebsiteSysAdminDO();
//                         bizRelWebsiteSysAdminDO.setOrderWeight(i1);
//                         bizRelWebsiteSysAdminDO.setRelWebsiteId(addWebsite.getId());
//                         bizRelWebsiteSysAdminDO.setRelSysAdminId(adminId);
//                         relWebsiteSysAdminService.saveValidate(bizRelWebsiteSysAdminDO);
//
//                         BizRelNavWebsiteSysAdminDO bizRelNavWebsiteSysAdminDO = new BizRelNavWebsiteSysAdminDO();
//                         bizRelNavWebsiteSysAdminDO.setRelNavId(addNav.getId());
//                         bizRelNavWebsiteSysAdminDO.setRelWebsiteId(addWebsite.getId());
//                         bizRelNavWebsiteSysAdminDO.setRelSysAdminId(adminId);
//                         relNavWebsiteSysAdminService.saveValidate(bizRelNavWebsiteSysAdminDO);
//                     }
//                 }
//             }
//         }
//     }
// }
