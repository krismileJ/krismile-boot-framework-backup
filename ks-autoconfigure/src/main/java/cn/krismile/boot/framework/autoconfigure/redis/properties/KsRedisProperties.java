package cn.krismile.boot.framework.autoconfigure.redis.properties;

import cn.krismile.boot.framework.autoconfigure.KrismileProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Redis配置文件
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = KsRedisProperties.KEY)
public class KsRedisProperties {

    public static final String KEY = KrismileProperties.KEY + "." + "redis";

    /**
     * 是否启用
     */
    private boolean enable = true;

}
