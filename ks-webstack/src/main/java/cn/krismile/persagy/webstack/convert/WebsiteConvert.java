package cn.krismile.persagy.webstack.convert;

import cn.krismile.persagy.webstack.pojo.domain.BizWebsiteDO;
import cn.krismile.persagy.webstack.pojo.dto.WebsiteDTO;
import cn.krismile.persagy.webstack.pojo.vo.admin.WebsiteVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 网站转换器
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Mapper
public interface WebsiteConvert {

    WebsiteConvert INSTANCE = Mappers.getMapper(WebsiteConvert.class);

    // ------------------------- DO To DTO -------------------------

    /**
     * 转换DTO
     *
     * @param source DO
     * @return DTO
     * @since 1.0.0
     */
    @Mappings({
            @Mapping(target = "websiteId", source = "id"),
            @Mapping(target = "websiteTitle", source = "title"),
            @Mapping(target = "websiteLink", source = "link"),
            @Mapping(target = "websiteTagName", source = "tagName"),
            @Mapping(target = "websiteImage", source = "image"),
            @Mapping(target = "websiteDescription", source = "description"),
            @Mapping(target = "websiteType", source = "type"),
            @Mapping(target = "websiteStatus", source = "status"),
            // 拓展字段
            @Mapping(target = "relNavId", source = "relNavId"),
            @Mapping(target = "websiteOrderWeight", source = "orderWeight")
    })
    WebsiteDTO toDto(BizWebsiteDO source);

    /**
     * 转换DTO
     *
     * @param source DO
     * @return DTO
     * @since 1.0.0
     */
    List<WebsiteDTO> toDto(List<BizWebsiteDO> source);

    // ------------------------- DTO To VO -------------------------

    /**
     * 转换VO
     *
     * @param source DTO
     * @return VO
     * @since 1.0.0
     */
    List<WebsiteVO> toVo(List<WebsiteDTO> source);

}
