package cn.krismile.persagy.webstack.controller.web;

import cn.krismile.boot.framework.core.util.common.ProjectUtils;
import cn.krismile.persagy.webstack.pojo.vo.web.NavVO;
import com.alibaba.fastjson2.JSON;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * IndexController
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Controller
public class IndexController {

    @RequestMapping
    public String index(Model model) throws IOException {
        File jsonFile = new File(ProjectUtils.getResourcePath("data.json"));
        List<NavVO> objects = JSON.parseArray(FileUtils.readFileToString(jsonFile, StandardCharsets.UTF_8), NavVO.class);
        model.addAttribute("dataList", objects);
        return "index";
    }
}
