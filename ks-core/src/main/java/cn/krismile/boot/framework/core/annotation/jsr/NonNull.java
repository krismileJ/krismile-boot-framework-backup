package cn.krismile.boot.framework.core.annotation.jsr;

import javax.annotation.Nonnull;
import javax.annotation.meta.TypeQualifierNickname;
import java.lang.annotation.*;

/**
 * 利用JSR-305元注解将Java中的nullable指示为具有JSR-305支持的通用工具
 *
 * @author JiYinchuan
 * @see NonNullApi
 * @see NonNullFields
 * @see Nullable
 * @since 1.0.0
 */
@Target({ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Nonnull
@TypeQualifierNickname
public @interface NonNull {
}
