package cn.krismile.boot.framework.autoconfigure.filter.properties.children;

import cn.krismile.boot.framework.autoconfigure.filter.properties.KsFilterProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * XSS攻击防御配置文件
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = KsXssProperties.KEY)
public class KsXssProperties {

    public static final String KEY = KsFilterProperties.KEY + "." + "xss";

    /**
     * 是否启用
     */
    private boolean enable = true;

}
