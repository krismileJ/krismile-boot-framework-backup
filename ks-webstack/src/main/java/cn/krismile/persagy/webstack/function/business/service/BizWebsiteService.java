package cn.krismile.persagy.webstack.function.business.service;

import cn.krismile.boot.framework.core.exception.EnumIllegalArgumentException;
import cn.krismile.boot.framework.core.exception.client.AuthorityException;
import cn.krismile.boot.framework.core.exception.client.RequestServerException;
import cn.krismile.boot.framework.mybatisplus.exception.MybatisServiceException;
import cn.krismile.boot.framework.mybatisplus.service.BaseService;
import cn.krismile.persagy.webstack.pojo.domain.BizWebsiteDO;
import cn.krismile.persagy.webstack.pojo.dto.WebsiteDTO;
import cn.krismile.persagy.webstack.pojo.query.WebsiteQuery;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.List;

/**
 * 业务-网站表 Service
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface BizWebsiteService extends BaseService<BizWebsiteDO> {

    String DEFAULT_NOT_FOUND_MESSAGE = "未找到网站";

    // ------------------------- 查询相关 -------------------------

    /**
     * 网站数据列表查询
     *
     * @param relNavIds 关联的导航栏ID
     * @return 网站数据列表
     * @throws RequestServerException 用户ID获取失败
     * @since 1.0.0
     */
    List<WebsiteDTO> listByUser(@NonNull List<Long> relNavIds) throws RequestServerException;

    /**
     * 网站数据列表查询
     *
     * @param query 查询条件
     * @return 网站数据列表
     * @throws RequestServerException 用户ID获取失败
     * @since 1.0.0
     */
    List<WebsiteDTO> listByUser(@NonNull WebsiteQuery query) throws RequestServerException;

    /**
     * 内置网站数据列表查询
     *
     * @return 内置网站数据列表
     * @since 1.0.0
     */
    List<WebsiteDTO> listByInternal();

    /**
     * 用户公开网站数据列表查询
     *
     * @return 用户公开网站数据列表
     * @since 1.0.0
     */
    List<WebsiteDTO> listByPublic();

    // ------------------------- 操作相关 -------------------------

    /**
     * 网站添加
     *
     * @param dto 网站DTO
     * @return 操作是否成功
     * @throws MybatisServiceException      无效的父导航栏
     * @throws RequestServerException       用户ID获取失败
     * @throws AuthorityException           非网站拥有者无法进行操作
     * @throws EnumIllegalArgumentException 未知的枚举类型
     * @since 1.0.0
     */
    boolean add(@NonNull WebsiteDTO dto)
            throws MybatisServiceException, RequestServerException, AuthorityException, EnumIllegalArgumentException;

    /**
     * 网站编辑
     *
     * @param dto 网站DTO
     * @return 操作是否成功
     * @throws MybatisServiceException      无效的父导航栏
     * @throws RequestServerException       用户ID获取失败
     * @throws AuthorityException           非网站拥有者无法进行操作
     * @throws EnumIllegalArgumentException 未知的枚举类型
     * @since 1.0.0
     */
    boolean edit(@NonNull WebsiteDTO dto)
            throws MybatisServiceException, RequestServerException, AuthorityException, EnumIllegalArgumentException;

    /**
     * 网站删除
     *
     * @param ids 导航ID
     * @return 操作是否成功
     * @throws RequestServerException       用户ID获取失败
     * @throws AuthorityException           非网站拥有者无法进行操作
     * @throws EnumIllegalArgumentException 未知的枚举类型
     * @since 1.0.0
     */
    boolean del(@Nullable List<Long> ids)
            throws RequestServerException, AuthorityException, EnumIllegalArgumentException;

}
