package cn.krismile.persagy.webstack.function.business.service;

import cn.krismile.persagy.webstack.pojo.vo.web.NavVO;

import java.util.List;

/**
 * 页面服务
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface WebService {

    /**
     * 页面数据缓存获取
     *
     * @return 页面数据
     * @since 1.0.0
     */
    List<NavVO> listCache();

}
