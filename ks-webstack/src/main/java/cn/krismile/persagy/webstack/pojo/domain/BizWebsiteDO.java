package cn.krismile.persagy.webstack.pojo.domain;

import cn.krismile.boot.framework.mybatisplus.domain.BaseDO;
import cn.krismile.persagy.webstack.enumeration.business.ResourceStatusEnum;
import cn.krismile.persagy.webstack.enumeration.business.ResourceTypeEnum;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 业务-网站表
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("biz_website")
@Schema(title = "BizWebsiteDO对象", description = "业务-网站表")
public class BizWebsiteDO extends BaseDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "标题名称")
    @TableField("title")
    private String title;

    @Schema(description = "缩略图")
    @TableField("image")
    private String image;

    @Schema(description = "链接地址")
    @TableField("link")
    private String link;

    @Schema(description = "标签名称")
    @TableField("tag_name")
    private String tagName;

    @Schema(description = "描述信息")
    @TableField("description")
    private String description;

    @Schema(description = "类型 [INTERNAL: 内部, USER: 用户, PUBLIC: 公共]")
    @TableField("type")
    private ResourceTypeEnum type;

    @Schema(description = "状态 [UNAPPROVED: 待审核, APPROVED: 已审核]")
    @TableField("status")
    private ResourceStatusEnum status;

    @Schema(description = "关联创建用户ID")
    @TableField("rel_create_admin_id")
    private Long relCreateAdminId;

    // ------------------------- 拓展字段 -------------------------

    @Schema(description = "关联导航栏ID")
    @TableField(exist = false)
    private Long relNavId;

    @Schema(description = "排序权重, 默认值为 [0]")
    @TableField(exist = false)
    private Integer orderWeight;

}
