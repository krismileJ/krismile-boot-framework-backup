package cn.krismile.boot.framework.autoconfigure.cache;

import cn.krismile.boot.framework.autoconfigure.redis.KsRedisAutoConfiguration;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.RedisSerializer;

import java.util.Objects;

;

/**
 * Redis缓存自动配置
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Configuration(proxyBeanMethods = false)
@AutoConfigureAfter(KsRedisAutoConfiguration.class)
@ConditionalOnClass(RedisConnectionFactory.class)
@ConditionalOnBean(RedisConnectionFactory.class)
@ConditionalOnMissingBean(CacheManager.class)
@ConditionalOnProperty(name = "spring.cache.type", havingValue = "redis")
public class KsRedisCacheConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public RedisCacheConfiguration redisCacheConfiguration(
            CacheProperties cacheProperties,
            @Qualifier(KsRedisAutoConfiguration.KEY_SERIALIZER_BEAN_NAME) RedisSerializer<String> keySerializer,
            @Qualifier(KsRedisAutoConfiguration.VALUE_SERIALIZER_BEAN_NAME) RedisSerializer<Object> valueSerializer) {
        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig();

        if (Objects.nonNull(cacheProperties.getRedis().getTimeToLive())) {
            config = config.entryTtl(cacheProperties.getRedis().getTimeToLive());
        }
        if (Objects.nonNull(cacheProperties.getRedis().getKeyPrefix())) {
            config = config.prefixCacheNameWith(cacheProperties.getRedis().getKeyPrefix());
        }
        if (!cacheProperties.getRedis().isCacheNullValues()) {
            config = config.disableCachingNullValues();
        }
        if (!cacheProperties.getRedis().isUseKeyPrefix()) {
            config = config.disableKeyPrefix();
        }

        config.serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(keySerializer));
        config.serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(valueSerializer));
        return config;
    }
}
