package cn.krismile.persagy.webstack.config;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.media.StringSchema;
import io.swagger.v3.oas.models.parameters.HeaderParameter;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * Swagger配置
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@SpringBootConfiguration
public class SwaggerConfigure {

    private static final String DEVELOPER = "JiYinchuan";
    private static final String EMAIL = "jyc@krismile.cn";
    private static final String SITE = "https://www.krismile.cn";

    @Bean
    public OpenAPI openApi() {
        return new OpenAPI().info(new Info()
                .title("Persagy-Webstack")
                .description("Persagy-Webstack")
                .termsOfService(SITE)
                .contact(new Contact().name(DEVELOPER).url(SITE).email(EMAIL))
                .summary("Krismile")
                .version("1.0.0"));
    }

    @Bean
    public GroupedOpenApi groupedOpenApi() {
        return GroupedOpenApi.builder()
                .group("Open")
                .pathsToMatch("/**")
                .addOpenApiMethodFilter(method -> method.isAnnotationPresent(Operation.class))
                .addOperationCustomizer((operation, handlerMethod) -> operation.addParametersItem(new HeaderParameter()
                        .name("Authorization").description("Token").required(true)
                        .schema(new StringSchema().name("Authorization").description("Token"))))
                .build();
    }
}
