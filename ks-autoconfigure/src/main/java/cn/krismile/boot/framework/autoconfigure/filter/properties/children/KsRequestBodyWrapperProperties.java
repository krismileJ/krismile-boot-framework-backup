package cn.krismile.boot.framework.autoconfigure.filter.properties.children;

import cn.krismile.boot.framework.autoconfigure.filter.properties.KsFilterProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 请求体包装配置文件
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = KsRequestBodyWrapperProperties.KEY)
public class KsRequestBodyWrapperProperties {

    public static final String KEY = KsFilterProperties.KEY + "." + "request-body-wrapper";

    /**
     * 是否启用
     */
    private boolean enable = true;

}
