package cn.krismile.boot.framework.mybatisplus.exception;

import cn.krismile.boot.framework.core.annotation.jsr.Nullable;
import cn.krismile.boot.framework.core.exception.ApplicationException;
import cn.krismile.boot.framework.mybatisplus.enumeration.error.MybatisServiceErrorEnum;

/**
 * MybatisService自定义异常
 *
 * @author JiYinchuan
 * @see MybatisServiceErrorEnum
 * @since 1.0.0
 */
public class MybatisServiceException extends ApplicationException {

    private static final long serialVersionUID = -3357707602915204679L;

    public MybatisServiceException(MybatisServiceErrorEnum mybatisServiceErrorEnum) {
        super(mybatisServiceErrorEnum);
    }

    public MybatisServiceException(MybatisServiceErrorEnum mybatisServiceErrorEnum, @Nullable String userTip) {
        super(mybatisServiceErrorEnum, userTip);
    }
}