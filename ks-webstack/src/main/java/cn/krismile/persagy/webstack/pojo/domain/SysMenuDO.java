package cn.krismile.persagy.webstack.pojo.domain;

import cn.krismile.boot.framework.mybatisplus.domain.BaseDO;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 系统-菜单表
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_menu")
@Schema(title = "SysMenuDO对象", description = "系统-菜单表")
public class SysMenuDO extends BaseDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "菜单名称")
    @TableField("name")
    private String name;

    @Schema(description = "路由地址")
    @TableField("path")
    private String path;

    @Schema(description = "图标样式")
    @TableField("icon")
    private String icon;

    @Schema(description = "是否为多级菜单 [0: 否, 1: 是]")
    @TableField("isMulti")
    private Boolean multi;

    @Schema(description = "自关联父级菜单ID")
    @TableField("parent_id")
    private Long parentId;

}
