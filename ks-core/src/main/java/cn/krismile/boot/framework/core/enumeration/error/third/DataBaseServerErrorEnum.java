package cn.krismile.boot.framework.core.enumeration.error.third;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import cn.krismile.boot.framework.core.enumeration.error.client.RequestParamErrorEnum;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 数据库服务出错异常枚举
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public enum DataBaseServerErrorEnum implements BaseEnum<String> {

    /**
     * 数据库服务出错
     */
    DATABASE_SERVICE_ERROR("C0300", "数据库服务出错"),

    /**
     * 表不存在
     */
    DATABASE_TABLE_NOT_EXISTS("C0311", "表不存在"),

    /**
     * 列不存在
     */
    DATABASE_COLUMN_NOT_EXISTS("C0312", "列不存在"),

    /**
     * 多表关联中存在多个相同名称的列
     */
    DATABASE_COLUMN_REPEAT("C0321", "多表关联中存在多个相同名称的列"),

    /**
     * 数据库死锁
     */
    DATABASE_DEADLOCK("C0331", "数据库死锁"),

    /**
     * 主键冲突
     */
    DATABASE_PRIMARY_KEY_CONFLICT("C0341", "主键冲突");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    DataBaseServerErrorEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static RequestParamErrorEnum analyze(String value) {
        return BaseEnum.analyze(value, RequestParamErrorEnum.class);
    }
}
