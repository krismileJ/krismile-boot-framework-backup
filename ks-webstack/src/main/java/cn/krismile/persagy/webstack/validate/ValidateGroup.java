package cn.krismile.persagy.webstack.validate;

/**
 * 验证分析
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface ValidateGroup {

    interface Insert {}
    interface Update {}
}
