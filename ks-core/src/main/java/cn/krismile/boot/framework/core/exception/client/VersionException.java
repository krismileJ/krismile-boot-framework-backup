package cn.krismile.boot.framework.core.exception.client;

import cn.krismile.boot.framework.core.enumeration.error.client.VersionErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 用户当前版本自定义异常
 *
 * @author JiYinchuan
 * @see VersionErrorEnum
 * @since 1.0.0
 */
public class VersionException extends ApplicationException {

    private static final long serialVersionUID = 9169335862405901543L;

    public VersionException(VersionErrorEnum versionErrorEnum) {
        super(versionErrorEnum);
    }

    public VersionException(VersionErrorEnum versionErrorEnum, String userTip) {
        super(versionErrorEnum, userTip);
    }
}
