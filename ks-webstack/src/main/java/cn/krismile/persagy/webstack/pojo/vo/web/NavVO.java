package cn.krismile.persagy.webstack.pojo.vo.web;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;

import java.util.List;

/**
 * 导航栏与网站DTO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
public class NavVO {

    /**
     * ID
     */
    private Long id;

    /**
     * 图标样式
     */
    @JSONField(name = "class")
    private String cls;

    /**
     * 标题
     */
    private String title;

    /**
     * 标签名称
     */
    private String tagName;

    /**
     * 子导航栏
     */
    private List<NavVO> children;

    /**
     * 绑定的网站
     */
    private List<WebSiteVO> webSites;

}
