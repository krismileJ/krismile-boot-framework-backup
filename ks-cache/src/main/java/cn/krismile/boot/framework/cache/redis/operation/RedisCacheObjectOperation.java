package cn.krismile.boot.framework.cache.redis.operation;

import cn.krismile.boot.framework.cache.CacheObjectOperation;
import cn.krismile.boot.framework.cache.redis.RedisCache;
import cn.krismile.boot.framework.core.annotation.jsr.Nullable;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * 对象缓存操作类
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@SuppressWarnings("unchecked")
public class RedisCacheObjectOperation implements CacheObjectOperation {

    /**
     * 代理类
     */
    // private final ValueOperations<String, Object> delegate;


    private final RedisTemplate<String, Object> delegate;

    /**
     * 构造器
     *
     * @param redisCache Redis缓存操作类
     * @since 1.0.0
     */
    public RedisCacheObjectOperation(RedisCache redisCache) {
        this.delegate = redisCache.delegate();
    }

    @Override
    @Nullable
    public <V> V get(String key) {
        return (V) delegate.opsForValue().get(key);
    }

    @Override
    public void set(String key, Object value) {
        delegate.opsForValue().set(key, value);
    }

    @Override
    public void set(String key, Object value, long expireTime, TimeUnit timeUnit) {
        delegate.opsForValue().set(key, value, expireTime, timeUnit);
    }

    @Override
    @Nullable
    public Long increment(String key, long factor) {
        return delegate.opsForValue().increment(key, factor);
    }

    @Override
    @Nullable
    public Long decrement(String key, long factor) {
        return delegate.opsForValue().decrement(key, factor);
    }
}
