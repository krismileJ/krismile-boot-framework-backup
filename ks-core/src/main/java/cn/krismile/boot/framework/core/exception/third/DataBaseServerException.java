package cn.krismile.boot.framework.core.exception.third;

import cn.krismile.boot.framework.core.enumeration.error.third.DataBaseServerErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 数据库服务出错自定义异常
 *
 * @author JiYinchuan
 * @see DataBaseServerErrorEnum
 * @since 1.0.0
 */
public class DataBaseServerException extends ApplicationException {

    private static final long serialVersionUID = -153700551956140763L;

    public DataBaseServerException(DataBaseServerErrorEnum dataBaseServerErrorEnum) {
        super(dataBaseServerErrorEnum);
    }

    public DataBaseServerException(DataBaseServerErrorEnum dataBaseServerErrorEnum, String userTip) {
        super(dataBaseServerErrorEnum, userTip);
    }
}
