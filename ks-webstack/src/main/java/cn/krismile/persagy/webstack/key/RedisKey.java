package cn.krismile.persagy.webstack.key;

import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.boot.framework.core.util.Assert;

/**
 * RedisKey
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface RedisKey {

    /**
     * 系统用户相关
     *
     * @author JiYinchuan
     * @since 1.0.0
     */
    interface Admin {

        /**
         * 系统用户信息Key
         *
         * @param adminId 系统用户ID
         * @return 统用户信息
         * @since 1.0.0
         */
        static String adminInfo(@NonNull Long adminId) {
            return RedisKey.Admin.adminInfo(String.valueOf(adminId));
        }

        /**
         * 系统用户信息Key
         *
         * @param adminId 系统用户ID
         * @return 统用户信息
         * @since 1.0.0
         */
        static String adminInfo(@NonNull String adminId) {
            Assert.notNull(adminId, "AdminId must not be null");

            return "ADMIN:INFO:" + adminId;
        }
    }
}
