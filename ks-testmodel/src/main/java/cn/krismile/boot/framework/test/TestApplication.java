package cn.krismile.boot.framework.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * TestApplication
 * <p>
 * <a href="https://docs.oracle.com/javase/8/docs/technotes/tools/windows/javadoc.html">https://docs.oracle.com/javase/8/docs/technotes/tools/windows/javadoc.html</a>
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@SpringBootApplication
public class TestApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }
}
