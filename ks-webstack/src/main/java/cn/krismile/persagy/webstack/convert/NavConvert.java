package cn.krismile.persagy.webstack.convert;

import cn.krismile.boot.framework.mybatisplus.domain.BaseDO;
import cn.krismile.persagy.webstack.pojo.domain.BizNavDO;
import cn.krismile.persagy.webstack.pojo.dto.NavDTO;
import cn.krismile.persagy.webstack.pojo.vo.admin.NavVO;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 导航栏转换器
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Mapper
@DecoratedWith(AbstractNavConvertDecorator.class)
public interface NavConvert {

    NavConvert INSTANCE = Mappers.getMapper(NavConvert.class);

    // ------------------------- DO To DTO -------------------------

    /**
     * 转换DTO
     *
     * @param source DO
     * @return DTO
     * @since 1.0.0
     */
    @Mappings({
            @Mapping(target = "templateId", ignore = true),
            @Mapping(target = "navId", source = "id"),
            @Mapping(target = "navTitle", source = "title"),
            @Mapping(target = "navTagName", source = "tagName"),
            @Mapping(target = "navIcon", source = "icon"),
            @Mapping(target = "navOrderWeight", source = "orderWeight"),
            @Mapping(target = "navIsAlreadyRelation", source = "isAlreadyRelation"),
            @Mapping(target = "navType", source = "type"),
            @Mapping(target = "navStatus", source = "status"),
            @Mapping(target = "navParentId", source = "parentId"),
            @Mapping(target = "navChildren", ignore = true)
    })
    NavDTO toDto(BizNavDO source);

    /**
     * 转换DTO
     *
     * @param source DO
     * @return DTO
     * @since 1.0.0
     */
    List<NavDTO> toDto(List<BizNavDO> source);

    // ------------------------- DTO To DO -------------------------

    /**
     * 转换DO
     *
     * @param source DTO
     * @return DO
     * @since 1.0.0
     */
    default BizNavDO toDo(NavDTO source) {
        return toDo(source, new BizNavDO());
    }

    /**
     * 转换DO
     *
     * @param source DTO
     * @param domain target
     * @return DO
     * @since 1.0.0
     */
    @Mappings({
            @Mapping(target = "title", source = "navTitle"),
            @Mapping(target = "tagName", source = "navTagName"),
            @Mapping(target = "icon", source = "navIcon"),
            @Mapping(target = "type", source = "navType"),
            @Mapping(target = "status", source = "navStatus"),
            @Mapping(target = "parentId", source = "navParentId"),
            @Mapping(target = "relCreateAdminId", ignore = true),
            @Mapping(target = "orderWeight", ignore = true),
            @Mapping(target = "isAlreadyRelation", ignore = true),
            @Mapping(target = BaseDO.ID, ignore = true),
            @Mapping(target = BaseDO.CREATE_TIME, ignore = true),
            @Mapping(target = BaseDO.UPDATE_TIME, ignore = true)

    })
    BizNavDO toDo(NavDTO source, @MappingTarget BizNavDO domain);

    // ------------------------- DTO To VO -------------------------

    /**
     * 转换VO
     *
     * @param source DTO
     * @return VO
     * @since 1.0.0
     */
    List<NavVO> toVo(List<NavDTO> source);

}
