package cn.krismile.persagy.webstack.function.system.service;

import cn.krismile.persagy.webstack.pojo.domain.SysDepartmentDO;
import cn.krismile.boot.framework.mybatisplus.service.BaseService;

/**
 * 系统-部门表 Service
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface SysDepartmentService extends BaseService<SysDepartmentDO> {

}
