package cn.krismile.persagy.webstack.pojo.domain;

import cn.krismile.boot.framework.mybatisplus.domain.BaseDO;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 系统-角色表
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_role")
@Schema(title = "SysRoleDO对象", description = "系统-角色表")
public class SysRoleDO extends BaseDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "角色名称")
    @TableField("name")
    private String name;

    @Schema(description = "角色描述")
    @TableField("description")
    private String description;

}
