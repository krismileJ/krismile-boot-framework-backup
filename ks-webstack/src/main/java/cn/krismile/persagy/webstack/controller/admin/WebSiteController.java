package cn.krismile.persagy.webstack.controller.admin;

import cn.krismile.boot.framework.core.response.ResultVO;
import cn.krismile.boot.framework.core.response.vo.BaseVO;
import cn.krismile.boot.framework.core.response.vo.MultiVO;
import cn.krismile.boot.framework.core.response.vo.SingleVO;
import cn.krismile.persagy.webstack.convert.NavConvert;
import cn.krismile.persagy.webstack.convert.WebsiteConvert;
import cn.krismile.persagy.webstack.function.business.service.BizNavService;
import cn.krismile.persagy.webstack.function.business.service.BizRelNavSysAdminService;
import cn.krismile.persagy.webstack.function.business.service.BizRelWebsiteSysAdminService;
import cn.krismile.persagy.webstack.function.business.service.BizWebsiteService;
import cn.krismile.persagy.webstack.pojo.dto.NavDTO;
import cn.krismile.persagy.webstack.pojo.dto.WebsiteDTO;
import cn.krismile.persagy.webstack.pojo.dto.WebsiteSortDTO;
import cn.krismile.persagy.webstack.pojo.query.WebsiteQuery;
import cn.krismile.persagy.webstack.pojo.vo.admin.NavVO;
import cn.krismile.persagy.webstack.pojo.vo.admin.WebsiteVO;
import cn.krismile.persagy.webstack.validate.ValidateGroup;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 网站管理
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Tag(name = "网站管理")
@RestController
@RequestMapping("/website")
public class WebSiteController {

    @Resource
    private BizNavService navService;
    @Resource
    private BizWebsiteService websiteService;
    @Resource
    private BizRelWebsiteSysAdminService relWebsiteSysAdminService;
    @Resource
    private BizRelNavSysAdminService relNavSysAdminService;

    // ------------------------- 网站相关 -------------------------

    @Operation(summary = "[查询-网站] 列表")
    @GetMapping
    public MultiVO<WebsiteVO> listWebsites(@ParameterObject @Validated WebsiteQuery query) {
        return ResultVO.okMulti(WebsiteConvert.INSTANCE.toVo(websiteService.listByUser(query)));
    }

    @Operation(summary = "[查询-网站] 内置")
    @GetMapping("/internal")
    public MultiVO<WebsiteVO> listInternalWebsites() {
        return ResultVO.okMulti(WebsiteConvert.INSTANCE.toVo(websiteService.listByInternal()));
    }

    @Operation(summary = "[查询-网站] 公开")
    @GetMapping("/public")
    public MultiVO<WebsiteVO> listPublicWebsites() {
        return ResultVO.okMulti(WebsiteConvert.INSTANCE.toVo(websiteService.listByPublic()));
    }

    @Operation(summary = "[新增- 网站]")
    @PostMapping
    @Parameter(name = "websiteId", hidden = true)
    public BaseVO addWebsite(@RequestBody @Validated(ValidateGroup.Insert.class) WebsiteDTO dto) {
        return ResultVO.ok(() -> websiteService.add(dto));
    }

    @Operation(summary = "[编辑-网站]")
    @PutMapping
    public BaseVO editWebsite(@RequestBody @Validated(ValidateGroup.Update.class) WebsiteDTO dto) {
        return ResultVO.ok(() -> websiteService.edit(dto));
    }

    @Operation(summary = "[排序-网站] ")
    @PutMapping("/sort")
    public BaseVO sortWebsite(@RequestBody @Validated WebsiteSortDTO dto) {
        return ResultVO.ok(() -> relWebsiteSysAdminService.sortExchange(dto));
    }

    @Operation(summary = "[删除-网站]")
    @DeleteMapping
    public BaseVO delWebsites(@RequestBody List<Long> ids) {
        return ResultVO.ok(() -> websiteService.del(ids));
    }

    // ------------------------- 导航栏相关 -------------------------

    @Operation(summary = "[查询-导航栏] 列表")
    @GetMapping("/nav")
    public MultiVO<NavVO> listNavs() {
        return ResultVO.okMulti(NavConvert.INSTANCE.toVo(navService.listByUser()));
    }

    @Operation(summary = "[查询-导航栏] 内置")
    @GetMapping("/nav/internal")
    public MultiVO<NavVO> listInternalNavs() {
        return ResultVO.okMulti(NavConvert.INSTANCE.toVo(navService.listByInternal()));
    }

    @Operation(summary = "[查询-导航栏] 公开")
    @GetMapping("/nav/public")
    public MultiVO<NavVO> listPublicNavs() {
        return ResultVO.okMulti(NavConvert.INSTANCE.toVo(navService.listByPublic()));
    }

    @Operation(summary = "[新增-导航栏]")
    @PostMapping("/nav")
    public BaseVO addNav(@RequestBody @Validated(ValidateGroup.Insert.class) NavDTO dto) {
        return ResultVO.ok(() -> navService.add(dto));
    }

    @Operation(summary = "[编辑-导航栏]")
    @PutMapping("/nav")
    public BaseVO editNav(@RequestBody @Validated(ValidateGroup.Update.class) NavDTO dto) {
        return ResultVO.ok(() -> navService.edit(dto));
    }

    @Operation(summary = "[删除-导航栏]")
    @DeleteMapping("/nav")
    public BaseVO delNavs(@RequestBody List<Long> ids) {
        return ResultVO.ok(() -> navService.del(ids));
    }

    @Operation(summary = "[检查-导航栏] 关联")
    @GetMapping("/nav/{navId}/relation")
    public SingleVO<Boolean> checkRelationNav(@PathVariable("navId") Long navId) {
        return ResultVO.okSingle(relNavSysAdminService.checkRelation(navId));
    }
}
