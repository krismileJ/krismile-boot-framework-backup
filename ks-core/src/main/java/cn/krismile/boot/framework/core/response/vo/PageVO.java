package cn.krismile.boot.framework.core.response.vo;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import cn.krismile.boot.framework.core.response.ResultVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * 分页VO
 *
 * <p>该类为分页VO, 用于分页数据返回的情况下使用, 分页详情数据请参考 {@link PageDetailVO} 说明, 使用 {@link ResultVO} 中相关方法进行返回
 *
 * @author JiYinchuan
 * @see PageDetailVO
 * @see ResultVO
 * @since 1.0.0
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public final class PageVO<T> extends BaseVO implements Serializable {

    private static final long serialVersionUID = -772532661816381979L;

    /**
     * 分页详情数据
     */
    private PageDetailVO detail;

    /**
     * 分页数据
     */
    private Collection<T> data;

    /**
     * 构造器
     *
     * @since 1.0.0
     */
    public PageVO() {
        data = new ArrayList<>();
    }

    /**
     * 构造器
     *
     * @param baseEnum 响应枚举
     * @param detail   分页详情数据
     * @param data     分页数据
     * @since 1.0.0
     */
    public PageVO(BaseEnum<String> baseEnum, PageDetailVO detail, Collection<T> data) {
        super(baseEnum);
        this.detail = detail;
        this.data = data;
    }

    /**
     * 构造器
     *
     * @param baseEnum 响应枚举
     * @param userTip  用户提示信息
     * @param detail   分页详情数据
     * @param data     分页数据
     * @since 1.0.0
     */
    public PageVO(BaseEnum<String> baseEnum, String userTip, PageDetailVO detail, Collection<T> data) {
        super(baseEnum, userTip);
        this.detail = detail;
        this.data = data;
    }

    /**
     * 构造器
     *
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     * @param detail       分页详情数据
     * @param data         分页数据
     * @since 1.0.0
     */
    public PageVO(String errorCode, String errorMessage, PageDetailVO detail, Collection<T> data) {
        super(errorCode, errorMessage);
        this.detail = detail;
        this.data = data;
    }

    /**
     * 构造器
     *
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     * @param userTip      用户提示信息
     * @param detail       分页详情数据
     * @param data         分页数据
     * @since 1.0.0
     */
    public PageVO(String errorCode, String errorMessage, String userTip, PageDetailVO detail, Collection<T> data) {
        super(errorCode, errorMessage, userTip);
        this.detail = detail;
        this.data = data;
    }
}
