package cn.krismile.boot.framework.autoconfigure.cache;

import cn.krismile.boot.framework.autoconfigure.cache.properties.KsCacheProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.cache.CacheAutoConfiguration;
import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.CacheAspectSupport;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.cache.CacheKeyPrefix;

/**
 * Cache自动配置
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Configuration(proxyBeanMethods = false)
@AutoConfigureBefore(CacheAutoConfiguration.class)
@ConditionalOnClass(CacheManager.class)
@ConditionalOnBean(CacheAspectSupport.class)
@EnableConfigurationProperties({
        CacheProperties.class,
        KsCacheProperties.class
})
@Import({
        KsRedisCacheConfiguration.class
})
public class KsCacheAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public KeyGenerator keyGenerator(ObjectProvider<ObjectMapper> objectMapperProvider) {
        return (target, method, params) -> {
            String paramString;
            try {
                paramString = objectMapperProvider.getIfAvailable(ObjectMapper::new)
                        .writeValueAsString(params).replaceAll(":", ";");
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
            return target.getClass().getSimpleName()
                    + CacheKeyPrefix.SEPARATOR
                    + method.getName()
                    + CacheKeyPrefix.SEPARATOR
                    + paramString;
        };
    }
}
