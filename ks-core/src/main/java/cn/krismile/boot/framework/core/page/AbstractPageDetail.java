package cn.krismile.boot.framework.core.page;

import cn.krismile.boot.framework.core.query.PageQuery;
import cn.krismile.boot.framework.core.response.vo.PageDetailVO;

import java.io.Serializable;
import java.util.Collection;

/**
 * 分页参数抽象类, 定义了默认的分页参数
 *
 * <p>框架默认提供了实现类, 如不满足使用需要可以自行继承该类:
 * <ul>
 *     <li><b>{@link DefaultListPage}</b> - 默认分页对象, 其分页数据为 {@link java.util.List}</li>
 *     <li><b>{@link DefaultSetPage}</b> - 默认分页对象, 其分页数据为 {@link java.util.Set}</li>
 * </ul>
 *
 * @author JiYinchuan
 * @see Pageable
 * @see DefaultListPage
 * @see DefaultSetPage
 * @since 1.0.0
 */
public abstract class AbstractPageDetail<T extends Collection<?>> implements Pageable<T>, Serializable {

    private static final long serialVersionUID = -7894974711113638539L;

    /**
     * 页码
     */
    private Long pageNo;

    /**
     * 每页数量
     */
    private Long pageSize;

    /**
     * 数据总条数
     */
    private Long totalCount;

    /**
     * 构造器
     *
     * @since 1.0.0
     */
    protected AbstractPageDetail() {
        this.pageNo = DEFAULT_PAGE_NO;
        this.pageSize = DEFAULT_PAGE_SIZE;
        this.totalCount = DEFAULT_TOTAL_COUNT;
    }

    /**
     * 构造器
     *
     * @param pageNo   页码
     * @param pageSize 每页数量
     * @since 1.0.0
     */
    protected AbstractPageDetail(long pageNo, long pageSize) {
        this();
        setPageSize(pageSize);
        setPageNo(pageNo);
    }

    /**
     * 构造器
     *
     * @param pageNo     页码
     * @param pageSize   每页数量
     * @param totalCount 数据总条数
     * @since 1.0.0
     */
    protected AbstractPageDetail(long pageNo, long pageSize, long totalCount) {
        this();
        setTotalCount(totalCount);
        setPageSize(pageSize);
        setPageNo(pageNo);
    }

    /**
     * 构造器
     *
     * @param pageQuery 分页查询对象
     * @since 1.0.0
     */
    protected AbstractPageDetail(PageQuery pageQuery) {
        this();
        setPageSize(pageQuery.getPageSize());
        setPageNo(pageQuery.getPageNo());
    }

    /**
     * 构造器
     *
     * @param pageQuery  分页查询对象
     * @param totalCount 数据总条数
     * @since 1.0.0
     */
    protected AbstractPageDetail(PageQuery pageQuery, long totalCount) {
        this();
        setTotalCount(totalCount);
        setPageSize(pageQuery.getPageSize());
        setPageNo(pageQuery.getPageNo());
    }

    /**
     * 构造器
     *
     * @param pageDetailVO 分页详情VO
     * @since 1.0.0
     */
    protected AbstractPageDetail(PageDetailVO pageDetailVO) {
        this();
        setTotalCount(pageDetailVO.getTotalCount());
        setPageSize(pageDetailVO.getPageSize());
        setPageNo(pageDetailVO.getPageNo());
    }

    @Override
    public long getTotalCount() {
        return totalCount;
    }

    @Override
    public long getPageSize() {
        return pageSize;
    }

    @Override
    public long getPageNo() {
        return pageNo;
    }

    @Override
    public long getTotalPage() {
        long totalPage = (totalCount / pageSize);
        if (totalPage == 0 || totalCount % pageSize != 0) {
            totalPage++;
        }
        return totalPage;
    }

    @Override
    public boolean isFirstPage() {
        return pageNo <= 1;
    }

    @Override
    public boolean isLastPage() {
        return pageNo >= getTotalPage();
    }

    @Override
    public long getNextPage() {
        if (isLastPage()) {
            return pageNo;
        } else {
            return pageNo + 1;
        }
    }

    @Override
    public long getPrePage() {
        if (isFirstPage()) {
            return pageNo;
        } else {
            return pageNo - 1;
        }
    }

    /**
     * 设置数据总条数
     *
     * @param totalCount 数据总条数
     * @return 当前实例
     * @since 1.0.0
     */
    public AbstractPageDetail<T> setTotalCount(long totalCount) {
        this.totalCount = totalCount;
        adjustPageNo();
        return this;
    }

    /**
     * 设置每页数量
     *
     * @param pageSize 每页数量
     * @return 当前实例
     * @since 1.0.0
     */
    public AbstractPageDetail<T> setPageSize(long pageSize) {
        if (pageSize < 1) {
            this.pageSize = DEFAULT_PAGE_SIZE;
        } else {
            this.pageSize = Math.min(pageSize, DEFAULT_MAX_PAGE_SIZE);
        }
        adjustPageNo();
        return this;
    }

    /**
     * 设置页码
     *
     * @param pageNo 分页页码
     * @return 当前实例
     * @since 1.0.0
     */
    public AbstractPageDetail<T> setPageNo(long pageNo) {
        this.pageNo = Math.max(pageNo, DEFAULT_PAGE_NO);
        adjustPageNo();
        return this;
    }

    /**
     * 调整页码，使不超过最大页数
     *
     * @since 1.0.0
     */
    private void adjustPageNo() {
        if (pageNo == 1) {
            return;
        }
        long totalPage = getTotalPage();
        if (pageNo > totalPage) {
            pageNo = totalPage;
        }
    }

    @Override
    public String toString() {
        return "AbstractPageDetail{" +
                "pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", totalCount=" + totalCount +
                '}';
    }
}
