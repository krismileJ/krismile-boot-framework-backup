package cn.krismile.boot.framework.autoconfigure.web;

import cn.krismile.boot.framework.autoconfigure.AbstractInitListener;
import cn.krismile.boot.framework.autoconfigure.web.converter.*;
import cn.krismile.boot.framework.autoconfigure.web.properties.KsWebProperties;
import cn.krismile.boot.framework.autoconfigure.web.properties.children.KsJacksonProperties;
import cn.krismile.boot.framework.autoconfigure.web.properties.children.KsResponseProperties;
import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.boot.framework.core.constant.KrismileConstant;
import cn.krismile.boot.framework.core.response.vo.BaseVO;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.autoconfigure.jackson.JacksonProperties;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 上下文自动配置类
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Configuration(proxyBeanMethods = false)
@ConditionalOnWebApplication
@AutoConfigureAfter(WebMvcAutoConfiguration.class)
@EnableConfigurationProperties({
        KsWebProperties.class,
        KsResponseProperties.class,
        KsJacksonProperties.class
})
@Import(KsWebAutoConfiguration.InitListener.class)
public class KsWebAutoConfiguration {

    @Bean(KrismileConstant.KRISMILE_ABBREVIATION + "WebMvcConfigurer")
    public WebMvcConfigurer webMvcConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addFormatters(@NonNull FormatterRegistry registry) {
                registry.addConverterFactory(new EnumConverterFactory());
                registry.addConverter(new DateConverter());
                registry.addConverter(new LocalDateTimeConverter());
                registry.addConverter(new LocalDateConverter());
                registry.addConverter(new LocalTimeConverter());
            }
        };
    }

    /**
     * Jackson自动配置类
     *
     * @author JiYinchuan
     * @since 1.0.0
     */
    @Configuration(proxyBeanMethods = false)
    @ConditionalOnProperty(
            prefix = KsJacksonProperties.KEY,
            name = "enable",
            havingValue = "true",
            matchIfMissing = true
    )
    static class JacksonConfiguration {

        @Bean(KrismileConstant.KRISMILE_ABBREVIATION + "Jackson2ObjectMapperBuilderCustomizer")
        public Jackson2ObjectMapperBuilderCustomizer jackson2ObjectMapperBuilderCustomizer(
                JacksonProperties jacksonProperties) {
            return new JacksonObjectMapperBuilder(jacksonProperties);
        }
    }

    /**
     * 初始化配置监听器, 在实例准备就绪时将所有自定义配置覆盖自带配置
     *
     * @author JiYinchuan
     * @since 1.0.0
     */
    static class InitListener extends AbstractInitListener {

        /**
         * Web配置文件
         */
        private final KsWebProperties ksWebProperties;

        /**
         * 构造器
         *
         * @param ksWebProperties Web配置文件
         * @since 1.0.0
         */
        public InitListener(KsWebProperties ksWebProperties) {
            this.ksWebProperties = ksWebProperties;
        }

        @Override
        public void onApplicationEvent(@NonNull ApplicationReadyEvent event) {
            // Response配置文件
            KsResponseProperties ksResponseProperties = ksWebProperties.getResponse();
            BaseVO.DEFAULT_SUCCESS_USER_TIP = ksResponseProperties.getDefaultSuccessUserTip();
            BaseVO.DEFAULT_ERROR_USER_TIP = ksResponseProperties.getDefaultErrorUserTip();
        }
    }
}
