package cn.krismile.persagy.webstack.function.business.service.impl;

import cn.krismile.boot.framework.core.exception.client.RequestServerException;
import cn.krismile.boot.framework.mybatisplus.service.BaseServiceImpl;
import cn.krismile.persagy.webstack.function.business.mapper.BizRelNavWebsiteSysAdminMapper;
import cn.krismile.persagy.webstack.function.business.service.BizNavService;
import cn.krismile.persagy.webstack.function.business.service.BizRelNavWebsiteSysAdminService;
import cn.krismile.persagy.webstack.function.business.service.BizWebsiteService;
import cn.krismile.persagy.webstack.pojo.context.UserInfoContext;
import cn.krismile.persagy.webstack.pojo.domain.BizRelNavWebsiteSysAdminDO;
import org.springframework.context.annotation.Lazy;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 业务-导航-站点-用户关联表 ServiceImpl
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Service
public class BizRelNavWebsiteSysAdminServiceImpl extends BaseServiceImpl<BizRelNavWebsiteSysAdminMapper, BizRelNavWebsiteSysAdminDO> implements BizRelNavWebsiteSysAdminService {

    @Resource
    @Lazy
    private BizNavService navService;
    @Resource
    @Lazy
    private BizWebsiteService websiteService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean relation(@NonNull Long navId, @NonNull Long websiteId) throws RequestServerException {
        checkExists(navService.isExistsByIdValidate(navId));
        checkExists(websiteService.isExistsByIdValidate(websiteId));

        BizRelNavWebsiteSysAdminDO addData = new BizRelNavWebsiteSysAdminDO();
        addData.setRelNavId(navId);
        addData.setRelWebsiteId(websiteId);
        addData.setRelSysAdminId(UserInfoContext.userId());
        return saveValidate(addData);
    }
}
