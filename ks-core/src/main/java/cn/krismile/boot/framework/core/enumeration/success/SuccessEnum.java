package cn.krismile.boot.framework.core.enumeration.success;

import com.fasterxml.jackson.annotation.JsonCreator;
import cn.krismile.boot.framework.core.enumeration.BaseEnum;

/**
 * 成功枚举
 *
 * @author JiYinchuan
 * @see cn.krismile.boot.framework.core.response.ResultVO
 * @since 1.0.0
 */
public enum SuccessEnum implements BaseEnum<String> {

    /**
     * 一切 ok
     */
    OK("00000", "OK");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    SuccessEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static SuccessEnum analyze(String value) {
        return BaseEnum.analyze(value, SuccessEnum.class);
    }
}
