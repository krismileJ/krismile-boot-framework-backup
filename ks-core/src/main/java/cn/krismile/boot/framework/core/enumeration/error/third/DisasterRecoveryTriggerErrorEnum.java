package cn.krismile.boot.framework.core.enumeration.error.third;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import cn.krismile.boot.framework.core.enumeration.error.client.RequestParamErrorEnum;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 第三方容灾系统被触发异常枚举
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public enum DisasterRecoveryTriggerErrorEnum implements BaseEnum<String> {

    /**
     * 第三方容灾系统被触发
     */
    THIRD_DISASTER_RECOVERY_TRIGGER("C0400", "第三方容灾系统被触发"),

    /**
     * 第三方系统限流
     */
    THIRD_FLOW_CONTROL("C0401", "第三方系统限流"),

    /**
     * 第三方功能降级
     */
    THIRD_DOWNGRADE("C0402", "第三方功能降级");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    DisasterRecoveryTriggerErrorEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static RequestParamErrorEnum analyze(String value) {
        return BaseEnum.analyze(value, RequestParamErrorEnum.class);
    }
}
