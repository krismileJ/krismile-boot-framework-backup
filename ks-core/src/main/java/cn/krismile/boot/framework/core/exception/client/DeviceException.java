package cn.krismile.boot.framework.core.exception.client;

import cn.krismile.boot.framework.core.enumeration.error.client.DeviceErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 用户隐私未授权自定义异常
 *
 * @author JiYinchuan
 * @see DeviceErrorEnum
 * @since 1.0.0
 */
public class DeviceException extends ApplicationException {

    private static final long serialVersionUID = 2932878351273362870L;

    public DeviceException(DeviceErrorEnum deviceErrorEnum) {
        super(deviceErrorEnum);
    }

    public DeviceException(DeviceErrorEnum deviceErrorEnum, String userTip) {
        super(deviceErrorEnum, userTip);
    }
}
