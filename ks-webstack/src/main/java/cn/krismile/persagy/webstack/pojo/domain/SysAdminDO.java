package cn.krismile.persagy.webstack.pojo.domain;

import cn.krismile.boot.framework.mybatisplus.domain.BaseDO;
import cn.krismile.persagy.webstack.enumeration.business.YesNoEnum;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 系统-用户表
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_admin")
@Schema(title = "SysAdminDO对象", description = "系统-用户表")
public class SysAdminDO extends BaseDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "用户帐号")
    @TableField("account")
    private String account;

    @Schema(description = "真实姓名")
    @TableField("real_name")
    private String realName;

    @Schema(description = "手机号码")
    @TableField("phone")
    private String phone;

    @Schema(description = "邮箱地址")
    @TableField("email")
    private String email;

    @Schema(description = "用户状态 [0: 禁用, 1: 启用]")
    @TableField("status")
    private YesNoEnum status;

    @Schema(description = "页面授权码")
    @TableField("web_code")
    private String webCode;

    @Schema(description = "是否为内置 [0: 否, 1: 是]")
    @TableField("is_internal")
    private Boolean internal;

    @Schema(description = "关联部门ID")
    @TableField("rel_department_id")
    private Long relDepartmentId;

}
