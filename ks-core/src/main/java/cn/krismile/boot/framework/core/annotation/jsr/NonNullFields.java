package cn.krismile.boot.framework.core.annotation.jsr;

import javax.annotation.Nonnull;
import javax.annotation.meta.TypeQualifierDefault;
import java.lang.annotation.*;

/**
 * 利用JSR-305元注释将Java中的nullable指示为具有JSR-305支持的通用工具
 *
 * @author JiYinchuan
 * @see NonNullApi
 * @see Nullable
 * @see NonNull
 * @since 1.0.0
 */
@Target(ElementType.PACKAGE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Nonnull
@TypeQualifierDefault(ElementType.FIELD)
public @interface NonNullFields {
}
