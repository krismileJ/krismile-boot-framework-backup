package cn.krismile.boot.framework.core.exception;

import cn.krismile.boot.framework.core.annotation.jsr.Nullable;
import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import cn.krismile.boot.framework.core.util.inner.StringUtils;

/**
 * 第三方自定义异常
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public class ThirdPartyException extends RuntimeException {

    private static final long serialVersionUID = 5662993814979255693L;

    /**
     * 错误码
     */
    protected final Object errorCode;

    /**
     * 错误信息
     */
    protected final String errorMessage;

    /**
     * 错误信息
     */
    protected final String userTip;

    /**
     * 构造器
     *
     * @param baseEnum 枚举基类
     * @since 1.0.0
     */
    public ThirdPartyException(BaseEnum<Object> baseEnum) {
        super("第三方服务异常: [errorCode: " + baseEnum.value() + ", errorMessage: " + baseEnum.getReasonPhrase() + "]");
        this.errorCode = baseEnum.value();
        this.errorMessage = baseEnum.getReasonPhrase();
        this.userTip = ApplicationException.DEFAULT_ERROR_USER_TIP;
    }

    /**
     * 构造器
     *
     * @param baseEnum 枚举基类
     * @param userTip  用户提示信息
     * @since 1.0.0
     */
    public ThirdPartyException(BaseEnum<Object> baseEnum, @Nullable String userTip) {
        super("第三方服务异常: [errorCode: " + baseEnum.value() + ", errorMessage: " + baseEnum.getReasonPhrase() + "]");
        this.errorCode = baseEnum.value();
        this.errorMessage = baseEnum.getReasonPhrase();
        this.userTip = StringUtils.defaultString(userTip, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 构造器
     *
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     * @since 1.0.0
     */
    public ThirdPartyException(Object errorCode, String errorMessage) {
        super("第三方服务异常: [errorCode: " + errorCode + ", errorMessage: " + errorMessage + "]");
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.userTip = ApplicationException.DEFAULT_ERROR_USER_TIP;
    }

    /**
     * 构造器
     *
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     * @param userTip      用户提示信息
     * @since 1.0.0
     */
    public ThirdPartyException(Object errorCode, String errorMessage, @Nullable String userTip) {
        super("第三方服务异常: [errorCode: " + errorCode + ", errorMessage: " + errorMessage + "]");
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.userTip = StringUtils.defaultString(userTip, ApplicationException.DEFAULT_ERROR_USER_TIP);
    }

    /**
     * 获取错误码
     *
     * @return 错误码
     * @since 1.0.0
     */
    public Object getErrorCode() {
        return errorCode;
    }

    /**
     * 获取错误信息
     *
     * @return 错误信息
     * @since 1.0.0
     */
    public Object getErrorMessage() {
        return errorMessage;
    }
}
