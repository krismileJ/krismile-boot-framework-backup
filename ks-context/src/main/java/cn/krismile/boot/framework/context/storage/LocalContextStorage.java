package cn.krismile.boot.framework.context.storage;

import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.boot.framework.core.annotation.jsr.Nullable;
import cn.krismile.boot.framework.core.constant.key.LogKey;
import cn.krismile.boot.framework.core.enumeration.error.client.RequestServerErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;
import cn.krismile.boot.framework.core.exception.client.RequestServerException;
import cn.krismile.boot.framework.core.util.common.ThreadLocalUtils;
import cn.krismile.boot.framework.core.util.common.UUIDUtils;
import com.alibaba.ttl.TransmittableThreadLocal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * 当前线程唯一变量储存
 *
 * <p><a href="https://github.com/alibaba/p3c">摘选自Java开发手册(黄山版7.6)</a>: 必须回收自定义的 [ThreadLocal] 变量,
 * 尤其在线程池场景下, 线程经常会被复用, 如果不清理自定义的 [ThreadLocal] 变量, 可能会影响后续业务逻辑和造成内存泄露等问题.
 * 尽量在代理中使用 [try-finally] 块进行回收.
 * 正例:
 * <pre>{@code
 *     objectThreadLocal.set(userInfo);
 *     try {
 *         // ...
 *     } finally {
 *         objectThreadLocal.remove();
 *     }
 * }</pre>
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@SuppressWarnings("unchecked")
public class LocalContextStorage {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocalContextStorage.class);

    private static final String LOG_TAG = "KS-Context-LocalStorage";

    private static final TransmittableThreadLocal<Map<String, Object>> LOCAL = new TransmittableThreadLocal<>();

    /**
     * 获取当前线程唯一ID
     *
     * @param isReset 是否重置
     * @return ID
     * @since 1.0.0
     */
    @NonNull
    public static String trackId(boolean isReset) {
        final String loggerThreadTtlKey = LogKey.TRACK_ID;
        if (isReset) {
            return set(loggerThreadTtlKey, UUIDUtils.random());
        }
        return (String) ThreadLocalUtils.getIfAbsentAndSet(LOCAL, loggerThreadTtlKey,
                (Supplier<String>) UUIDUtils::random);
    }

    /**
     * 设置值到源对象中
     *
     * @param key   键, 不能为空
     * @param value 值, 不能为空
     * @param <T>   值泛型
     * @return 设置的值
     * @since 1.0.0
     */
    @NonNull
    public static <T> T set(@NonNull final String key, @NonNull final T value) {
        return (T) ThreadLocalUtils.setAndGet(LOCAL, key, value);
    }

    /**
     * 从源对象中获取值
     *
     * @param key 键, 不能为空
     * @param <T> 值泛型
     * @return 获取到的 [value] 值, 未获取到值时返回 [null]
     * @since 1.0.0
     */
    @Nullable
    public static <T> T get(@NonNull final String key) {
        return (T) ThreadLocalUtils.get(LOCAL, key);
    }

    /**
     * 从源对象中获取值
     *
     * @param key 键, 不能为空
     * @param cls 值Class
     * @param <T> 值泛型
     * @return 获取到的 [value] 值, 未获取到值时返回 [null]
     * @since 1.0.0
     */
    @Nullable
    public static <T> T get(@NonNull final String key, @NonNull final Class<T> cls) {
        return cls.cast(get(key));
    }

    /**
     * 从源对象中获取值
     *
     * @param key 键, 不能为空
     * @param <T> 值泛型
     * @return 获取到的 [value] 值, 未获取到值时返回 [null]
     * @since 1.0.0
     */
    public static <T> Optional<T> getOpt(@NonNull final String key) {
        return Optional.ofNullable(get(key));
    }

    /**
     * 从源对象中获取值
     *
     * @param key 键, 不能为空
     * @param cls 值Class
     * @param <T> 值泛型
     * @return 获取到的 [value] 值, 未获取到值时返回 [null]
     * @since 1.0.0
     */
    public static <T> Optional<T> getOpt(@NonNull final String key, @NonNull final Class<T> cls) {
        return Optional.ofNullable(get(key, cls));
    }

    /**
     * 从源对象中获取值
     *
     * @param key 键, 不能为空
     * @return 获取到的 [value] 值, 未获取到值时抛出异常
     * @throws RequestServerException 未获取到值
     * @since 1.0.0
     */
    @NonNull
    public static <T> T getThrow(@NonNull final String key) throws RequestServerException {
        return getThrow(key, () -> new RequestServerException(RequestServerErrorEnum.SERVICE_ERROR));
    }

    /**
     * 从源对象中获取值
     *
     * @param key 键, 不能为空
     * @param cls 值Class
     * @return 获取到的 [value] 值, 未获取到值时抛出异常
     * @throws RequestServerException 未获取到值
     * @since 1.0.0
     */
    @NonNull
    public static <T> T getThrow(@NonNull final String key, @NonNull final Class<T> cls) throws RequestServerException {
        return getThrow(key, cls, () -> new RequestServerException(RequestServerErrorEnum.SERVICE_ERROR));
    }

    /**
     * 从源对象中获取值
     *
     * @param key 键, 不能为空
     * @param e   自定义异常
     * @return 获取到的 [value] 值, 未获取到值时抛出异常
     * @throws ApplicationException 未获取到值
     * @since 1.0.0
     */
    @NonNull
    public static <T> T getThrow(@NonNull final String key, @NonNull Supplier<ApplicationException> e) throws ApplicationException {
        Optional<T> valueOpt = getOpt(key);
        if (valueOpt.isPresent()) {
            return valueOpt.get();
        }
        throw e.get();
    }

    /**
     * 从源对象中获取值
     *
     * @param key 键, 不能为空
     * @param cls 值Class
     * @param e   自定义异常, 不能为空
     * @return 获取到的 [value] 值, 未获取到值时抛出异常
     * @throws RequestServerException 未获取到值
     * @since 1.0.0
     */
    @NonNull
    public static <T> T getThrow(@NonNull final String key, @NonNull final Class<T> cls,
                                 @NonNull Supplier<ApplicationException> e) throws ApplicationException {
        Optional<T> valueOpt = getOpt(key);
        if (valueOpt.isPresent()) {
            return cls.cast(valueOpt.get());
        }
        throw e.get();
    }

    /**
     * 判断源对象是否包括某个值
     *
     * @param key 键, 可为空
     * @return 源对象中是否包含 [key] 对应的值
     * @since 1.0.0
     */
    public static boolean contains(@NonNull final String key) {
        return ThreadLocalUtils.contains(LOCAL, key);
    }

    /**
     * 日志打印源对象中所有的数据
     *
     * @since 1.0.0
     */
    public static void printAllData() {
        LOGGER.info("------------------------- 开始打印 [{}] 数据 -------------------------", LOG_TAG);
        ThreadLocalUtils.Execute.eachFunction(LOCAL,
                (key, value) -> LOGGER.info("[key: {}, value: {}]", key, value));
        LOGGER.info("------------------------- 结束打印 [{}}] 数据 -------------------------", LOG_TAG);
    }

    /**
     * 删除源对象中的值
     *
     * @param key 键, 可为空
     */
    public static void remove(@NonNull final String key) {
        ThreadLocalUtils.remove(LOCAL, key);
    }

    /**
     * 清空源对象中的所有值
     */
    public static void removeAll() {
        ThreadLocalUtils.removeAll(LOCAL);
    }

    /**
     * 移除当前线程存储的Map
     *
     * <p>因为 {@link ThreadLocal} 底层使用的内部类 {@code ThreadLocalMap} 实现的, 生命周期为当前线程,
     * 所以不执行此方法当线程终止后 {@code ThreadLocalMap} 中的值会被JVM垃圾回收,
     * 但推荐在不需要使用的时候显性的执行此方法, 便于理解
     *
     * @since 1.0.0
     */
    public static void clear() {
        LOCAL.remove();
    }
}
