package cn.krismile.boot.framework.core.exception.client;

import cn.krismile.boot.framework.core.enumeration.error.client.RequestParamErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 请求参数自定义异常
 *
 * @author JiYinchuan
 * @see RequestParamErrorEnum
 * @since 1.0.0
 */
public class RequestParamException extends ApplicationException {

    private static final long serialVersionUID = -2785966243623208253L;

    public RequestParamException(RequestParamErrorEnum requestParamErrorEnum) {
        super(requestParamErrorEnum);
    }

    public RequestParamException(RequestParamErrorEnum requestParamErrorEnum, String userTip) {
        super(requestParamErrorEnum, userTip);
    }
}
