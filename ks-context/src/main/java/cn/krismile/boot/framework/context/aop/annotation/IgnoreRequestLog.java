package cn.krismile.boot.framework.context.aop.annotation;

import cn.krismile.boot.framework.context.aop.RequestLogAop;

import java.lang.annotation.*;

/**
 * 忽略请求日志AOP记录
 *
 * <p>当在类或方法上标注该注解时在进行日志打印时将判断是否忽略当前类或方法的日志打印
 *
 * @author JiYinchuan
 * @see RequestLogAop
 * @since 1.0.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
public @interface IgnoreRequestLog {
}
