package cn.krismile.persagy.webstack.function.system.service;

import cn.krismile.persagy.webstack.pojo.domain.SysMenuDO;
import cn.krismile.boot.framework.mybatisplus.service.BaseService;

/**
 * 系统-菜单表 Service
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface SysMenuService extends BaseService<SysMenuDO> {

}
