package cn.krismile.persagy.webstack.function.system.service.impl;

import cn.krismile.persagy.webstack.pojo.domain.SysMenuDO;
import cn.krismile.persagy.webstack.function.system.mapper.SysMenuMapper;
import cn.krismile.persagy.webstack.function.system.service.SysMenuService;
import cn.krismile.boot.framework.mybatisplus.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 系统-菜单表 ServiceImpl
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Service
public class SysMenuServiceImpl extends BaseServiceImpl<SysMenuMapper, SysMenuDO> implements SysMenuService {

}
