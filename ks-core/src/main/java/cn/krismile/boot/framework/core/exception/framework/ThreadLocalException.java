package cn.krismile.boot.framework.core.exception.framework;

import cn.krismile.boot.framework.core.enumeration.error.framework.ThreadLocalErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * ThreadLocal自定义异常
 *
 * @author JiYinchuan
 * @see ThreadLocalErrorEnum
 * @since 1.0.0
 */
public class ThreadLocalException extends ApplicationException {

    private static final long serialVersionUID = -8100022674299473446L;

    public ThreadLocalException(ThreadLocalErrorEnum threadLocalErrorEnum) {
        super(threadLocalErrorEnum);
    }

    public ThreadLocalException(ThreadLocalErrorEnum threadLocalErrorEnum, String userTip) {
        super(threadLocalErrorEnum, userTip);
    }
}
