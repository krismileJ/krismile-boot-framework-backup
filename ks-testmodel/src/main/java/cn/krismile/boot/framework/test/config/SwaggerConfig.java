// package cn.krismile.boot.framework.test.config;
//
// import io.swagger.v3.oas.annotations.Operation;
// import io.swagger.v3.oas.models.OpenAPI;
// import io.swagger.v3.oas.models.SpecVersion;
// import io.swagger.v3.oas.models.info.Info;
// import io.swagger.v3.oas.models.media.StringSchema;
// import io.swagger.v3.oas.models.parameters.HeaderParameter;
// import org.springdoc.core.GroupedOpenApi;
// import org.springframework.boot.SpringBootConfiguration;
// import org.springframework.context.annotation.Bean;
//
// /**
//  * SwaggerConfig
//  *
//  * @author JiYinchuan
//  * @since 1.0.0
//  */
// @SpringBootConfiguration
// public class SwaggerConfig {
//
//     @Bean
//     public OpenAPI openApi() {
//         return new OpenAPI(SpecVersion.V30).info(new Info()
//                 .title("能耗异常诊断")
//                 .description("能耗异常诊断 ApiDoc")
//                 .version("v4.3.0"));
//     }
//
//     @Bean
//     public GroupedOpenApi groupedOpenApi() {
//         return GroupedOpenApi.builder()
//                 .group("Open")
//                 .pathsToMatch("/**")
//                 .addOpenApiMethodFilter(method -> method.isAnnotationPresent(Operation.class))
//                 .addOperationCustomizer((operation, handlerMethod) -> operation.addParametersItem(new HeaderParameter()
//                         .name("groupCode").description("集团Code").schema(new StringSchema()._default("BR").name("groupCode").description("集团Code"))))
//                 .build();
//     }
// }
