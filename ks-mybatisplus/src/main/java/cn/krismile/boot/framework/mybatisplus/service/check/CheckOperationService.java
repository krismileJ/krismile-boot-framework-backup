package cn.krismile.boot.framework.mybatisplus.service.check;

import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.boot.framework.core.annotation.jsr.Nullable;
import cn.krismile.boot.framework.core.exception.ApplicationException;
import cn.krismile.boot.framework.mybatisplus.enumeration.error.MybatisServiceErrorEnum;
import cn.krismile.boot.framework.mybatisplus.exception.MybatisServiceException;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * 操作检查Service
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface CheckOperationService extends BaseCheckService {

    /**
     * 默认操作异常对象
     */
    Function<String, MybatisServiceException> DEFAULT_OPERATION_ERROR_EXCEPTION =
            userTip -> new MybatisServiceException(MybatisServiceErrorEnum.OPERATION_ERROR, userTip);

    /**
     * 通用结果检查
     *
     * @param operateResult 操作结果
     * @throws MybatisServiceException 执行操作失败
     * @since 1.0.0
     */
    default void checkOperation(@Nullable Boolean operateResult)
            throws MybatisServiceException {
        dynamicThrowError(operateResult, () -> DEFAULT_OPERATION_ERROR_EXCEPTION.apply(null));
    }

    /**
     * 通用结果检查
     *
     * @param operateResult 操作结果
     * @param userTip       默认用户提示信息
     * @throws MybatisServiceException 执行操作失败
     * @since 1.0.0
     */
    default void checkOperation(@Nullable Boolean operateResult,
                       @Nullable String userTip)
            throws MybatisServiceException {
        dynamicThrowError(operateResult, () -> DEFAULT_OPERATION_ERROR_EXCEPTION.apply(userTip));
    }

    /**
     * 通用结果检查
     *
     * @param operateResult 操作结果
     * @param e             当结果为 {@code null} 或 {@code false} 时抛出的结果, 不能为空
     * @throws ApplicationException 自定义异常
     * @since 1.0.0
     */
    default void checkOperation(@Nullable Boolean operateResult,
                       @NonNull Supplier<ApplicationException> e)
            throws ApplicationException {
        dynamicThrowError(operateResult, e);
    }
}
