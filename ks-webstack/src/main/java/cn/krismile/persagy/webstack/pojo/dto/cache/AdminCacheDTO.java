package cn.krismile.persagy.webstack.pojo.dto.cache;

import cn.krismile.persagy.webstack.enumeration.business.YesNoEnum;
import lombok.Data;

/**
 * 系统用户缓存DTO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
public class AdminCacheDTO {

    /**
     * 系统用户ID
     */
    private Long adminId;

    /**
     * 真实姓名
     */
    private String realName;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 用户状态
     */
    private YesNoEnum status;

    // ------------------------- 附加字段 -------------------------

    /**
     * 完整部门名称
     */
    private String fullDepartmentName;



}
