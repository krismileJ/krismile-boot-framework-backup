package cn.krismile.boot.framework.context.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 请求结果数据信息封装
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@Accessors(chain = true)
public class RequestInfo implements Serializable {

    private static final long serialVersionUID = 5793565130133348165L;

    /**
     * 线程ID
     */
    private String threadId;

    /**
     * 线程名称
     */
    private String threadName;

    /**
     * 请求URI
     */
    private String uri;

    /**
     * 客户端真实请求IP地址
     */
    private String clientIp;

    /**
     * User-Agent
     */
    private String userAgent;

    /**
     * 操作系统
     */
    private String os;

    /**
     * 浏览器
     */
    private String browser;

    /**
     * 请求方法类型
     */
    private String methodType;

    /**
     * 完整请求方法
     */
    private String fullMethodName;

    /**
     * 请求方法中的参数
     */
    private Object[] requestMethodArgs;

}
