package cn.krismile.persagy.webstack.controller;

import io.swagger.v3.oas.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import cn.krismile.persagy.webstack.function.system.service.SysMenuService;
import cn.krismile.persagy.webstack.pojo.domain.SysMenuDO;
import cn.krismile.boot.framework.core.query.PageQuery ;
import cn.krismile.boot.framework.core.response.ResultVO;
import cn.krismile.boot.framework.core.response.vo.BaseVO;
import cn.krismile.boot.framework.core.response.vo.PageVO;
import cn.krismile.boot.framework.core.response.vo.SingleVO;

import java.util.List;

/**
 * 系统-菜单表
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Tag(name = "[XXX] 系统-菜单表")
@RestController
@RequestMapping("/webstack/sysMenuDO")
public class SysMenuController {

    @Autowired
    private SysMenuService sysMenuService;

    @Operation(summary = "[查询] 单条数据")
    @GetMapping("/{id}")
    public SingleVO<SysMenuDO> get(@PathVariable("id") Long id) {
        return ResultVO.okSingle(sysMenuService.getByIdValidate(id));
    }

    @Operation(summary = "[查询] 分页数据")
    @GetMapping("/page")
    public PageVO<SysMenuDO> page(@ModelAttribute PageQuery pageQuery) {
        return ResultVO.okPage(sysMenuService.pageValidate(pageQuery));
    }

    @Operation(summary = "[新增] 单条数据")
    @PostMapping
    public BaseVO add(@RequestBody @Validated SysMenuDO sysMenu) {
        return ResultVO.ok(() -> sysMenuService.saveValidate(sysMenu));
    }

    @Operation(summary = "[更新] 单条数据")
    @PutMapping
    public BaseVO edit(@RequestBody @Validated SysMenuDO sysMenu) {
        return ResultVO.ok(() -> sysMenuService.updateByIdValidate(sysMenu));
    }

    @Operation(summary = "[删除] 批量数据")
    @DeleteMapping
    public BaseVO del(@RequestBody List<Long> ids) {
        return ResultVO.ok(() -> sysMenuService.removeByIdValidate(ids));
    }
}

