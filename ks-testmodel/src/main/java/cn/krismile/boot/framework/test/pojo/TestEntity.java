package cn.krismile.boot.framework.test.pojo;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * TODO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */

@Component
@Lazy
public class TestEntity {

    private String id;

    private String name;

    public TestEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
