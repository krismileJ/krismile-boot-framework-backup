package cn.krismile.boot.framework.test.service.impl;

import cn.krismile.boot.framework.test.pojo.TestDTO;
import cn.krismile.boot.framework.test.service.BaseService;
import cn.krismile.boot.framework.test.service.TestService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * TODO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Service
public class TestServiceImpl extends BaseService<TestDTO> implements TestService<TestDTO> {
    @Override
    public void test(TestDTO testDTO, List<Object> list) {
        query(testDTO);
//        return "This is test service.";
    }

    @Override
    public void query(TestDTO testDTO) {

    }
}
