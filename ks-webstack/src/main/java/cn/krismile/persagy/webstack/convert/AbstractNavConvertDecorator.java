package cn.krismile.persagy.webstack.convert;

import cn.krismile.persagy.webstack.pojo.domain.BizNavDO;
import cn.krismile.persagy.webstack.pojo.dto.NavDTO;
import org.apache.commons.collections4.CollectionUtils;

import java.util.*;

/**
 * 导航栏自定义装饰器
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public abstract class AbstractNavConvertDecorator implements NavConvert {

    @Override
    public List<NavDTO> toDto(List<BizNavDO> source) {
        List<NavDTO> result = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(source)) {
            // 非顶级导航栏
            List<BizNavDO> itemNavs = new ArrayList<>();

            // 添加顶级导航栏至结果集中, 缓存非顶级导航栏
            for (BizNavDO sourceItem : source) {
                Long parentId = sourceItem.getParentId();
                if (parentId == BizNavDO.TOP_PARENT_ID) {
                    result.add(toDto(sourceItem));
                } else {
                    itemNavs.add(sourceItem);
                }
            }
            // 匹配顶级导航栏与非顶级导航栏
            for (NavDTO topNav : result) {
                Long topNavId = topNav.getNavId();
                List<NavDTO> topNavChildren = topNav.getNavChildren();
                for (BizNavDO itemNav : itemNavs) {
                    Long itemParentId = itemNav.getParentId();
                    if (Objects.equals(topNavId, itemParentId)) {
                        topNavChildren.add(toDto(itemNav));
                    }
                }
            }
        }
        return result;
    }
}
