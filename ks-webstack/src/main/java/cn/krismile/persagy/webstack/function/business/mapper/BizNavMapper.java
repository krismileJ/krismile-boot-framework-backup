package cn.krismile.persagy.webstack.function.business.mapper;

import cn.krismile.persagy.webstack.enumeration.business.ResourceStatusEnum;
import cn.krismile.persagy.webstack.enumeration.business.ResourceTypeEnum;
import cn.krismile.persagy.webstack.pojo.domain.BizNavDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 业务-导航栏表 Mapper 接口
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Mapper
@Repository
public interface BizNavMapper extends BaseMapper<BizNavDO> {

    /**
     * 获取数据
     *
     * @param isLock 是否锁定
     * @param navId  导航栏ID
     * @return 数据
     * @since 1.0.0
     */
    BizNavDO get(@Param("isLock") boolean isLock, @NonNull @Param("navId") Long navId);

    /**
     * 获取数据列表
     *
     * @param sysAdminId 系统用户ID, 需要查询和用户相关信息时必传
     * @param navType    导航栏类型
     * @param navStatus  导航栏状态, 为 {@code null} 时默认查询 {@link ResourceStatusEnum#APPROVED} 状态
     * @return 数据列表
     * @since 1.0.0
     */
    List<BizNavDO> list(@Nullable @Param("sysAdminId") Long sysAdminId,
                        @Nullable @Param("navType") ResourceTypeEnum navType,
                        @Nullable @Param("navStatus") ResourceStatusEnum navStatus);

    /**
     * 指定用户导航栏列表查询
     *
     * @param sysAdminId 系统用户ID
     * @return 指定用户导航栏列表
     * @since 1.0.0
     */
    List<BizNavDO> listByAdmin(@NonNull @Param("sysAdminId") Long sysAdminId);

}
