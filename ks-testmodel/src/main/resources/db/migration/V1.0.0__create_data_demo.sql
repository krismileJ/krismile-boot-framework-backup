CREATE TABLE `demo`
(
    `id`          int          NOT NULL AUTO_INCREMENT,
    `create_time` datetime     NOT NULL,
    `update_time` datetime     NOT NULL,
    `name`        varchar(255) NOT NULL,
    `description`  varchar(255) NULL,
    PRIMARY KEY (`id`)
);