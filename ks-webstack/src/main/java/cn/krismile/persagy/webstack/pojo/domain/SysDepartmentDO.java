package cn.krismile.persagy.webstack.pojo.domain;

import cn.krismile.boot.framework.mybatisplus.domain.BaseDO;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 系统-部门表
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_department")
@Schema(title = "SysDepartmentDO对象", description = "系统-部门表")
public class SysDepartmentDO extends BaseDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "部门名称")
    @TableField("name")
    private String name;

    @Schema(description = "自关联上级部门ID")
    @TableField("parent_id")
    private Long parentId;

}
