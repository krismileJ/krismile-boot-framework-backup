package cn.krismile.boot.framework.autoconfigure.web.properties.children;

import cn.krismile.boot.framework.autoconfigure.web.properties.KsWebProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Jackson配置
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = KsJacksonProperties.KEY)
public class KsJacksonProperties {

    public static final String KEY = KsWebProperties.KEY + "." + "jackson";

    /**
     * 是否启用
     */
    private boolean enable = true;

}
