package cn.krismile.boot.framework.core.exception.third;

import cn.krismile.boot.framework.core.enumeration.error.third.MiddlewareServerErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 中间件服务出错自定义异常
 *
 * @author JiYinchuan
 * @see MiddlewareServerErrorEnum
 * @since 1.0.0
 */
public class MiddlewareServerException extends ApplicationException {

    private static final long serialVersionUID = -3773087373469431105L;

    public MiddlewareServerException(MiddlewareServerErrorEnum middlewareServerErrorEnum) {
        super(middlewareServerErrorEnum);
    }

    public MiddlewareServerException(MiddlewareServerErrorEnum middlewareServerErrorEnum, String userTip) {
        super(middlewareServerErrorEnum, userTip);
    }
}