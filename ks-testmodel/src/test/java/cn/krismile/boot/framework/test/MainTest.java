package cn.krismile.boot.framework.test;

import cn.hutool.core.math.MathUtil;
import com.alibaba.fastjson2.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ql.util.express.ExpressRunner;
import lombok.Data;

import java.math.BigDecimal;

/**
 * MainTest
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public class MainTest {

    private static final ExpressRunner runner = new ExpressRunner();

    public static void main(String[] args) throws Exception {
    //
    //     BigDecimal result = BigDecimal.valueOf(execute("1.167*(160.0023-140.0023)*178.14229999999998"))
    //             .add(BigDecimal.valueOf(execute("1.167*(116.33-110.0027)*129.9973")))
    //             .add(BigDecimal.valueOf(execute("1.167*(150.33-159.9977)*121.85770000000001")))
    //             .add(BigDecimal.valueOf(execute("1.167*(183.5427-189.9973)*170.0027")));
    //     System.out.println(result.multiply(new BigDecimal("0.25")).toPlainString());

        BigDecimal result = BigDecimal.valueOf(execute("1.167*(160.0023-140.0023)*178.14229999999998"))
                .add(BigDecimal.valueOf(execute("1.167*(109.9973-89.9973)*129.9973")))
                .add(BigDecimal.valueOf(execute("1.167*(60.002300000000005-40.002300000000005)*80.0023")))
                .add(BigDecimal.valueOf(execute("1.167*(9.9973+10.0027)*29.9973")));
        System.out.println(result.multiply(new BigDecimal("0.25")).toPlainString());

    }

    private static Double execute(String expressString) throws Exception {
        return (Double) runner.execute(expressString, null, null, true, false);
    }

    @Data
    public static class Tesla {

        private String aName;
        private String aFactory;
        private String vehicleType;
        private Double vehiclePrice;

    }
}
