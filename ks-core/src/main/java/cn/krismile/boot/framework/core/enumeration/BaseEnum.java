package cn.krismile.boot.framework.core.enumeration;

import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.boot.framework.core.annotation.jsr.Nullable;
import cn.krismile.boot.framework.core.exception.EnumIllegalArgumentException;
import cn.krismile.boot.framework.core.util.Assert;
import cn.krismile.boot.framework.core.util.inner.StringUtils;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.function.BiFunction;

/**
 * 全局枚举父接口
 *
 * <p>该类为全局枚举父接口, 为避免部分功能失效, 项目中所有的自定义枚举原则上都必须实现该接口
 * <p><b>Tip</b> - 该类提供了 {@link #additionalValue()} 和 {@link #additionalMapValue()} 用于提供附加值,
 * 开发者可选择性的重写这两个方法或任意一个方法, 便于获取每个枚举对象的附加参数值
 * <p>注意: 如果未重写方法直接调动附加值方法请注意判断 {@code null} 值, 避免出现空指针异常
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface BaseEnum<T> {

    /**
     * 默认解析异常
     */
    BiFunction<String, Object, EnumIllegalArgumentException> DEFAULT_ANALYZE_EXCEPTION = (enumName, analyzedValue) ->
            new EnumIllegalArgumentException("No matching [" + enumName + "] for [" + analyzedValue + "]");

    /**
     * 获取枚举值
     *
     * @return 枚举值
     * @since 1.0.0
     */
    @JsonValue
    T value();

    /**
     * 获取枚举信息
     *
     * @return 枚举信息
     * @since 1.0.0
     */
    String getReasonPhrase();


    /**
     * 附加值
     *
     * @return 附加值
     * @since 1.0.0
     */
    default String additionalValue() {
        return null;
    }

    /**
     * 附加值
     *
     * @return 附加值
     * @since 1.0.0
     */
    default Map<String, String> additionalMapValue() {
        return Collections.emptyMap();
    }

    /**
     * 解析枚举参数值
     *
     * <p>默认解析方法不支持动态解析功能, 如需要动态解析请参考 {@link #analyze(Object, Class, boolean, boolean)}
     *
     * @param value     参数值
     * @param enumClass 需要解析的枚举类
     * @param <E>       继承 {@link BaseEnum} 的枚举类型
     * @return 解析后的枚举对象
     * @throws EnumIllegalArgumentException 解析异常
     * @see #analyze(Object, Class, boolean, boolean)
     * @since 1.0.0
     */
    static <T, E extends BaseEnum<T>> E analyze(@Nullable T value,
                                                @NonNull Class<E> enumClass)
            throws EnumIllegalArgumentException {
        return analyze(value, enumClass, false, true);
    }

    /**
     * 解析枚举参数值
     *
     * @param value            参数值
     * @param enumClass        需要解析的枚举类
     * @param isDynamicAnalyze 是否动态解析, 为动态解析时可动态解析字符串与数字类型的映射
     * @param isNotFoundThrow  是否在没有找到匹配的值时抛出异常
     * @param <E>              继承 {@link BaseEnum} 的枚举类型
     * @return 解析后的枚举对象
     * @throws EnumIllegalArgumentException 解析异常
     * @since 1.0.0
     */
    @SuppressWarnings("unchecked")
    static <T, E extends BaseEnum<T>> E analyze(@Nullable T value,
                                                @NonNull Class<E> enumClass,
                                                boolean isDynamicAnalyze,
                                                boolean isNotFoundThrow)
            throws EnumIllegalArgumentException {
        Assert.notNull(enumClass, "EnumClass can not be null");
        if (!enumClass.isEnum()) {
            throw new EnumIllegalArgumentException(enumClass.getName() + " is not an Enum");
        }
        E enumValue = null;
        E[] enumConstants = enumClass.getEnumConstants();
        if (Objects.nonNull(enumConstants) && enumConstants.length > 0 && Objects.nonNull(value)) {
            // 参数值为字符串时需要现去除前后空格
            if (value instanceof String) {
                value = (T) ((String) value).trim();
            }
            for (E enumConstant : enumConstants) {
                T enumConstantValue = enumConstant.value();
                if (Objects.nonNull(enumConstantValue)) {
                    if (enumConstant.value().equals(value)) {
                        enumValue = enumConstant;
                        break;
                    }
                    if (isDynamicAnalyze) {
                        // 以下是为了兼容参数值为 String 时且枚举参数为 Number 类型时的兼容情况
                        if (value instanceof String && enumConstantValue instanceof Number) {
                            String tempValue = (String) value;
                            if (StringUtils.isParsable(tempValue)) {
                                String tempEnumConstant = String.valueOf(enumConstantValue);
                                if (tempEnumConstant.equals(tempValue)) {
                                    enumValue = enumConstant;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (isNotFoundThrow && enumValue == null) {
            throw DEFAULT_ANALYZE_EXCEPTION.apply(enumClass.getName(), value);
        }
        return enumValue;
    }
}
