package cn.krismile.persagy.webstack.pojo.domain;

import cn.krismile.boot.framework.mybatisplus.domain.BaseDO;
import cn.krismile.persagy.webstack.enumeration.business.ResourceStatusEnum;
import cn.krismile.persagy.webstack.enumeration.business.ResourceTypeEnum;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 业务-导航栏表
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("biz_nav")
@Schema(title = "BizNavDO对象", description = "业务-导航栏表")
public class BizNavDO extends BaseDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 顶级导航栏父ID值
     */
    public static final long TOP_PARENT_ID = -1L;

    @Schema(description = "标题名称")
    @TableField("title")
    private String title;

    @Schema(description = "标签名称")
    @TableField(value = "tag_name", updateStrategy = FieldStrategy.IGNORED)
    private String tagName;

    @Schema(description = "图标样式")
    @TableField("icon")
    private String icon;

    @Schema(description = "类型 [INTERNAL: 内部, USER: 用户, PUBLIC: 公共]")
    @TableField("type")
    private ResourceTypeEnum type;

    @Schema(description = "状态 [UNAPPROVED: 待审核, APPROVED: 已审核]")
    @TableField("status")
    private ResourceStatusEnum status;

    @Schema(description = "自关联上级导航栏ID, 第一级导航栏为 [-1]")
    @TableField("parent_id")
    private Long parentId;

    @Schema(description = "关联创建用户ID")
    @TableField("rel_create_admin_id")
    private Long relCreateAdminId;

    // ------------------------- 拓展字段 -------------------------

    @Schema(description = "排序权重, 默认值为 [0]")
    @TableField(exist = false)
    private Integer orderWeight;

    @Schema(description = "是否已关联当前用户")
    @TableField(exist = false)
    private Boolean isAlreadyRelation;

}
