package cn.krismile.persagy.webstack.function.system.service.impl;

import cn.krismile.persagy.webstack.pojo.domain.SysRelRolePermissionDO;
import cn.krismile.persagy.webstack.function.system.mapper.SysRelRolePermissionMapper;
import cn.krismile.persagy.webstack.function.system.service.SysRelRolePermissionService;
import cn.krismile.boot.framework.mybatisplus.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 系统-角色-权限关联表 ServiceImpl
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Service
public class SysRelRolePermissionServiceImpl extends BaseServiceImpl<SysRelRolePermissionMapper, SysRelRolePermissionDO> implements SysRelRolePermissionService {

}
