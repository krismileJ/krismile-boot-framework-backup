package cn.krismile.boot.framework.mybatisplus.generate;

import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.boot.framework.core.util.Assert;
import cn.krismile.boot.framework.mybatisplus.domain.BaseDO;
import cn.krismile.boot.framework.mybatisplus.enumeration.error.MybatisGenerateErrorEnum;
import cn.krismile.boot.framework.mybatisplus.exception.MybatisGenerateException;
import cn.krismile.boot.framework.mybatisplus.generate.config.CodeBasicConfig;
import cn.krismile.boot.framework.mybatisplus.generate.config.CodeDataSourceConfig;
import cn.krismile.boot.framework.mybatisplus.generate.config.CodePackageConfig;
import cn.krismile.boot.framework.mybatisplus.service.BaseService;
import cn.krismile.boot.framework.mybatisplus.service.BaseServiceImpl;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.DateType;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * MybatisPlus代码生成器
 *
 * <p>该类为 [MybatisPlus] 代码生成器服务, 自动将数据库表生成为 [Java] 代码
 * <p>Danger: 代码生成器将自动覆盖原有文件, 推荐在生成之前做好代码备份,
 * 如只需自动生成指定表则设置 {@link CodeDataSourceConfig#setIncludeTables(String...)} 即可
 * <hr>
 * <p><a href="https://mp.baomidou.com/guide/generator-new.html">点击查看官方文档</a>
 *
 * @author JiYinchuan
 * @see CodeBasicConfig
 * @see CodeDataSourceConfig
 * @see CodePackageConfig
 * @since 1.0.0
 */
public class DefaultGenerateCodeServiceImpl implements MybatisGenerateCodeService {

    /**
     * 代码生成
     *
     * @throws MybatisGenerateException 代码生成异常
     * @since 1.0.0
     */
    @Override
    public void generate(@NonNull CodeBasicConfig basicConfig,
                         @NonNull CodeDataSourceConfig dataSourceConfig,
                         @NonNull CodePackageConfig packageConfig) throws MybatisGenerateException {
        Assert.notNull(basicConfig, "MybatisGenerate basicConfig must not be null");
        Assert.notNull(dataSourceConfig, "MybatisGenerate dataSourceConfig must not be null");
        Assert.notNull(packageConfig, "MybatisGenerate packageConfig must not be null");

        if (dataSourceConfig.getUrl() == null || dataSourceConfig.getUrl().isEmpty()) {
            throw new MybatisGenerateException(MybatisGenerateErrorEnum.DATA_SOURCE_URL_IS_BLANK);
        }
        if (dataSourceConfig.getUsername() == null || dataSourceConfig.getUsername().isEmpty()) {
            throw new MybatisGenerateException(MybatisGenerateErrorEnum.DATA_SOURCE_USERNAME_IS_BLANK);
        }
        if (dataSourceConfig.getPassword() == null || dataSourceConfig.getPassword().isEmpty()) {
            throw new MybatisGenerateException(MybatisGenerateErrorEnum.DATA_SOURCE_PASSWORD_IS_BLANK);
        }
        if (packageConfig.getOutputPackage() == null || packageConfig.getOutputPackage().isEmpty()) {
            throw new MybatisGenerateException(MybatisGenerateErrorEnum.OUTPUT_PACKAGE_IS_BLANK);
        }
        if (packageConfig.getParentModelName() == null || packageConfig.getParentModelName().isEmpty()) {
            throw new MybatisGenerateException(MybatisGenerateErrorEnum.PACKAGE_PARENT_MODEL_NAME_IS_BLANK);
        }

        String url = dataSourceConfig.getUrl();
        String username = dataSourceConfig.getUsername();
        String password = dataSourceConfig.getPassword();
        FastAutoGenerator.create(url, username, password)
                .globalConfig(initGlobalConfig(basicConfig, packageConfig))
                .packageConfig(initPackageConfig(packageConfig))
                .strategyConfig(initStrategyConfig(dataSourceConfig))
                .templateConfig(initTemplateConfig(basicConfig))
                .injectionConfig(initInjectionConfig(basicConfig))
                .execute();

    }

    /**
     * 初始化全局配置
     *
     * <p><a href="https://mp.baomidou.com/guide/generator-new.html#%E6%95%B0%E6%8D%AE%E5%BA%93%E9%85%8D%E7%BD%AE-datasourceconfig">点击查看官方文档</a>
     *
     * @param basicConfig   基本配置
     * @param packageConfig 包配置
     * @return 全局配置
     * @since 1.0.0
     */
    private Consumer<GlobalConfig.Builder> initGlobalConfig(CodeBasicConfig basicConfig, CodePackageConfig packageConfig) {
        return builder -> {
            builder
                    // 指定输出目录
                    .outputDir(packageConfig.getOutputDir())
                    // 作者名
                    .author(basicConfig.getAuthor())
                    // 开启 swagger 模式
                    .enableSwagger()
                    // 时间策略
                    .dateType(DateType.TIME_PACK);
            if (Objects.nonNull(basicConfig.getEnableSwagger()) && basicConfig.getEnableSwagger()) {
                builder.enableSwagger();
            }
        };
    }

    /**
     * 初始化包配置
     *
     * <p><a href="https://mp.baomidou.com/guide/generator-new.html#%E5%8C%85%E9%85%8D%E7%BD%AE-packageconfig">点击查看官方文档</a>
     *
     * @param packageConfig 包配置
     * @return 包配置
     * @since 1.0.0
     */
    private Consumer<PackageConfig.Builder> initPackageConfig(CodePackageConfig packageConfig) {
        return builder -> builder
                // 父包名
                .parent(packageConfig.getOutputPackage())
                // 父包模块名
                .moduleName(packageConfig.getParentModelName())
                // Entity 包名
                .entity(packageConfig.getEntityPackageName())
                // Service 包名
                .service(packageConfig.getServicePackageName())
                // ServiceImpl 包名
                .serviceImpl(packageConfig.getServiceImplPackageName())
                // Mapper 包名
                .mapper(packageConfig.getMapperPackageName())
                // Controller 包名
                .controller(packageConfig.getControllerPackageName())
                // 路径配置信息
                .pathInfo(Collections.singletonMap(OutputFile.xml, packageConfig.getOutputResourcesDir() + packageConfig.getMapperXmlPackageName()));
    }

    /**
     * 初始化模版配置
     *
     * <p><a href="https://mp.baomidou.com/guide/generator-new.html#%E6%A8%A1%E6%9D%BF%E9%85%8D%E7%BD%AE-templateconfig">点击查看官方文档</a>
     *
     * @param basicConfig 基本配置
     * @return 包配置
     * @since 1.0.0
     */
    private Consumer<TemplateConfig.Builder> initTemplateConfig(CodeBasicConfig basicConfig) {
        return builder -> {
            builder
                    // 设置实体模板路径
                    .entity("/templates/entity.java")
                    // 设置 service 模板路径
                    .service("/templates/service.java")
                    // 设置 serviceImpl 模板路径
                    .serviceImpl("/templates/serviceImpl.java")
                    // 设置 mapper 模板路径
                    .mapper("/templates/mapper.java")
                    // 设置 mapperXml 模板路径
                    .xml("/templates/mapper.xml")
                    // 设置 controller 模板路径
                    .controller("/templates/controller.java");
            if (basicConfig.getOutputType() == CodeBasicConfig.OutputType.ONLY_DOMAIN) {
                builder.entity("/templates/entity.java");
                builder.disable(TemplateType.SERVICE);
                builder.disable(TemplateType.SERVICEIMPL);
                builder.disable(TemplateType.CONTROLLER);
                builder.disable(TemplateType.MAPPER);
                builder.disable(TemplateType.XML);
            }
            if (basicConfig.getOutputType() == CodeBasicConfig.OutputType.ALL_EXCLUDE_DOMAIN) {
                builder.disable(TemplateType.ENTITY);
            }
        };
    }

    /**
     * 初始化注入配置
     *
     * <p><a href="https://mp.baomidou.com/guide/generator-new.html#%E6%B3%A8%E5%85%A5%E9%85%8D%E7%BD%AE-injectionconfig">点击查看官方文档</a>
     *
     * @param basicConfig 基本配置
     * @return 包配置
     * @since 1.0.0
     */
    private Consumer<InjectionConfig.Builder> initInjectionConfig(CodeBasicConfig basicConfig) {
        Map<String, Object> injectionMap = new HashMap<>(16);
        injectionMap.put("since", basicConfig.getSince());
        return builder -> builder
                // 自定义配置 Map 对象
                .customMap(injectionMap);
    }

    /**
     * 初始化策略配置
     *
     * <p><a href="https://mp.baomidou.com/guide/generator-new.html#%E7%AD%96%E7%95%A5%E9%85%8D%E7%BD%AE-strategyconfig">点击查看官方文档</a>
     *
     * @param dataSourceConfig 数据源配置
     * @return 策略配置
     * @since 1.0.0
     */
    private Consumer<StrategyConfig.Builder> initStrategyConfig(CodeDataSourceConfig dataSourceConfig) {
        return builder -> {
            if (dataSourceConfig.getTablePrefix() != null && !dataSourceConfig.getTablePrefix().isEmpty()) {
                builder.addTablePrefix(dataSourceConfig.getTablePrefix());
            }
            if (dataSourceConfig.getIncludeTables().length > 0) {
                // 增加表匹配 (内存过滤)
                builder.addInclude(dataSourceConfig.getIncludeTables());
            }
            // 实体策略配置
            builder.entityBuilder()
                    // 设置父类
                    .superClass(BaseDO.class)
                    // 开启 lombok 模型
                    .enableLombok()
                    // 开启 Boolean 类型字段移除 is 前缀
                    .enableRemoveIsPrefix()
                    // 开启生成实体时生成字段注解
                    .enableTableFieldAnnotation()
                    // 乐观锁字段名 (数据库)
                    .versionColumnName(BaseDO.VERSION)
                    // 乐观锁属性名 (实体)
                    .versionPropertyName(BaseDO.VERSION)
                    // 逻辑删除字段名 (数据库)
                    .logicDeleteColumnName(BaseService.toUnderlineCase(BaseDO.LOGIC_DELETE))
                    // 逻辑删除属性名 (实体)
                    .logicDeletePropertyName(BaseDO.LOGIC_DELETE)
                    // 添加父类公共字段
                    .addSuperEntityColumns(BaseDO.ID,
                            BaseService.toUnderlineCase(BaseDO.CREATE_TIME),
                            BaseService.toUnderlineCase(BaseDO.UPDATE_TIME))
                    // 格式化文件名称
                    .formatFileName("%sDO")
                    .build();

            // Controller策略配置
            builder.controllerBuilder()
                    // 开启生成 @RestController 控制器
                    .enableRestStyle()
                    .build();

            // Service策略配置
            builder.serviceBuilder()
                    // 设置 service 接口父类
                    .superServiceClass(BaseService.class)
                    // 设置 service 实现类父类
                    .superServiceImplClass(BaseServiceImpl.class)
                    // 格式化 service 接口文件名称
                    .formatServiceFileName("%sService")
                    // 格式化 service 实现类文件名称
                    .formatServiceImplFileName("%sServiceImpl")
                    .build();

            // Mapper策略配置
            builder.mapperBuilder()
                    // 开启 @Mapper 注解
                    .enableMapperAnnotation()
                    // 格式化 mapper 文件名称
                    .formatMapperFileName("%sMapper")
                    // 格式化 xml 实现类文件名称
                    .formatXmlFileName("%sMapper")
                    .build();
        };
    }
}
