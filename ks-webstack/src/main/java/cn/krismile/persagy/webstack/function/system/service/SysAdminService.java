package cn.krismile.persagy.webstack.function.system.service;

import cn.krismile.boot.framework.core.exception.client.AuthorityException;
import cn.krismile.boot.framework.mybatisplus.service.BaseService;
import cn.krismile.persagy.webstack.pojo.domain.SysAdminDO;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.function.Supplier;

/**
 * 系统-用户表 Service
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface SysAdminService extends BaseService<SysAdminDO> {

    /**
     * 用户数据缓存中获取
     *
     * @param userId 用户ID
     * @return 用户数据
     * @since 1.0.0
     */
    SysAdminDO getCacheById(@NonNull Long userId);

    /**
     * 用户数据缓存中获取
     *
     * @param webCode 页面Code
     * @return 用户数据
     * @since 1.0.0
     */
    SysAdminDO getCacheByWebCode(@NonNull String webCode);

    /**
     * 检查用户状态是否可用
     *
     * @param userId 用户ID, 为空时从上下文中获取
     * @throws AuthorityException 帐号被冻结
     * @since 1.0.0
     */
    void checkStatus(@Nullable Long userId) throws AuthorityException;

    /**
     * 校验是否为系统内置用户
     *
     * @param userId    用户ID, 为空时从上下文中获取
     * @param condition 执行校验条件
     * @throws AuthorityException 访问未授权
     * @since 1.0.0
     */
    void checkIsInternal(@Nullable Long userId, @NonNull Supplier<Boolean> condition) throws AuthorityException;

}
