package cn.krismile.persagy.webstack.pojo.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 网站查询条件
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@Schema(title = "网站查询条件")
public class WebsiteQuery {

    @Schema(description = "关联导航栏ID")
    @NotNull
    private Long relNavId;

    @Schema(description = "网站标题名称")
    private String websiteTitle;

    @Schema(description = "网站标签名称")
    private String websiteTagName;

    @Schema(description = "网站链接")
    private String websiteLink;

    @Schema(description = "网站描述信息")
    private String websiteDescription;

}
