package cn.krismile.boot.framework.mybatisplus.service;

import cn.krismile.boot.framework.mybatisplus.domain.BaseDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * 封装 MybatisServiceImpl
 *
 * <p>该类为 {@link BaseService} 实现类, 与 {@link BaseService} 联合使用, 以替代 [Mybatis] 中的 {@link ServiceImpl}
 *
 * @author JiYinchuan
 * @see BaseService
 * @since 1.0.0
 */
public class BaseServiceImpl<M extends BaseMapper<T>, T extends BaseDO> extends ServiceImpl<M, T> implements BaseService<T> {
}
