package cn.krismile.boot.framework.core.util.inner;

/**
 * 字符串工具类
 *
 * <p>该类主要用于框架内的内部使用, 完整使用推荐
 * <a href="https://commons.apache.org/proper/commons-lang/">Apache's Commons Lang</a>,
 * 以获取更好的使用体验
 *
 * <ul>
 *     <li><b>isBlank</b> - 检查字符串序列是否为空</li>
 *     <li><b>isNotBlank</b> - 检查字符串序列是否不为空</li>
 *     <li><b>defaultString</b> - 如果为空则返回默认字符串</li>
 *     <li><b>length</b> - 获取字符串序列的长度</li>
 *     <li><b>isParsable</b> - 是否为可解析的数字</li>
 * </ul>
 *
 * @author JiYinchuan
 * @see String
 * @since 1.0.0
 */
public class StringUtils {

    /**
     * 检查字符串序列是否为空
     *
     * <p>空格检查使用 {@link Character#isWhitespace(char)}
     *
     * <pre>
     * StringUtils.isBlank(null)      = true
     * StringUtils.isBlank("")        = true
     * StringUtils.isBlank(" ")       = true
     * StringUtils.isBlank("bob")     = false
     * StringUtils.isBlank("  bob  ") = false
     * </pre>
     *
     * @param cs 字符串序列, 可为空
     * @return 字符串序列是否为空
     * @since 1.0.0
     */
    public static boolean isBlank(final CharSequence cs) {
        final int strLen = length(cs);
        if (strLen == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(cs.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    /**
     * 检查字符串序列是否不为空
     *
     * <p>空格检查使用 {@link Character#isWhitespace(char)}.</p>
     *
     * <pre>
     * StringUtils.isNotBlank(null)      = false
     * StringUtils.isNotBlank("")        = false
     * StringUtils.isNotBlank(" ")       = false
     * StringUtils.isNotBlank("bob")     = true
     * StringUtils.isNotBlank("  bob  ") = true
     * </pre>
     *
     * @param cs 字符串序列, 可为空
     * @return 字符串序列是否不为空
     * @since 1.0.0
     */
    public static boolean isNotBlank(final CharSequence cs) {
        return !isBlank(cs);
    }

    /**
     * 如果 {@code str} 为 {@code null}, 则返回 {@code defaultStr}
     *
     * <pre>
     * StringUtils.defaultString(null, "NULL")  = "NULL"
     * StringUtils.defaultString("", "NULL")    = ""
     * StringUtils.defaultString(" ", "EMPTY")  = "EMPTY"
     * StringUtils.defaultString("bat", "NULL") = "bat"
     * </pre>
     *
     * @param str        字符串, 可为空
     * @param defaultStr 返回的默认字符串
     * @return 返回传入的 {@code str}, 如果为空时则返回 {@code defaultStr}
     * @see #isBlank(CharSequence)
     */
    public static String defaultString(final String str, final String defaultStr) {
        return isBlank(str) ? defaultStr : str;
    }

    /**
     * 获取字符串序列的长度
     *
     * @param cs 字符串序列, 可为空
     * @return 字符串序列的长度, 字符串序列为空时长度为 {@code 0}
     * @since 1.0.0
     */
    public static int length(final CharSequence cs) {
        return cs == null ? 0 : cs.length();
    }

    /**
     * 是否为可解析的数字
     *
     * <p>该方法不支持十六进制与科学计数法
     *
     * @param str 需要判断的字符串
     * @return [true: 可解析, false: 不可解析]
     * @since 1.0.0
     */
    @SuppressWarnings("AlibabaUndefineMagicConstant")
    public static boolean isParsable(final String str) {
        if (isBlank(str)) {
            return false;
        }
        if (str.charAt(str.length() - 1) == '.') {
            return false;
        }
        if (str.charAt(0) == '-') {
            if (str.length() == 1) {
                return false;
            }
            return withDecimalsParsing(str, 1);
        }
        return withDecimalsParsing(str, 0);
    }

    /**
     * 带小数进行解析是否为可解析的数字
     *
     * @param str      需要解析的字符串
     * @param beginIdx 开始索引
     * @return 是否为数字
     * @since 1.0.0
     */
    private static boolean withDecimalsParsing(final String str, final int beginIdx) {
        int decimalPoints = 0;
        for (int i = beginIdx; i < str.length(); i++) {
            final boolean isDecimalPoint = str.charAt(i) == '.';
            if (isDecimalPoint) {
                decimalPoints++;
            }
            if (decimalPoints > 1) {
                return false;
            }
            if (!isDecimalPoint && !Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
