package cn.krismile.boot.framework.core.exception.client;

import cn.krismile.boot.framework.core.enumeration.error.client.RequestServerErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 请求服务自定义异常
 *
 * @author JiYinchuan
 * @see RequestServerErrorEnum
 * @since 1.0.0
 */
public class RequestServerException extends ApplicationException {

    private static final long serialVersionUID = -247045443350877419L;

    public RequestServerException(RequestServerErrorEnum requestServerErrorEnum) {
        super(requestServerErrorEnum);
    }

    public RequestServerException(RequestServerErrorEnum requestServerErrorEnum, String userTip) {
        super(requestServerErrorEnum, userTip);
    }
}
