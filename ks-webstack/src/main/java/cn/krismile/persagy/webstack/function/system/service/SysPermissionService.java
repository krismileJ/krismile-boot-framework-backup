package cn.krismile.persagy.webstack.function.system.service;

import cn.krismile.persagy.webstack.pojo.domain.SysPermissionDO;
import cn.krismile.boot.framework.mybatisplus.service.BaseService;

/**
 * 系统-权限表 Service
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface SysPermissionService extends BaseService<SysPermissionDO> {

}
