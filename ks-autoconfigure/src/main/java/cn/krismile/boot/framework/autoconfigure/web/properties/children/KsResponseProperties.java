package cn.krismile.boot.framework.autoconfigure.web.properties.children;

import cn.krismile.boot.framework.autoconfigure.web.properties.KsWebProperties;
import cn.krismile.boot.framework.core.response.vo.BaseVO;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 请求响应自动配置
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@ConfigurationProperties(prefix = KsResponseProperties.KEY)
public class KsResponseProperties {

    public static final String KEY = KsWebProperties.KEY + "." + "response";

    /**
     * 默认成功用户提示信息
     */
    private String defaultSuccessUserTip = BaseVO.DEFAULT_SUCCESS_USER_TIP;

    /**
     * 默认异常用户提示信息
     */
    private String defaultErrorUserTip = BaseVO.DEFAULT_ERROR_USER_TIP;

}
