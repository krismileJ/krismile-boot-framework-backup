/**
 * [ks-mybatisplus] 异常
 */
@NonNullApi
@NonNullFields
package cn.krismile.boot.framework.mybatisplus.exception;

import cn.krismile.boot.framework.core.annotation.jsr.NonNullApi;
import cn.krismile.boot.framework.core.annotation.jsr.NonNullFields;