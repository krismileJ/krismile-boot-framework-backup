package cn.krismile.boot.framework.mybatisplus.generate.config;

import lombok.Data;

/**
 * MyBatisPlus数据源配置文件
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
public class CodeDataSourceConfig {

    /**
     * 驱动连接的URL
     */
    private String url;

    /**
     * 数据库连接用户名
     */
    private String username;

    /**
     * 数据库连接密码
     */
    private String password;

    /**
     * 统一表前缀
     */
    private String tablePrefix;

    /**
     * 输出包含表
     */
    private String[] includeTables;

    /**
     * 设置输出包含表
     *
     * @param includeTables 输出包含表
     * @since 1.0.0
     */
    public void setIncludeTables(String... includeTables) {
        this.includeTables = includeTables;
    }
}
