package cn.krismile.persagy.webstack.pojo.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 网站排序DTO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@Schema(title = "网站排序DTO")
public class WebsiteSortDTO {

    @Schema(description = "源网站ID")
    @NotNull
    private Long sourceWebsiteId;

    @Schema(description = "目标网站ID")
    @NotNull
    private Long targetWebsiteId;

}
