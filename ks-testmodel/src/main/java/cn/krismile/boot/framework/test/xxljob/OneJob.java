package cn.krismile.boot.framework.test.xxljob;

import cn.hutool.http.HttpUtil;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 定时任务
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Component
@Slf4j
@RestController
public class OneJob {

    private final AtomicInteger counter = new AtomicInteger();
    private static ExecutorService executorService = Executors.newFixedThreadPool(5);

    @XxlJob("test_job")
    public ReturnT<String> test_job() throws InterruptedException {
        log.info("thread id {}", Thread.currentThread().getId());
        XxlJobHelper.log("thread id {}", Thread.currentThread().getId());

        List<CompletableFuture<String>> all = new ArrayList<>();
        Thread thread = Thread.currentThread();
        for (int i = 0; i < 20; i++) {
            CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> {
                if (thread.isInterrupted()) {
                    log.error("InterruptedException");
                    XxlJobHelper.log("InterruptedException");
                    return "InterruptedException";
                }
                return HttpUtil.get("http://localhost:10001/a");
            }, executorService);
            all.add(completableFuture);
        }
        List<String> allResult = new ArrayList<>();
        for (CompletableFuture<String> stringCompletableFuture : all) {
            allResult.add(stringCompletableFuture.join());
        }
        System.out.println(allResult);
        // try {
        //     HttpUtil.get("http://localhost:10001/a");
        //     if (Thread.currentThread().isInterrupted()) {
        //         log.error("InterruptedException");
        //         XxlJobHelper.log("InterruptedException");
        //         throw new InterruptedException();
        //     }
        // } catch (Exception e) {
        //     if (e instanceof InterruptedException) {
        //         throw e;
        //     }
        //     log.error("Exception", e);
        //     XxlJobHelper.log("Exception", e);
        // }
        // log.info("isInterrupted {}", Thread.currentThread().isInterrupted());
        // log.info("execute num {}", counter.get());
        // XxlJobHelper.log("execute num {}", counter.getAndIncrement());
        return ReturnT.FAIL;
    }

    @RequestMapping("/b")
    private void a() {
        try {
            TimeUnit.SECONDS.sleep(10);
            System.out.println("bbbbbb");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
