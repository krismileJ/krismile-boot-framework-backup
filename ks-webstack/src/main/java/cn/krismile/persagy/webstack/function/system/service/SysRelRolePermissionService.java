package cn.krismile.persagy.webstack.function.system.service;

import cn.krismile.persagy.webstack.pojo.domain.SysRelRolePermissionDO;
import cn.krismile.boot.framework.mybatisplus.service.BaseService;

/**
 * 系统-角色-权限关联表 Service
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface SysRelRolePermissionService extends BaseService<SysRelRolePermissionDO> {

}
