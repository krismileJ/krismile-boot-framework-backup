package cn.krismile.boot.framework.core.response.vo;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import cn.krismile.boot.framework.core.response.ResultVO;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.Collection;

/**
 * 多条数据VO
 *
 * <p>该类为多条数据VO, 用于多条数据返回的情况下使用, 多条数据+分页时请使用 {@link PageVO}, 使用 {@link ResultVO} 中相关方法进行返回
 *
 * @author JiYinchuan
 * @see ResultVO
 * @since 1.0.0
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public final class MultiVO<T> extends BaseVO implements Serializable {

    private static final long serialVersionUID = 124382196987354261L;

    /**
     * 多条数据
     */
    private Collection<T> data;

    /**
     * 构造器
     *
     * @param baseEnum 响应枚举
     * @param data     多条数据
     * @since 1.0.0
     */
    public MultiVO(BaseEnum<String> baseEnum, Collection<T> data) {
        super(baseEnum);
        this.data = data;
    }

    /**
     * 构造器
     *
     * @param baseEnum 响应枚举
     * @param userTip  用户提示信息
     * @param data     多条数据
     * @since 1.0.0
     */
    public MultiVO(BaseEnum<String> baseEnum, String userTip, Collection<T> data) {
        super(baseEnum, userTip);
        this.data = data;
    }

    /**
     * 构造器
     *
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     * @param data         多条数据
     * @since 1.0.0
     */
    public MultiVO(String errorCode, String errorMessage, Collection<T> data) {
        super(errorCode, errorMessage);
        this.data = data;
    }

    /**
     * 构造器
     *
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     * @param userTip      用户提示信息
     * @param data         多条数据
     * @since 1.0.0
     */
    public MultiVO(String errorCode, String errorMessage, String userTip, Collection<T> data) {
        super(errorCode, errorMessage, userTip);
        this.data = data;
    }
}
