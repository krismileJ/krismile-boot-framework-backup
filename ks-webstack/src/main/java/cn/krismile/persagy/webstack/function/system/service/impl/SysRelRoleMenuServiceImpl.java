package cn.krismile.persagy.webstack.function.system.service.impl;

import cn.krismile.persagy.webstack.pojo.domain.SysRelRoleMenuDO;
import cn.krismile.persagy.webstack.function.system.mapper.SysRelRoleMenuMapper;
import cn.krismile.persagy.webstack.function.system.service.SysRelRoleMenuService;
import cn.krismile.boot.framework.mybatisplus.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 系统-角色-菜单关联表 ServiceImpl
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Service
public class SysRelRoleMenuServiceImpl extends BaseServiceImpl<SysRelRoleMenuMapper, SysRelRoleMenuDO> implements SysRelRoleMenuService {

}
