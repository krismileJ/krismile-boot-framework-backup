package cn.krismile.boot.framework.mybatisplus.exception;

import cn.krismile.boot.framework.core.annotation.jsr.Nullable;
import cn.krismile.boot.framework.core.exception.ApplicationException;
import cn.krismile.boot.framework.mybatisplus.enumeration.error.MybatisGenerateErrorEnum;
import cn.krismile.boot.framework.mybatisplus.enumeration.error.MybatisServiceErrorEnum;

/**
 * MybatisService自定义异常
 *
 * @author JiYinchuan
 * @see MybatisServiceErrorEnum
 * @since 1.0.0
 */
public class MybatisGenerateException extends ApplicationException {

    private static final long serialVersionUID = -319265732841645292L;

    public MybatisGenerateException(MybatisGenerateErrorEnum mybatisServiceErrorEnum) {
        super(mybatisServiceErrorEnum);
    }

    public MybatisGenerateException(MybatisGenerateErrorEnum mybatisServiceErrorEnum, @Nullable String userTip) {
        super(mybatisServiceErrorEnum, userTip);
    }
}