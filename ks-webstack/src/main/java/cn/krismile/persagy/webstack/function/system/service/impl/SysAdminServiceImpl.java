package cn.krismile.persagy.webstack.function.system.service.impl;

import cn.krismile.boot.framework.core.enumeration.error.client.AuthorityErrorEnum;
import cn.krismile.boot.framework.core.exception.client.AuthorityException;
import cn.krismile.boot.framework.mybatisplus.service.BaseServiceImpl;
import cn.krismile.persagy.webstack.enumeration.business.YesNoEnum;
import cn.krismile.persagy.webstack.function.system.mapper.SysAdminMapper;
import cn.krismile.persagy.webstack.function.system.service.SysAdminService;
import cn.krismile.persagy.webstack.pojo.context.UserInfoContext;
import cn.krismile.persagy.webstack.pojo.domain.SysAdminDO;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * 系统-用户表 ServiceImpl
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Service
public class SysAdminServiceImpl extends BaseServiceImpl<SysAdminMapper, SysAdminDO> implements SysAdminService {

    @Resource
    @Lazy
    private SysAdminService self;

    @Override
    @Cacheable(cacheNames = "user", key = "#result.id+'-*'")
    public SysAdminDO getCacheById(@NonNull Long userId) {
        return getByIdValidate(userId);
    }

    @Override
    @Cacheable(cacheNames = "user", key = "#result.id+'-*'")
    public SysAdminDO getCacheByWebCode(@NonNull String webCode) {
        return lambdaQuery().eq(SysAdminDO::getWebCode, webCode).one();
    }

    @Override
    public void checkStatus(@Nullable Long userId) throws AuthorityException {
        userId = Objects.isNull(userId) ? UserInfoContext.userId() : userId;

        SysAdminDO dbUser = self.getCacheById(userId);
        if (YesNoEnum.NO == dbUser.getStatus()) {
            throw new AuthorityException(AuthorityErrorEnum.ACCOUNT_IS_FROZEN);
        }
    }

    @Override
    public void checkIsInternal(@Nullable Long userId, @NonNull Supplier<Boolean> condition) throws AuthorityException {
        if (BooleanUtils.isTrue(condition.get())) {
            userId = Objects.isNull(userId) ? UserInfoContext.userId() : userId;

            SysAdminDO dbUser = self.getCacheById(userId);
            Boolean dbInternal = dbUser.getInternal();
            // 检查状态是否为系统内置
            if (BooleanUtils.isNotTrue(dbInternal)) {
                throw new AuthorityException(AuthorityErrorEnum.UNAUTHORIZED);
            }
        }
    }
}
