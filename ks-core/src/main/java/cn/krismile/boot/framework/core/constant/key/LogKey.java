package cn.krismile.boot.framework.core.constant.key;

/**
 * 日志Key
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface LogKey {

    /**
     * TrackId
     */
    String TRACK_ID = "KS_LOG_TTL";

}
