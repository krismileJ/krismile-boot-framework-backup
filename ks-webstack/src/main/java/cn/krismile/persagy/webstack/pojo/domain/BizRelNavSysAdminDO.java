package cn.krismile.persagy.webstack.pojo.domain;

import cn.krismile.boot.framework.mybatisplus.domain.BaseDO;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 业务-导航-用户关联表
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("biz_rel_nav_sys_admin")
@Schema(title = "BizRelNavSysAdminDO实体类", description = "业务-导航-用户关联表")
public class BizRelNavSysAdminDO extends BaseDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "排序权重, 默认值为 [0]")
    @TableField("order_weight")
    private Integer orderWeight;

    @Schema(description = "关联导航ID")
    @TableField("rel_nav_id")
    private Long relNavId;

    @Schema(description = "关联系统用户ID")
    @TableField("rel_sys_admin_id")
    private Long relSysAdminId;

}
