/**
 * [ks-core] 异常-框架
 */
@NonNullApi
@NonNullFields
package cn.krismile.boot.framework.core.exception.framework;

import cn.krismile.boot.framework.core.annotation.jsr.NonNullApi;
import cn.krismile.boot.framework.core.annotation.jsr.NonNullFields;