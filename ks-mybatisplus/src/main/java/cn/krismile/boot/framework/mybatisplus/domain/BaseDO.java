package cn.krismile.boot.framework.mybatisplus.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 基础DO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
public class BaseDO implements Serializable {

    private static final long serialVersionUID = 89624647673473543L;

    public static final String ID = "id";
    public static final String CREATE_TIME = "createTime";
    public static final String UPDATE_TIME = "updateTime";
    public static final String LOGIC_DELETE = "logicDelete";
    public static final String VERSION = "version";

    /**
     * 唯一ID
     *
     * <p>默认使用雪花ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 创建时间
     *
     * <p>新增时将自动填充当前时间
     */
    @TableField(value = "create_time", fill = FieldFill.INSERT, insertStrategy = FieldStrategy.NOT_NULL)
    private LocalDateTime createTime;

    /**
     * 最后修改时间
     *
     * <p>新增与修改时自动填充当前时间
     */
    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE,
            insertStrategy = FieldStrategy.NOT_NULL, updateStrategy = FieldStrategy.NOT_NULL)
    private LocalDateTime updateTime;

}
