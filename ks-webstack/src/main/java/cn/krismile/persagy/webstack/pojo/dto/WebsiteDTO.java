package cn.krismile.persagy.webstack.pojo.dto;

import cn.krismile.boot.framework.context.mvc.annotation.TrimWhiteSpace;
import cn.krismile.persagy.webstack.enumeration.business.ResourceStatusEnum;
import cn.krismile.persagy.webstack.enumeration.business.ResourceTypeEnum;
import cn.krismile.persagy.webstack.validate.ValidateGroup;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 导航栏网站DTO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@TrimWhiteSpace
public class WebsiteDTO implements Serializable {

    private static final long serialVersionUID = 8322758138884570384L;

    @Schema(description = "关联导航栏ID")
    @NotNull(groups = ValidateGroup.Insert.class)
    private Long relNavId;

    @Schema(description = "网站ID")
    @NotNull(groups = ValidateGroup.Update.class)
    private Long websiteId;

    @Schema(description = "网站标题")
    @NotBlank(groups = {ValidateGroup.Insert.class, ValidateGroup.Update.class})
    private String websiteTitle;

    @Schema(description = "网站缩略图, 为空时使用默认缩略图")
    private String websiteImage;

    @Schema(description = "网站链接地址")
    @NotBlank(groups = {ValidateGroup.Insert.class, ValidateGroup.Update.class})
    private String websiteLink;

    @Schema(description = "网站标签名称")
    private String websiteTagName;

    @Schema(description = "网站描述信息")
    private String websiteDescription;

    @Schema(description = "网站类型 [INTERNAL: 内部, USER: 用户, PUBLIC: 公共]")
    @NotNull(groups = ValidateGroup.Insert.class)
    private ResourceTypeEnum websiteType;

    @Schema(description = "网站状态 [UNAPPROVED: 待审核, APPROVED: 已审核]")
    private ResourceStatusEnum websiteStatus;

    @Schema(description = "网站排序权重, 默认值为 [0]")
    @NotNull(groups = {ValidateGroup.Insert.class, ValidateGroup.Update.class})
    @Min(value = 0, groups = {ValidateGroup.Insert.class, ValidateGroup.Update.class})
    private Integer websiteOrderWeight;

}
