package cn.krismile.boot.framework.core.exception.server;

import cn.krismile.boot.framework.core.enumeration.error.server.ResourceErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 系统资源自定义异常
 *
 * @author JiYinchuan
 * @see ResourceErrorEnum
 * @since 1.0.0
 */
public class ResourceException extends ApplicationException {

    private static final long serialVersionUID = -7330653879165739204L;

    public ResourceException(ResourceErrorEnum resourceErrorEnum) {
        super(resourceErrorEnum);
    }

    public ResourceException(ResourceErrorEnum resourceErrorEnum, String userTip) {
        super(resourceErrorEnum, userTip);
    }
}