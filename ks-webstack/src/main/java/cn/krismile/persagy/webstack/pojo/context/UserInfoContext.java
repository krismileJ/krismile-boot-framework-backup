package cn.krismile.persagy.webstack.pojo.context;

import cn.krismile.boot.framework.context.storage.LocalContextStorage;
import cn.krismile.persagy.webstack.key.cache.UserKey;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 用户信息DTO
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@Accessors(chain = true)
public class UserInfoContext {

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 从当前线程中获取用户ID
     *
     * @return 当前线程中的用户ID
     * @since 1.0.0
     */
    public static Long userId() {
        return LocalContextStorage.getThrow(UserKey.CONTEXT, UserInfoContext.class).getUserId();
    }
}
