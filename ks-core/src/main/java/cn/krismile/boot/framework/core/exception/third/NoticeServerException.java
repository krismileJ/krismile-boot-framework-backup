package cn.krismile.boot.framework.core.exception.third;

import cn.krismile.boot.framework.core.enumeration.error.third.NoticeServerErrorEnum;
import cn.krismile.boot.framework.core.exception.ApplicationException;

/**
 * 通知服务出错自定义异常
 *
 * @author JiYinchuan
 * @see NoticeServerErrorEnum
 * @since 1.0.0
 */
public class NoticeServerException extends ApplicationException {

    private static final long serialVersionUID = -350926595725509138L;

    public NoticeServerException(NoticeServerErrorEnum noticeServerErrorEnum) {
        super(noticeServerErrorEnum);
    }

    public NoticeServerException(NoticeServerErrorEnum noticeServerErrorEnum, String userTip) {
        super(noticeServerErrorEnum, userTip);
    }
}
