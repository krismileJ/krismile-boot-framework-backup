package cn.krismile.boot.framework.mybatisplus.generate;

import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import cn.krismile.boot.framework.mybatisplus.exception.MybatisGenerateException;
import cn.krismile.boot.framework.mybatisplus.generate.config.CodeBasicConfig;
import cn.krismile.boot.framework.mybatisplus.generate.config.CodeDataSourceConfig;
import cn.krismile.boot.framework.mybatisplus.generate.config.CodePackageConfig;

/**
 * MybatisPlus代码生成器接口
 *
 * <p>默认提供了一个代码生成器接口实现, 见 {@link DefaultGenerateCodeServiceImpl}
 * <hr>
 * <p>简单使用示例:
 * <pre>
 *     MybatisGenerateCodeService generateCodeService = new DefaultGenerateCodeServiceImpl();
 *
 *     CodeBasicConfig basicConfig = new CodeBasicConfig();
 *     // 作者
 *     basicConfig.setAuthor("ks-boot4j");
 *
 *     CodeDataSourceConfig dataSourceConfig = new CodeDataSourceConfig();
 *     // 驱动连接的URL, 须指定
 *     dataSourceConfig.setUrl("jdbc:mysql://127.0.0.1:3306/ks_boot4j_mybatisplus_demo?useUnicode=true&amp;characterEncoding=utf8&amp;serverTimezone=Asia/Shanghai&amp;useSSL=false&amp;allowMultiQueries=true");
 *     // 数据库连接用户名, 须指定
 *     dataSourceConfig.setUsername("root");
 *     // 数据库连接密码, 须指定
 *     dataSourceConfig.setPassword("123456");
 *     // 统一表前缀, 可为空
 *     dataSourceConfig.setTablePrefix(null);
 *     // 输出包含表, 可为空, 为空时则输出所有表
 *     dataSourceConfig.setIncludeTables();
 *
 *     CodePackageConfig packageConfig = new CodePackageConfig();
 *     // 输出包名, 须指定
 *     packageConfig.setOutputPackage("cn.krismile.boot.framework");
 *     // 父包模块名, 须指定
 *     packageConfig.setParentModelName("mybatisplus");
 *
 *     generateCodeService.generate(basicConfig, dataSourceConfig, packageConfig);
 * </pre>
 * <hr>
 * <p>完整使用示例:
 * <pre>
 *     MybatisGenerateCodeService generateCodeService = new DefaultGenerateCodeServiceImpl();
 *
 *     CodeBasicConfig basicConfig = new CodeBasicConfig();
 *     // 作者
 *     basicConfig.setAuthor("ks-boot4j");
 *     // 版本号
 *     // 默认为 [1.0.0]
 *     basicConfig.setSince("1.0.0");
 *     // 输出包类型
 *     // 默认为 [CodeBasicConfig.OutputType.ALL]
 *     basicConfig.setOutputType(CodeBasicConfig.OutputType.ALL);
 *     // 是否开启Swagger注解支持
 *     // 默认为 [true]
 *     basicConfig.setEnableSwagger(true);
 *
 *     CodeDataSourceConfig dataSourceConfig = new CodeDataSourceConfig();
 *     // 驱动连接的URL, 须指定
 *     dataSourceConfig.setUrl("jdbc:mysql://127.0.0.1:3306/ks_boot4j_mybatisplus_demo?useUnicode=true&amp;characterEncoding=utf8&amp;serverTimezone=Asia/Shanghai&amp;useSSL=false&amp;allowMultiQueries=true");
 *     // 数据库连接用户名, 须指定
 *     dataSourceConfig.setUsername("root");
 *     // 数据库连接密码, 须指定
 *     dataSourceConfig.setPassword("123456");
 *     // 统一表前缀, 可为空
 *     dataSourceConfig.setTablePrefix(null);
 *     // 输出包含表, 可为空, 为空时则输出所有表
 *     dataSourceConfig.setIncludeTables();
 *
 *     CodePackageConfig packageConfig = new CodePackageConfig();
 *     // 输出包名, 须指定
 *     packageConfig.setOutputPackage("cn.krismile.boot.framework");
 *     // 父包模块名, 须指定
 *     packageConfig.setParentModelName("mybatisplus");
 *     // 实体类包名
 *     // 默认为 [pojo.domain]
 *     packageConfig.setEntityPackageName("pojo.domain");
 *     // 数据访问层包名
 *     // 默认为 [mapper]
 *     packageConfig.setMapperPackageName("mapper");
 *     // 数据访问层XML包名, 生成后请手动移动到 [resource] 对应目录下
 *     // 默认为 [mapper/xml]
 *     packageConfig.setMapperXmlPackageName("mapper/xml");
 *     // 业务逻辑层包名
 *     // 默认为 [service]
 *     packageConfig.setServicePackageName("service");
 *     // 业务逻辑实现层包名
 *     // 默认为 [service.impl]
 *     packageConfig.setServiceImplPackageName("service.impl");
 *     // 界面层包名
 *     // 默认为 [controller]
 *     packageConfig.setControllerPackageName("controller");
 *     // 输出根目录绝对路径
 *     // 默认为 [当前项目目录/src/main/java]
 *     packageConfig.setOutputDir(System.getProperty("user.dir").concat("/src/main/java/"));
 *
 *     generateCodeService.generate(basicConfig, dataSourceConfig, packageConfig);
 * </pre>
 * <hr>
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public interface MybatisGenerateCodeService {

    /**
     * 代码生成
     *
     * @param basicConfig      基本配置
     * @param dataSourceConfig 数据源配置
     * @param packageConfig    包配置
     * @throws MybatisGenerateException 代码生成异常
     * @since 1.0.0
     */
    void generate(@NonNull CodeBasicConfig basicConfig,
                  @NonNull CodeDataSourceConfig dataSourceConfig,
                  @NonNull CodePackageConfig packageConfig)
            throws MybatisGenerateException;

}
