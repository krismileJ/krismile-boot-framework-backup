/**
 * [ks-mybatisplus] 枚举
 */
@NonNullApi
@NonNullFields
package cn.krismile.boot.framework.mybatisplus.enumeration;

import cn.krismile.boot.framework.core.annotation.jsr.NonNullApi;
import cn.krismile.boot.framework.core.annotation.jsr.NonNullFields;