package cn.krismile.boot.framework.context.mvc.deserialization;

import cn.krismile.boot.framework.context.mvc.annotation.TrimWhiteSpace;
import cn.krismile.boot.framework.core.annotation.jsr.NonNull;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.BeanDeserializerBuilder;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.util.Objects;

/**
 * 清除空格反序列化器
 *
 * <p>实现类说明: {@link StdSerializer} 为 jackson 推荐自定义序列化器基类,
 * {@link ContextualSerializer} 为 jackson 创建序列化程序的上下文实例以用于处理受支持类型的属性
 * <p><b>Warning: </b>因该自定义序列化器为 {@link TrimWhiteSpace @TrimWhiteSpace} 注解标记到类上, 即只有标记了注解才会执行该类
 *
 * @author JiYinchuan
 * @see TrimWhiteSpace @TrimWhiteSpace
 * @since 1.0.0
 */
public class TrimWhiteSpaceDeserialization extends StdDeserializer<Object> implements ContextualDeserializer {

    private static final long serialVersionUID = 5806133459639734299L;

    private static final JsonDeserializer<String> DEFAULT_STRING_DESERIALIZER = new StringDeserializer();

    private TrimWhiteSpace trimWhiteSpaceAnnotation;

    protected TrimWhiteSpaceDeserialization() {
        super(Object.class);
    }

    @Override
    public String deserialize(JsonParser p, @NonNull DeserializationContext ctxt) throws IOException {
        String defaultDeserializationValue = DEFAULT_STRING_DESERIALIZER.deserialize(p, ctxt);
        if (trimWhiteSpaceAnnotation != null && !defaultDeserializationValue.isEmpty()) {
            return defaultDeserializationValue.trim();
        }
        return defaultDeserializationValue;
    }

    @Override
    public JsonDeserializer<?> createContextual(
            DeserializationContext ctxt, BeanProperty property) throws JsonMappingException {
        TrimWhiteSpace annotation;
        if (Objects.isNull(property)) {
            JavaType contextualType = ctxt.getContextualType();
            Class<?> rawClass = contextualType.getRawClass();
            annotation = rawClass.getAnnotation(TrimWhiteSpace.class);
            if (Objects.isNull(annotation)) {
                annotation = rawClass.getDeclaredAnnotation(TrimWhiteSpace.class);
            }
            if (Objects.nonNull(annotation)) {
                this.trimWhiteSpaceAnnotation = annotation;
                return this;
            }
            return new BeanDeserializerBuilder(ctxt.getConfig().introspect(contextualType), ctxt).build();
        }
        annotation = property.getAnnotation(TrimWhiteSpace.class);
        if (annotation == null) {
            annotation = property.getContextAnnotation(TrimWhiteSpace.class);
        }
        if (annotation != null) {
            this.trimWhiteSpaceAnnotation = annotation;
            return this;
        }
        return DEFAULT_STRING_DESERIALIZER;
    }
}
