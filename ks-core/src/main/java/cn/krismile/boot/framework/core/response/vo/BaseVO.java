package cn.krismile.boot.framework.core.response.vo;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import cn.krismile.boot.framework.core.response.ResultVO;
import lombok.Data;

import java.io.Serializable;

/**
 * VO基类
 *
 * <p>该类为全局VO响应基类, 提供了默认响应参数, 用于无数据返回的情况下使用, 使用 {@link ResultVO} 中相关方法进行返回
 *
 * @author JiYinchuan
 * @see ResultVO
 * @since 1.0.0
 */
@Data
public class BaseVO implements Serializable {

    private static final long serialVersionUID = 3751203403306545328L;
    public static final String ERROR_CODE = "errorCode";
    public static final String ERROR_MESSAGE = "errorMessage";
    public static final String USER_TIP = "userTip";

    /**
     * 默认成功用户提示信息
     */
    public static String DEFAULT_SUCCESS_USER_TIP = "OK";
    /**
     * 默认异常用户提示信息
     */
    public static String DEFAULT_ERROR_USER_TIP = "网络开小差了, 请稍后再试 (╯﹏╰)";

    /**
     * 错误码
     */
    private String errorCode;

    /**
     * 错误信息
     */
    private String errorMessage;

    /**
     * 用户提示信息
     */
    private String userTip;

    /**
     * 构造器
     *
     * @since 1.0.0
     */
    public BaseVO() {
        this.userTip = DEFAULT_SUCCESS_USER_TIP;
    }

    /**
     * 构造器
     *
     * @param baseEnum 响应枚举
     * @since 1.0.0
     */
    public BaseVO(BaseEnum<String> baseEnum) {
        this.errorCode = baseEnum.value();
        this.errorMessage = baseEnum.getReasonPhrase();
        this.userTip = DEFAULT_SUCCESS_USER_TIP;
    }

    /**
     * 构造器
     *
     * @param baseEnum 响应枚举
     * @param userTip  用户提示信息
     * @since 1.0.0
     */
    public BaseVO(BaseEnum<String> baseEnum, String userTip) {
        this.errorCode = baseEnum.value();
        this.errorMessage = baseEnum.getReasonPhrase();
        this.userTip = userTip;
    }

    /**
     * 构造器
     *
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     * @since 1.0.0
     */
    public BaseVO(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.userTip = DEFAULT_SUCCESS_USER_TIP;
    }


    /**
     * 构造器
     *
     * @param errorCode    错误码
     * @param errorMessage 错误信息
     * @param userTip      用户提示信息
     * @since 1.0.0
     */
    public BaseVO(String errorCode, String errorMessage, String userTip) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.userTip = userTip;
    }
}
