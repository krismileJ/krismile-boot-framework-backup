package cn.krismile.boot.framework.test.pojo;

import lombok.ToString;

import java.io.Serializable;

/**
 * TODO
 *
 * @author WangWeijie
 * @since 1.0.0
 */
@ToString
public class TestVO implements Serializable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
