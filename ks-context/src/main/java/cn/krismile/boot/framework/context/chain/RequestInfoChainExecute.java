package cn.krismile.boot.framework.context.chain;

import cn.krismile.boot.framework.context.model.RequestInfo;
import cn.krismile.boot.framework.context.model.ResponseInfo;

import javax.servlet.http.HttpServletRequest;

/**
 * 请求信息链式处理
 *
 * <p>该类提供两个方法, 分别在方法执行前和执行后自定义处理逻辑, 可用于日至记录或日至打印等自定义逻辑
 *
 * @author JiYinchuan
 * @see cn.krismile.boot.framework.context.aop.RequestLogAop
 * @see DefaultRequestLogChainExecute
 * @since 1.0.0
 */
public interface RequestInfoChainExecute {

    /**
     * 默认执行顺序
     */
    int DEFAULT_EXECUTE_ORDER = 100;

    /**
     * 处理前置请求信息
     *
     * @param request     {@link HttpServletRequest}
     * @param requestInfo 请求信息
     * @since 1.0.0
     */
    void beforeExecute(HttpServletRequest request, RequestInfo requestInfo);

    /**
     * 处理后置请求信息
     *
     * @param request           {@link HttpServletRequest}
     * @param requestInfo       请求信息
     * @param responseInfo 请求结果信息
     * @since 1.0.0
     */
    void afterExecute(HttpServletRequest request, RequestInfo requestInfo, ResponseInfo responseInfo);

}
