package cn.krismile.persagy.webstack.pojo.domain;

import cn.krismile.boot.framework.mybatisplus.domain.BaseDO;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 系统-权限表
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_permission")
@Schema(title = "SysPermissionDO对象", description = "系统-权限表")
public class SysPermissionDO extends BaseDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "权限名称")
    @TableField("name")
    private String name;

    @Schema(description = "权限标识")
    @TableField("mark")
    private String mark;

    @Schema(description = "权限描述")
    @TableField("description")
    private String description;

    @Schema(description = "关联菜单ID")
    @TableField("rel_menu_id")
    private Long relMenuId;

}
