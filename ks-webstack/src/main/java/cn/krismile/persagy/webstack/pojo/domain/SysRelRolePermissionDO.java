package cn.krismile.persagy.webstack.pojo.domain;

import cn.krismile.boot.framework.mybatisplus.domain.BaseDO;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 系统-角色-权限关联表
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_rel_role_permission")
@Schema(title = "SysRelRolePermissionDO对象", description = "系统-角色-权限关联表")
public class SysRelRolePermissionDO extends BaseDO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Schema(description = "关联角色ID")
    @TableField("rel_role_id")
    private Long relRoleId;

    @Schema(description = "关联权限ID")
    @TableField("rel_permission_id")
    private Long relPermissionId;

    @Schema(description = "描述信息")
    @TableField("description")
    private String description;

}
