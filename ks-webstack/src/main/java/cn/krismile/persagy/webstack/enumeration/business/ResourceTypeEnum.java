package cn.krismile.persagy.webstack.enumeration.business;

import cn.krismile.boot.framework.core.enumeration.BaseEnum;
import com.baomidou.mybatisplus.annotation.IEnum;
import com.fasterxml.jackson.annotation.JsonCreator;

/**
 * 资源类型枚举
 *
 * @author JiYinchuan
 * @since 1.0.0
 */
public enum ResourceTypeEnum implements BaseEnum<String>, IEnum<String> {

    /**
     * 内部
     */
    INTERNAL("INTERNAL", "内部"),

    /**
     * 用户
     */
    USER("USER", "用户"),

    /**
     * 公共
     */
    PUBLIC("PUBLIC", "公共");

    /**
     * 枚举值
     */
    private final String value;

    /**
     * 枚举信息
     */
    private final String reasonPhrase;

    /**
     * 构造器
     *
     * @param value        枚举值
     * @param reasonPhrase 枚举信息
     * @since 1.0.0
     */
    ResourceTypeEnum(String value, String reasonPhrase) {
        this.value = value;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String value() {
        return this.value;
    }

    @Override
    public String getReasonPhrase() {
        return this.reasonPhrase;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator(mode = JsonCreator.Mode.DELEGATING)
    public static ResourceTypeEnum analyze(String value) {
        return BaseEnum.analyze(value, ResourceTypeEnum.class);
    }
}
